import argh
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import os
from pyproj import Proj
from sklearn.decomposition import PCA
import scipy.interpolate

def jsons(lines):
    for line in lines:
        yield json.loads(line)

def strinterp(x, y):
    x = np.array(x)
    y = np.array(y)
    def i(nx):
        print(x)
        print(nx)
        idx = x.searchsorted(nx)
        idx[idx >= len(x)] = len(x) - 1 
        return y[idx]
    return i

def main(sdir):
    gdata = jsons(open(os.path.join(sdir, 'location.jsons')))
    protocol = jsons(open(os.path.join(sdir, 'protocol.jsons')))
    rows = []
    for hdr, o in gdata:
        if o['mProvider'] != 'gps':
            continue
        rows.append((hdr['ts'], o['mLatitude'], o['mLongitude']))
    
    protos = [[-np.inf, '']]
    for hdr, o in protocol:
        if 'block_start' in o:
            protos.append((hdr['time'], o['block_type'][0]))
        if 'block_end' in o:
            protos.append((hdr['time'], o['block_type'][0]))
            protos.append((hdr['time'] + 0.001, ''))
    
    protos = np.rec.fromrecords(protos, names='ts,proto')
    proto = strinterp(protos.ts, protos.proto)

    proj = Proj(init="EPSG:3067")
    coords = np.array(rows)
    coords[:,1:] = np.array(proj(coords[:,2], coords[:,1])).T

    cprotos = proto(coords[:,0])
    valid = np.in1d(cprotos, ('blind', 'unblind'))

    #plt.plot(x, y)
    pca = PCA()
    pca.fit(coords[:,1:][valid])
    m = pca.mean_
    r = pca.components_
    #coords[:,1:] = pca.transform(coords[:,1:])
    coords[:,1:] -= m
    coords[:,1:] = np.dot(r, coords[:,1:].T).T
    plt.plot(coords[:,1], coords[:,2])
    plt.axis('equal')
    plt.show()

if __name__ == '__main__':
    argh.dispatch_command(main)
