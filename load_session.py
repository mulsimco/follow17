import os
from collections import defaultdict
import itertools

import numpy as np
import pyproj
import pandas as pd
import ujson as json
from scipy.interpolate import interp1d

from decan import decode_can
from sensortypes import sensor_id_map

proj = pyproj.Proj(init='EPSG:3132')

def jsons(lines):
    for line in lines:
        if(hasattr(line, 'decode')):
            line = line.decode('utf-8')
        yield json.loads(line)

class GrowArray:
    def __init__(self):
        self.buf = None
        self.n = 0
        self.append = self._first_append

    def _first_append(self, row):
        self.n = 1
        self.buf = np.copy(np.array(row).reshape(1, -1))
        self.append = self._gen_append

    def _gen_append(self, row):
        i = self.n
        self.n += 1
        if self.n > len(self.buf):
            ncols = self.buf.shape[1]
            self.buf.resize(len(self.buf)*2, ncols)
        self.buf[i] = row

    def array(self):
        return self.buf[:self.n]

def read_sensors(getfile):
    sensors = open(getfile('sensors.jsons'))
    sensors = (json.loads(l) for l in sensors)
    data = defaultdict(GrowArray)
    #sensors = itertools.islice(sensors, 0, int(6e5))
    for hdr, obj in sensors:
        stype = obj['sensor_type']
        try:
            sname = sensor_id_map[stype]
        except KeyError:
            continue
        
        row = (hdr['ts'], obj['timestamp'], *obj['values'])
        data[sname].append(row)
    
    data = {n: np.rec.fromrecords(v.array(), names='ts,android_ts') for n, v in data.items()}
    return data

def read_telemetry(getfile):
    can = open(getfile('obd.jsons'))
    #start = int(2e6 + 1e5)
    #dur = int(1.5e5)
    #can = itertools.islice(open(getfile('obd.jsons')), int(1e6), None)
    #can = itertools.islice(open(getfile('obd.jsons')), start, start+dur)
    can = decode_can(can)
    
    toms = 1.0/(100.0*3.6)

    my = can.sender == 180
    wheelspeed = can.data.column(5, '>u2')[my]
    wheelspeed = np.rec.fromarrays(
            (can.ts[my], wheelspeed*toms),
            names='ts,wheelspeed')
    
    my = can.sender == 178
    rearwheels = np.rec.fromarrays(
            (can.ts[my], can.data.column(0, '>u2')[my]*toms, can.data.column(2, '>u2')[my]*toms, can.data.column(5, '>u1')[my]),
            names='ts,rightspeed,leftspeed,sawtooth')
    
    my = can.sender == 176
    frontwheels = np.rec.fromarrays(
            (can.ts[my], can.data.column(0, '>u2')[my]*toms, can.data.column(2, '>u2')[my]*toms, can.data.column(5, '>u1')[my]),
            names='ts,rightspeed,leftspeed,sawtooth')

    my = can.sender == 36
    yaw_rate = np.rec.fromarrays(
            (can.ts[my], (can.data.column(0, '>u2')[my].astype(float) - 512)/4),
            names='ts,yaw_rate')
    

    return dict(
            wheelspeed=wheelspeed,
            rearwheels=rearwheels,
            frontwheels=frontwheels,
            yaw_rate=yaw_rate
            )
    """
    s = can[can.sender == 178] # Front?
    #s = can[can.sender == 0x0b0]
    # Use only "Burst end" values"
    dt = np.diff(s.ts)
    mid = np.mean(dt)
    changes = dt > mid
    s = s[1:][changes]
    s1 = s.b0*255 + s.b1 # Probably left wheel
    s2 = s.b2*255 + s.b3 # Probably right wheel
    """
    dt = np.ediff1d(can.ts, to_begin=0)
    changes = dt > np.mean(dt)
    changes[0] = True; changes[-1] = True
    rng = np.arange(len(can.ts))
    can.ts = interp1d(rng[changes], can.ts[changes])(rng)

    sr = can[can.sender == 176] # Rear?

    dt = np.diff(sr.ts)
    mid = np.mean(dt)
    rng = np.arange(len(sr))

    sr.ts[:] = interp1d(rng[changes], sr.ts[changes])(rng)

    sr1 = sr.b0*255 + sr.b1 # Probably left wheel
    sr2 = sr.b2*255 + sr.b3 # Probably right wheel
    
    
    #speed = pd.DataFrame(dict(ts=sr.ts, left=sr1, right=sr2))
    
    # TODO:
    # Speed: (180, 5, '>u2')
    # Yaw rate: (36, 0, '>u2')
    # ---
    # Longitudal acceleration (186, 0, '>u2')
    s = can[can.sender == 180]
    wheelspeed = s.b5*255 + s.b6
    speed = pd.DataFrame(dict(ts=s.ts, wheelspeed=wheelspeed))
    
    s = can[can.sender == 36]
    yaw_rate = s.b0*255 + s.b1
    yaw_rate = pd.DataFrame(dict(ts=s.ts, yaw_rate=yaw_rate))

    #speed = pd.DataFrame(dict(ts=s.ts, left=s1, right=s2))
    return {
        "wheelspeed": speed,
        "yaw_rate": yaw_rate
    }

def read_gps(getfile):
    #ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    #lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')   
    gpsobjs = jsons(open(getfile('location.jsons')))
    gps = []
    for hdr, d in gpsobjs:
        if d['mProvider'] != 'gps': continue

        gps.append(dict(
            ts=hdr['ts'],
            gps_ts=d['mTime'],
            android_ts=d['mElapsedRealtimeNanos'],
            latitude=d['mLatitude'],
            longitude=d['mLongitude'],
            speed=d['mSpeed'],
            bearing=d['mBearing'],
            altitude=d['mAltitude'],
            ))
    
    gps = pd.DataFrame.from_records(gps)
    gps['x'], gps['y'] = proj(gps.longitude.values, gps.latitude.values)

    return dict(
            gps=gps.to_records(index=False)
            )

def read_blinder(getfile):
    objs = jsons(open(getfile('blinder.jsons')))
    buttons = []
    status = []
    commands = []
    for obj in objs:
        hdr, o = obj
        if 'button' in o:
            buttons.append((hdr['ts'], o['button']))
        if 'controller' in o:
            msg = o['controller'].strip()
            parts = msg.split()
            if ':' not in msg:
                if msg.startswith("Handling command "):
                    cmd = msg[-1]
                    commands.append((hdr['ts'], cmd))
            else:
                stat = {k[:-1]: v.strip() for k, v in zip(parts[::2], parts[1::2])}
                stat['ts'] = hdr['ts']
                #status.append(stat)
                try:
                    status.append((stat['ts'], float(stat['control_mode']), float(stat['blind_at']), float(stat['time'])))
                except KeyError:
                    continue
                #print(status)
    status = np.rec.fromrecords(status, names='ts,control_mode,blind_at,blinder_ts')
    commands = np.rec.fromrecords(commands, names='ts,command')
    buttons = np.rec.fromrecords(buttons, names='ts,button')
    return dict(
            status=status,
            commands=commands,
            buttons=buttons)

def read_lidar(getfile):
    #idc = open(getfile('ibeo.idc'))
    leader = np.load(getfile('chosen_object.npy'))
    data = [[ts, obj['distance'], obj['relative_velocity']['x'], obj['relative_velocity']['y'], obj['age']] for ts, obj in leader]
    data = np.rec.fromrecords(data, names='ts,distance,relvel_x,relvel_y,age')
    
    return dict(leader=data)

def read_protocol(getfile):
    events = []
    for line in open(getfile('protocol.jsons')):
        h, o = json.loads(line)
        if 'block_start' in o:
            events.append((h['time'], 'block_start', o['block_start'], o['block_type'][0]))
        if 'block_init' in o:
            events.append((h['time'], 'block_init', o['block_init'], o['block_type'][0]))
        if 'block_end' in o:
            events.append((h['time'], 'block_end', o['block_end'], o['block_type'][0]))

    events = np.rec.fromrecords(events, names='ts,event_type,block_number,block_type')
    return dict(events=events)

def main(directory, output):
    try:
        os.mkdir(output)
    except FileExistsError:
        pass
    getfile = lambda x: os.path.join(directory, x)
    
    if not os.path.exists(os.path.join(output, 'protocol.npz')):
        protocol = read_protocol(getfile)
        np.savez(os.path.join(output, 'protocol.npz'), **protocol)
    
    if not os.path.exists(os.path.join(output, 'lidar.npz')):
        lidar = read_lidar(getfile)
        np.savez(os.path.join(output, 'lidar.npz'), **lidar)
    
    if not os.path.exists(os.path.join(output, 'blinder.npz')):
        blinder = read_blinder(getfile)
        np.savez(os.path.join(output, 'blinder.npz'), **blinder)

    if not os.path.exists(os.path.join(output, 'telemetry.npz')):
        telemetry = read_telemetry(getfile)
        np.savez(os.path.join(output, 'telemetry.npz'), **telemetry)
    
    if not os.path.exists(os.path.join(output, 'gps.npz')):
        gps = read_gps(getfile)
        np.savez(os.path.join(output, 'gps.npz'), **gps)
    
    if not os.path.exists(os.path.join(output, 'sensors.npz')):
        sensors = read_sensors(getfile)
        np.savez_compressed(os.path.join(output, 'sensors.npz'), **sensors)
    

    gps = np.load(os.path.join(output, 'gps.npz'))
    sensors = np.load(os.path.join(output, 'sensors.npz'))
    telemetry = np.load(os.path.join(output, 'telemetry.npz'))
    
if __name__ == '__main__':
    import argh
    argh.dispatch_command(main)
