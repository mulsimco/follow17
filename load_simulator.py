import numpy as np
import sys
import os
import msgpack
import gzip
import json

def decode(l):
    return json.loads(l.replace("'", '"').replace("False", "false").replace("True", "true").replace('None', 'null'))

EXP_IDS = dict(
    closeTheGap=1,
    calibration=2,
    verification=3,
    switchLanes=4,
    speedControl=5,
    miniCalibration=6,
    miniVerification=7,
    followInTraffic=8,
    blindFollowInTraffic=9,
    laneDriving=10,
    )

def parseSession(f):
    lines = (l.decode('utf-8') for l in gzip.GzipFile(f))
    sid = int(os.path.basename(f).split('.')[0])
    #data = [json.loads(l) for l in open(path)]
    sys.stderr.write(f"{sid}\n"); sys.stderr.flush()
    data = (decode(l) for l in lines)
    
    blocksize = 10000
    i = 0
    dtype = [
        ('session_id', int),
        ('block', int),
        ('experiment', int),
        ('ts', float),
        ('player', float),
        ('leader', float),
        ('abstime', float),
        ('isBlind', bool),
        ('throttle', float),
        ('brake', float),
        ('steering', float),
        ('player_lateral', float)
    ]
    result = np.recarray(blocksize, dtype=dtype)

    block = -1
    experiment = None
    positions = []
    glanceTimes = []
    trialStartTime = None
    prevTime = None
    isBlind = False
    throttle = 0
    brake = 0
    steering = 0
    for row in data:
        d = row['data']
        if 'loadingScenario' in d:
            experiment = EXP_IDS[d['loadingScenario']]
            block += 1
        if 'endTime' in d:
            experiment = None
        if experiment is None: continue

        if 'blinder' in d:
            isBlind = d['blinder']

        if 'telemetry' in d:
            throttle = d['telemetry']['throttle']
            brake = d['telemetry']['brake']
            steering = d['telemetry']['steering']

        if 'physics' not in row['data']:
            continue
        pos = {}
        pos['ts'] = row['data']['physics']['time']
        if pos['ts'] is None:
            continue
        prevTime = pos['ts']
        for body in row['data']['physics']['bodies']:
            if body.get('objectClass') != 'vehicle':
                continue
            if body.get('objectName') == 'player':
                pos['player'] = body['position']['z']
                pos['player_lateral'] = body['position']['x']
            else:
                pos['leader'] = body['position']['z']
        
        if 'player' not in pos: continue
        if i >= len(result):
            result.resize(len(result)*2)
        row = (sid, block, experiment, pos['ts'], pos['player'], pos.get('leader', np.nan), row['time'], isBlind, throttle, brake, steering, pos['player_lateral'])
        result[i] = row
        i += 1
        #print(row)
    return result[:i]

def main(paths=sys.argv[1:]):
    data = []
    for path in paths:
        print(path)
        rows = parseSession(path)
        if len(rows) > 0:
            data.append(rows)
    if len(data) == 0:
        return None
    data = np.hstack(data)
    np.save("simagg.npy", data)
    return data

if __name__ == '__main__':
    main()
