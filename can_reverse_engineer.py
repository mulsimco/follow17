import numpy as np
import argh
import matplotlib.pyplot as plt
import os
import scipy.interpolate
import re
from binascii import unhexlify, b2a_hex
import struct
from collections import defaultdict
import numpy as np
import ujson as json

NONHEX=re.compile(r"[^0-9a-fA-F]")

def wheel_rotation(sensor_no):
    def parse(d):
        a, b = struct.unpack('<H<H', d)
        return dict(
            wheel_sensor=sensor_no,
            wheel_speed_a=a,
            wheel_speed_b=b
        )
    return parse

can_handlers = {
    0x0b0: wheel_rotation(1),
    0x0b2: wheel_rotation(2),
}

def decode_can_message(obj):
    hdr, msg = obj
    if 'can' not in msg:
        return {}

    msg = "0" + msg
    msg = unhexlify(msg)
    can_hdr, data = msg[:2], msg[2:]
    sender = int.from_bytes(hdr, byteorder='big', signed=False)
    if sender not in can_handlers:
        return {}
    return can_handlers[sender](data)

def jsons(lines):
    for line in lines:
        yield json.loads(line)

def get_gps_speeds(objs):
    res = []
    for hdr, o in objs:
        if o['mProvider'] != 'gps':
            continue
        res.append((hdr['ts'], o['mSpeed']))
    return np.array(res)

nbytes = 8
def decode_can(objs):
    senders = defaultdict(int)
    rows = []
    maxlen = 0
    for obj in objs:
        msg = obj[1]['can']
        if NONHEX.match(msg):
            continue

        msg = msg
        #msg = "00000" + msg
        msg = "0" + msg
        msg = unhexlify(msg)
        hdr = msg[:2]
        #sender = hdr[1]
        sender = int.from_bytes(hdr, byteorder='big', signed=False)
        #senders[sender] += 1
        #if(sender not in senders):
        #    print(sender)
        #    senders.add(sender)
        d = msg[2:]
        r = bytearray(nbytes)
        r[:len(d)] = d
        rows.append([obj[0]['ts'], sender, hdr, d] + list(r))
        if len(d) > maxlen:
            maxlen = len(d)
    assert maxlen <= nbytes
        
    #senders = sorted(senders.items(), key=lambda a: a[1])[::-1]
    rows = np.rec.fromrecords(rows, names='ts,sender,hdr,data'.split(',')+['b%i'%i for i in range(nbytes)])
    return rows

def parse_can_dump(lines):
    for obj in jsons(lines):
        v = decode_can_message(obj)
        if v is not None:
            yield v

def gps_vs_can_speed(directory):
    def getobjs(f):
        return jsons(open(os.path.join(directory, f)))
    gspeed = get_gps_speeds(getobjs('location.jsons'))
    gspeed = scipy.interpolate.interp1d(gspeed[:,0], gspeed[:,1], bounds_error=False)
    
    objs = getobjs('obd.jsons')

    # Do some "rewinding" so this won't take ages
    [next(objs) for i in range(100000)]
    can = [next(objs) for i in range(300000)]

    can = decode_can(can)
    s = can[can.sender == 0x0b0]
    # It seems that we get "chunked" timestamps. Let's assume a
    # constant frequency in the can-messages and cook up new
    # timestamps
    s.ts = np.poly1d(np.polyfit(np.arange(len(s)), s.ts, 1))(np.arange(len(s)))
    s1 = s.b0*255 + s.b1 # Probably left wheel
    s2 = s.b2*255 + s.b3 # Probably right wheel

    speed_interp = scipy.interpolate.interp1d(s.ts, (s1 + s2)/2.0, kind='linear', fill_value='extrapolate')
    
    plt.plot(s.ts, s1, color='red', label='Left wheel?')
    plt.plot(s.ts, s2, color='green', label='Right wheel?')
    plt.plot(s.ts, speed_interp(s.ts), color='blue', label='Wheel average')

    plt.legend()
    plt.twinx()
    plt.plot(s.ts, gspeed(s.ts), color='black', label='GPS speed (m/s)')
    plt.legend()
    plt.show()

def plot_sawtooth(directory):
    def getobjs(f):
        return jsons(open(os.path.join(directory, f)))

    can = getobjs('obd.jsons')
    #[next(can) for i in range(100000)]
    #can = [next(can) for i in range(300000)]
    #can = [next(can) for i in range(1000)]
    can = decode_can(can)
    
    can = can[can.sender == 176]
    #est_ts = np.poly1d(np.polyfit(np.arange(len(can)), can.ts, 1))(np.arange(len(can)))
    
    dts = np.diff(can.ts)
    flushes = dts > np.mean(dts)
    flush_idx = np.flatnonzero(flushes)
    flush_sizes = np.diff(flush_idx)
    durations = np.diff(can.ts[flush_idx])
    packets_per_second = flush_sizes/durations
    #plt.plot(packets_per_second)
    #plt.plot(can.ts[flush_idx])
    est_ts = np.poly1d(np.polyfit(flush_idx, can.ts[flush_idx], 1))(np.arange(len(can)))
    plt.plot(est_ts[flush_idx], est_ts[flush_idx] - can.ts[flush_idx])
    #plt.hist(dts[flushes])
    #plt.axvline(np.mean(dts))
    #plt.plot(can.b5)
    #plt.figure()
    #plt.hist(np.diff(can.ts))
    #plt.plot(can.ts - est_ts)
    plt.show()

from pprint import pprint
def plot_bytes(directory):
    def getobjs(f):
        return jsons(open(os.path.join(directory, f)))
    gspeed = get_gps_speeds(getobjs('location.jsons'))
    gspeed = scipy.interpolate.interp1d(gspeed[:,0], gspeed[:,1], bounds_error=False)
    
    objs = getobjs('obd.jsons')
    [next(objs) for i in range(100000)]
    can = [next(objs) for i in range(300000)]
    #can = [next(objs) for i in range(1000)]
    can = decode_can(can)
    senders = np.unique(can['sender'])
    scounts = []
    for s in senders:
        scounts.append((np.sum(can.sender == s),  s))
    scounts = list(reversed(sorted(scounts)))

    nbytes = 8
    for c, s in scounts:
        plt.suptitle(s)
        c = can[can.sender == s]
        ax = plt.subplot(nbytes, 1, 1)
        for bi in range(nbytes):
            plt.subplot(nbytes, 1, bi+1, sharex=ax)
            plt.plot(c.ts, c['b%i'%bi])
            #plt.plot(c.ts, gspeed(c.ts))
        plt.show()
    pprint(scounts)

def brutalize(directory):
    def getpath(f): return os.path.join(directory, f)
    return jsons(open(os.path.join(directory, f)))

def main(directory):
    def getobjs(f):
        return jsons(open(os.path.join(directory, f)))
    gspeed = get_gps_speeds(getobjs('location.jsons'))
    gspeed = scipy.interpolate.interp1d(gspeed[:,0], gspeed[:,1], bounds_error=False)
    
    objs = getobjs('obd.jsons')
    [next(objs) for i in range(100000)]
    can = [next(objs) for i in range(300000)]
    #can = [next(objs) for i in range(1000)]
    can = decode_can(can)

    s = can[can.sender == 0x0b0]
    s1 = s.b0*255 + s.b1 # Probably left wheel
    s2 = s.b2*255 + s.b3 # Probably right wheel
    speed_interp = scipy.interpolate.interp1d(s.ts, (s1 + s2)/2.0, kind='linear', fill_value='extrapolate')
    
    s = can[can.sender == 0x0b0]
    # It seems that we get "chunked" timestamps. Let's assume a
    # constant frequency in the can-messages and cook up new
    # timestamps
    
    s.ts = np.poly1d(np.polyfit(np.arange(len(s)), s.ts, 1))(np.arange(len(s)))
    s1 = s.b0*255 + s.b1 # Probably left wheel
    s2 = s.b2*255 + s.b3 # Probably right wheel
    
    o_ = can[can.sender == 0x0b2]
    # It seems that we get "chunked" timestamps. Let's assume a
    # constant frequency in the can-messages and cook up new
    # timestamps
    
    o_.ts = np.poly1d(np.polyfit(np.arange(len(o_)), o_.ts, 1))(np.arange(len(o_)))
    s3 = o_.b0*255 + o_.b1 # Probably left wheel
    s4 = o_.b2*255 + o_.b3 # Probably right wheel
    
    brakes = []
    for r in can[can.sender == 0x224]:
        parsed = struct.unpack("<BBBBHBB", r['data'])
        brakes.append([r['ts']] + list(parsed))
    
    brakes = np.array(brakes)
    speed_interp = scipy.interpolate.interp1d(o_.ts, (s3 + s4)/2.0, kind='nearest', fill_value='extrapolate')
    plt.plot(brakes[:,0], brakes[:,5])
    plt.twinx()
    plt.plot(brakes[:,0], speed_interp(brakes[:,0]), color='black')
    plt.show()
    throttles = []
    for r in can[can.sender == 0x2c1]:
        if len(r['data']) != 8:
                continue
        parsed = struct.unpack("<HBBBBbb", r['data'])
        throttles.append([r['ts']] + list(parsed))
    
    throttles = np.array(throttles)
    speed_interp = scipy.interpolate.interp1d(o_.ts, (s3 + s4)/2.0, kind='nearest', fill_value='extrapolate')
    plt.plot(throttles[:,0], throttles[:,-1])
    plt.twinx()
    plt.plot(throttles[:,0], speed_interp(throttles[:,0]), color='black')
    plt.show()
    #plt.plot(s.ts, s.b5, '.')
    #plt.show()
    #plt.hist(s.b5, bins=1000)
    #print(flagbits.shape)
    #for bit in range(flagbits.shape[1]):
    #    plt.title("%i"%bit)
    #    plt.plot(s.ts, flagbits[:,bit], '.')
    #    plt.show()
    speed = (s1 + s2)/2.0
    speed = (speed + speed_interp(s.ts))/2.0
    #speed = s1
    #plt.plot(s.ts, s1)
    #plt.plot(s.ts, s2)
    gs = gspeed(s.ts)
    valid = np.isfinite(gs)
    multiplier = float(np.linalg.lstsq(speed[valid].reshape(-1, 1), gs[valid])[0])
    #multiplier = 0.0028
    speed = (multiplier*speed).astype(np.float)
    from scipy.ndimage import gaussian_filter1d
    speed = gaussian_filter1d(speed, 5)
    
    #plt.plot(s.ts, s.b5)
    #print(np.unique(s.b5))
    #plt.show()
    """
    plt.plot(s.ts, gspeed(s.ts), color='red')
    plt.plot(s.ts, speed)
    plt.show()
    plt.plot(s.ts, np.gradient(speed)/np.gradient(s.ts))
    plt.show()
    """
    
    senders = [0x0b0, 0x0b2]
    senders = list(zip(*senders))[0]
    for sender in senders:
        sd = can[can.sender == sender]
        for byte in range(nbytes - 1):
            plt.title("Sender %x, byte %i"%(int(sender), byte))
            d = sd["b%i"%byte]*255 + sd["b%i"%(byte + 1)]
            plt.plot(sd.ts, d, '.')
            plt.twinx()
            plt.plot(sd.ts, gspeed(sd.ts), color='red')
            plt.show()

if __name__ == '__main__':
    import argh
    argh.dispatch_command(plot_bytes)
    #argh.dispatch_command(plot_sawtooth)
