import sys
import ujson as json
import numpy as np
from pathlib import Path
import itertools
from collections import namedtuple
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

types = 'i1,u1,i2,u2,i4,u4,f2,f4'.split(',')
DEFAULT_DTYPES = ["".join(v) for v in itertools.product('<>', types)]

class rawarray:
    def __init__(self, data, rowlen, base_offset=0):
        self.data = data
        self.rowlen = rowlen
        self.base_offset = base_offset

    def column(self, offset, dtype):
        dt = dict(
            names=('val',),
            formats=(dtype,),
            offsets=(self.base_offset + offset,),
            itemsize=self.rowlen
        )

        return np.frombuffer(self.data, dtype=dt)['val']

    @property
    def colbytes(self):
        return self.rowlen - self.base_offset

    def after(self, offset):
        return rawarray(self.data, self.rowlen, self.base_offset + offset)

candata = namedtuple('candata', 'senders,ts,data')
def decode_can(lines):
    data = bytearray()
    ts = []
    msglen = 10
    template = b'\0'*msglen
    for line in lines:
        hdr, obj = json.loads(line)
        msg = obj['can']
        try:
            msg = bytes.fromhex("0" + msg)
        except ValueError:
            continue
        buf = bytearray(template)
        buf[:len(msg)] = msg
        data += buf
        ts.append(hdr['ts'])
    ts = np.array(ts)
    dt = np.ediff1d(ts, to_begin=0)
    changes = dt > np.mean(dt)
    changes[0] = True; changes[-1] = True
    rng = np.arange(len(ts))
    ts = interp1d(rng[changes], ts[changes])(rng)
    #plt.hist(dt, bins=100)
    #plt.show()
    data = np.frombuffer(data, dtype=np.uint8)
    data = rawarray(data, msglen)
    #data = data.reshape(-1, msglen)
    senders = data.column(0, '>i2')
    return candata(senders, ts, data.after(2))

def get_gps_feats(objs):
    res = []
    for line in objs:
        hdr, o = json.loads(line)
        if o['mProvider'] != 'gps':
            continue
        res.append((hdr['ts'], o['mSpeed'], o['mBearing']))
    return np.rec.fromrecords(res, names='ts,speed,bearing')

def allcols(can, signal, dtypes=DEFAULT_DTYPES):
    colbytes = can.data.colbytes
    allsig = signal(can.ts)
    for sender in np.unique(can.senders):
        my = can.senders == sender
        ts = can.ts[my]
        sig = allsig[my]
        for dtype in dtypes:
            ds = np.dtype(dtype).itemsize
            if ds == 1 and '<' in dtype: continue
            for offset in range(colbytes - ds + 1):
                d = can.data.column(offset, dtype)[my]
                if 'i' in dtype and np.all(d >= 0): continue
                yield (sender, offset, dtype), d, sig

def brutalize_signal(can, signal, dtypes=DEFAULT_DTYPES, min_n=100):
    import scipy.stats
    cors = []
    for i, (idx, d, ref) in enumerate(allcols(can, signal, dtypes)):
        valid = np.isfinite(d) & np.isfinite(ref)
        n = valid.sum()
        if n < min_n: continue
        if np.var(d) == 0: continue
        with np.errstate(all='raise'):
            try:
                fit = np.poly1d(np.polyfit(d[valid].astype(float), ref[valid].astype(float), 1))
                fiterr = np.mean((fit(d[valid]) - ref[valid])**2)
                cor = fiterr
            except FloatingPointError:
                continue
            #try:
            #    cor = scipy.stats.pearsonr(ref[valid], d[valid])[0]
            #except FloatingPointError:
            #    continue
        if ~np.isfinite(cor): continue
        cors.append((cor, idx))

    cors.sort(key=lambda x: x[0])
    return cors

def main(directory):
    directory = Path(directory)
    can = (directory/'obd.jsons').open()
    can = itertools.islice(can, int(1e6), int(1e6 + 1e6))
    can = decode_can(can)
    gps = get_gps_feats((directory/'location.jsons').open())
    #gps.ts -= 0.65
    #gps.ts -= 0.5
    
    """
    adiff = np.gradient(gps.bearing)
    adiff = (adiff + 180)%360 - 180
    #yaw_change = np.arctan(np.sin(adiff), np.cos(adiff)) # Does this make sense!?!
    yaw_rate = adiff/np.gradient(gps.ts)
    valid = yaw_rate < 10.0
    signal = interp1d(gps.ts[valid], yaw_rate[valid], bounds_error=False)
    #plt.plot(gps.ts, signal(gps.ts))
    #plt.show()
    """
    
    #signal = interp1d(gps.ts, gps.speed)
    
    rearwheels = can.senders == 178
    left = can.data.column(0, '>u2')[rearwheels]
    right = can.data.column(2, '>u2')[rearwheels]
    wd = left.astype(float) - right.astype(float)
    #signal = interp1d(can.ts[rearwheels], wd, bounds_error=False)
    #plt.plot(can.ts[rearwheels], wd)
    #plt.plot(can.ts[rearwheels], right)
    #plt.show()

    my = can.senders == 180
    wheelspeed = can.data.column(5, '>u2')[my].astype(float)
    wheelaccel = np.gradient(wheelspeed)
    #wheelaccel = wheelaccel/np.gradient(can.ts[my])
    from scipy.ndimage import gaussian_filter1d
    wheelaccel = gaussian_filter1d(wheelaccel, 10)
    signal = interp1d(can.ts[my], wheelaccel, bounds_error=False)
    #plt.plot(can.ts[my], wheelaccel)
    #plt.show()
    
    #my = can.senders == 36
    #yaw_rate = can.data.column(0, '>u2')[my]
    #signal = interp1d(can.ts[my], yaw_rate, bounds_error=False)
    
    cors = brutalize_signal(can, signal)
    for cor, idx in cors:
        (sender, offset, dtype) = idx
        d = can.data.column(offset, dtype)
        my = can.senders == sender
        ts = can.ts[my]
        d = d[my]
        ref = signal(ts)
        valid = np.isfinite(ref)
        d = d[valid]
        ref = ref[valid]
        ts = ts[valid]
        fit = np.poly1d(np.polyfit(d, ref, 1))
        plt.plot(ts, fit(d), color='red')
        plt.plot(ts, ref, color='green')
        print(cor, idx)
        plt.show()


if __name__ == '__main__':
    import argh
    argh.dispatch_command(main)
