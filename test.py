import numpy as np
import json
import argh
import matplotlib.pyplot as plt
import os
import scipy.interpolate
import subprocess
from collections import defaultdict

from pyproj import Proj
proj = Proj(init='epsg:3067')
import pandas as pd
#TRACK_CENTER = np.array([392209.84275035  6680965.76580571])
#TRACK_ROTATION = np.array([[-0.99056619  0.13703508], [-0.13703508 -0.99056619]])

def lineseg_point_projection(p, a, b):
	(p, a, b) = map(np.array, (p, a, b))
	segd = b - a
	seglen = np.linalg.norm(segd)
	normstart = p - a
	t = np.dot(normstart, segd)/(seglen**2)
	if t > 1:
		error = np.linalg.norm(p - b)
		t = 1.0
		return t, error, b

	if t < 0:
		error = np.linalg.norm(normstart)
		t = 0.0
		return t, error, a
	
	proj = a + t*segd
	error = np.linalg.norm(p - proj)
	return t, error, proj

def to_track_coords(latlon):
    coords = np.array(proj(latlon[:,1], latlon[:,0])).T
    coords -= TRACK_CENTER
    coords = np.dot(TRACK_ROTATION, coords.T).T
    return coords

def est_ibeo_speed(objs):
    d = np.array([(o['relative_velocity']['y'], o['relative_velocity']['x']) for o in objs['objects']])
    if len(d) == 0:
        return np.nan
    vmag = np.linalg.norm(d[:,:2], axis=1)
    return np.mean(vmag)

def jsons(lines):
    for line in lines:
        if(hasattr(line, 'decode')):
            line = line.decode('utf-8')
        yield json.loads(line)

def get_gps_speeds(objs):
    res = []
    for hdr, o in objs:
        if o['mProvider'] != 'gps':
            continue
        res.append((hdr['ts'], o['mSpeed']))
    return np.array(res)

def spline_smoother(t, x, w=None, std=10.0):
    t = np.array(t)
    x = np.array(x)
    xtype = type(x)
    if w is None:
        w = np.zeros(len(t)) + 1.0/std
    t0 = t[0]
    dur = t[-1] - t0
    u = (t - t0)/dur
    spl = scipy.interpolate.splprep(x.T, u=u, w=w, s=np.sqrt(len(w))/3.0,)
    def interp(t):
        u = (t - t0)/dur
        ret = scipy.interpolate.splev(u, spl[0])
        return np.array(ret).T.copy().view(xtype)
    interp.x = t
    interp.y = x
    def velocity(t):
        u = (t - t0)/dur
        ret = scipy.interpolate.splev(u, spl[0], der=1)
        return np.array(ret).T.copy().view(xtype)
    interp.velocity = velocity
    return interp

def get_gps_position(objs):
    ts = []
    lat = []
    lon = []
    bearing = []
    for hdr, o in objs:
        if o['mProvider'] != 'gps':
            continue
        ts.append(hdr['ts'])
        lat.append(o['mLatitude'])
        lon.append(o['mLongitude'])
        bearing.append(np.radians(o['mBearing']))

    x, y = proj(lon, lat)
    xy = np.array([x, y]).T
    interp = scipy.interpolate.interp1d(ts, xy, axis=0, bounds_error=False)
    velocity_interp = scipy.interpolate.interp1d(ts, np.gradient(xy, axis=0)/np.gradient(ts).reshape(-1, 1), axis=0, bounds_error=False)
    
    interp.velocity = velocity_interp
    interp.bearing = scipy.interpolate.interp1d(ts, bearing, bounds_error=False)
    return interp
    #interp = spline_smoother(ts, xy)

def get_glance_times(objs):
    lift_times = []
    for hdr, o in objs:

        if 'controller' not in o: continue
        if o['controller'].strip() == 'Lifting':
            lift_times.append(hdr['ts'])
    return np.array(lift_times)

def is_in_front(o):
    return np.sign(np.min(o[:,1])) != np.sign(np.max(o[:,1]))

def est_ibeo_distance(objects):
    for o in objects['objects']:
        contour = np.array([[p['x'], p['y']] for p in o['contour']])
        if not is_in_front(contour):
            continue
        cp = np.min(contour[:,0])
        if cp < 0.5:
            continue
        return cp
    return np.nan

def ibeo_objects(idc):
    mangler = subprocess.Popen(['./objects_to_json'], stdout=subprocess.PIPE, stdin=idc)
    return jsons(mangler.stdout)

def strinterp(x, y):
    x = np.array(x)
    y = np.array(y)
    def i(nx):
        idx = x.searchsorted(nx)
        idx[idx >= len(x)] = len(x) - 1 
        return y[idx]
    return i

def find_leader(objects, mypos):
    objtracks = defaultdict(list)
    abspoints = []
    for i, (hdr, objs) in enumerate(objects):
        bearing = -mypos.bearing(hdr['ts'])
        R = np.array([[np.cos(bearing), -np.sin(bearing)], [np.sin(bearing), np.cos(bearing)]])
        loc = mypos(hdr['ts'])
        for o in objs['objects']:
            if o['age'] == 0:
                continue
            start_frame = i - o['age']
            obj_id = "%i/%i"%(o['id'], start_frame)
            
            point = np.argmin([p['x'] for p in o['contour']])
            point = o['contour'][point]
            point = [point['x'], point['y']]
            
            vx, vy = o['relative_velocity']['x'], o['relative_velocity']['y']
            width = np.ptp([p['y'] for p in o['contour']])
            #if np.linalg.norm(point) > 1.0:
            #    cp = np.array([[p['y'], p['x']] for p in o['contour']])
            #    abspoint = [np.dot(R, p) + loc for p in cp]
            #    
            #    abspoints.extend(abspoint)
            objtracks[obj_id].append([hdr['ts']] + point + [vx, vy, width])
        #if i > 50000:
        #    break

    #abspoints = np.array(abspoints)
    #plt.plot(abspoints[:,0], abspoints[:,1], '.', alpha=0.1)
    #plt.show()
    leader_spans = []
    for oid, cp in objtracks.items():
        if(len(cp) < 100): continue
        cp = np.array(cp)
        ts, dist = cp[:,0], cp[:,1:]
        relspeed = dist[:,2]
        myspeed = np.linalg.norm(mypos.velocity(ts), axis=1)
        distance = np.linalg.norm(dist[:,0:2], axis=1)
        landspeed = relspeed + myspeed
        medspeed = np.median(landspeed)
        meddist = np.median(dist[:,0])
        medwidth = np.median(dist[:,4])
        if(medwidth < 1.0):
            continue
        if(meddist < 3):
            continue
        if(medspeed < 10.0/3.6):
            continue
        
        leader_spans.append([oid, ts[0], ts[-1]])
        #thw = distance/myspeed
        #valid = myspeed > 3.0
        #plt.plot(ts, landspeed)
    
    leader_spans.sort(key=lambda x: x[1])
    blacklist = set()
    whitelist = list()
    
    while True:
        for i in range(len(leader_spans) - 1):
            if leader_spans[i+1][1] <= leader_spans[i][2]:
                rng1 = leader_spans[i][2] - leader_spans[i][1]
                rng2 = leader_spans[i+1][2] - leader_spans[i+1][1]
                if(rng1 >= rng2):
                    del leader_spans[i+1]
                else:
                    del leader_spans[i]
                break

        else:
            break
    
    track = []
    for oid, _, _ in leader_spans:
        track.extend(objtracks[oid])
    track = np.rec.fromrecords(track, names='ts,x,y,vx,vy,width')
    return track


def project_to_track(track, position):
    segments = []
    seg_lengths = []
    for i in range(len(track) - 1):
        s = track[i]
        e = track[i+1]
        dist = np.linalg.norm(np.subtract(e, s))
        seg_lengths.append(dist)
        segments.append([s, e])
    s = track[-1]
    e = track[0]
    dist = np.linalg.norm(np.subtract(e, s))
    seg_lengths.append(dist)
    segments.append([s, e])

    seg_starts = np.cumsum([0] + seg_lengths)
    trackdists = []
    laps = []
    lap = 0
    prevdist = 0
    for ts, pos in zip(position.x, position.y):
        projections = np.array([lineseg_point_projection(pos, s[0], s[1])[:2] for s in segments])
        closest_i = np.argmin(projections[:,1])
        closest = projections[closest_i]
        pathdist = seg_starts[closest_i] + closest[0]*seg_lengths[closest_i]
        if pathdist - prevdist < -seg_starts[-1]/2.0:
            lap += 1
        elif pathdist - prevdist > seg_starts[-1]/2.0:
            lap -= 1
        elif pathdist < prevdist:
            pathdist = prevdist
        trackdists.append((ts, pathdist, lap, lap*seg_starts[-1] + pathdist))
        prevdist = pathdist
    
    dists = np.array(trackdists)
    return dists, seg_starts[-1]
        

def test(sdir):
    glance_times = get_glance_times(jsons(open(os.path.join(sdir, 'blinder.jsons'))))
    #blind_durations = np.diff(glance_times)
    position = get_gps_position(jsons(open(os.path.join(sdir, 'location.jsons'))))
    track = json.load(open('track.json'))
    
    progress, track_length = project_to_track(track, position)

    start = 1000
    dur = 500
    span = slice(start, start+dur)
    ts = position.x
    ts = np.arange(ts[0], ts[-1], 0.1)
    pos = position(ts)
    velocity = position.velocity(ts)
    lap = scipy.interpolate.interp1d(progress[:,0], progress[:,2], kind='nearest', bounds_error='extrapolate')
    lap_pos = scipy.interpolate.interp1d(progress[:,0], progress[:,3], bounds_error=False)
    lap_speed = scipy.interpolate.interp1d(progress[:,0], np.gradient(progress[:,3])/np.gradient(progress[:,0]))
    protocol = (jsons(open(os.path.join(sdir, 'protocol.jsons'))))
    
    protos = [[-np.inf, '']]
    for hdr, o in protocol:
        if 'block_start' in o:
            protos.append((hdr['time'], o['block_type'][0]))
        if 'block_end' in o:
            protos.append((hdr['time'], o['block_type'][0]))
            protos.append((hdr['time'] + 0.001, ''))
    
    protos = np.rec.fromrecords(protos, names='ts,proto')
    proto = strinterp(protos.ts, protos.proto)
    
    ibeodata = (ibeo_objects(open(os.path.join(sdir, 'ibeo.idc'))))
    leader = find_leader(ibeodata, position)
    leader_distance = np.linalg.norm([leader.x, leader.y], axis=0)
    leader_distance = scipy.interpolate.interp1d(leader.ts, leader_distance, bounds_error=False)

    data = pd.DataFrame(dict(
        ts=ts,
        lap=lap(ts),
        lap_pos=lap_pos(ts),
        lap_speed=lap_speed(ts),
        block_type=proto(ts),
        landspeed=np.linalg.norm(velocity, axis=1),
        leader_distance=leader_distance(ts),
        ))
    return data
    data = data[data.block_type.isin(['blind', 'unblind'])]
    #data = data[data.block_type
    data = data[data.lap > 3]
    data = data[data.lap < np.max(data.lap)]
    drng = np.arange(0, track_length, 1.0)
    


    """
    distspeeds = []
    for lap, ld in data.groupby('lap'):
        p = ld['lap_pos'].values - lap*track_length
        dist_to_speed = scipy.interpolate.interp1d(p, ld.landspeed, bounds_error=False)
        distspeeds.append(dist_to_speed(drng))
        #plt.plot(drng, dist_to_speed(drng))

    medspeed = np.nanmean(distspeeds, axis=0)
    plt.plot(drng, medspeed)
    """
    """
    plt.plot(pos[:,0], pos[:,1])
    track = []
    trackp, = plt.plot([], [], 'o-')
    #import matplotlib
    def onclick(event):
        if event.button == 1:
            track.append([event.xdata, event.ydata])
        elif event.button == 3:
            track.pop()
        trackp.set_data(*zip(*track))
        trackp.figure.canvas.draw()

    trackp.figure.canvas.mpl_connect('button_press_event', onclick)
    plt.show()

    print(json.dumps(track))

    """

    """
    #plt.plot(pos[:,0], pos[:,1])
    
    plt.plot(position.x, np.linalg.norm(np.gradient(position.y, axis=0)/np.gradient(position.x).reshape(-1, 1), axis=1))
    #plt.plot(ts, np.linalg.norm(np.gradient(pos, axis=0)/np.gradient(ts).reshape(-1, 1), axis=1))
    plt.plot(ts, np.linalg.norm(velocity, axis=1))
    #np.linalg.norm(velocity, axis=1))
    plt.show()
    """
    

    #gspeeds = get_gps_speeds(jsons(open(os.path.join(sdir, 'location.jsons'))))
    #direction = velocity/np.linalg.norm(velocity, axis=1).reshape(-1, 1)
    #plt.plot(ts, np.linalg.norm(direction, axis=1))
    #plt.show()

    dists = []
    ibeodata = (ibeo_objects(open(os.path.join(sdir, 'ibeo.idc'))))
    leader = find_leader(ibeodata, position)
    return
    """
    for d in ibeodata:
        ts = d[0]['ts']
        dist = est_ibeo_distance(d[1])
        dists.append((ts, dist))
        
    protos = [[-np.inf, '']]
    for hdr, o in protocol:
        if 'block_start' in o:
            protos.append((hdr['time'], o['block_type'][0]))
        if 'block_end' in o:
            protos.append((hdr['time'], o['block_type'][0]))
            protos.append((hdr['time'] + 0.001, ''))
    
    protos = np.rec.fromrecords(protos, names='ts,proto')
    proto = strinterp(protos.ts, protos.proto)
    
    dists = np.array(dists)
    speed_interp = scipy.interpolate.interp1d(gspeeds[:,0] - 1.0, gspeeds[:,1], bounds_error=False)
    
    ax = plt.subplot(3,1,1)
    for t in glance_times:
        plt.axvline(t, alpha=0.5, color='black')
    speed = speed_interp(dists[:,0])
    valid = speed >= 1.0
    thw = dists[:,1]/speed
    thw[~valid] = np.nan

    blind_dur = np.diff(glance_times)
    valid = blind_dur < 30.0
    plt.plot(glance_times[1:][valid], blind_dur[valid])
    
    plt.plot(dists[:,0], thw)
    plt.subplot(3,1,2, sharex=ax)
    plt.plot(gspeeds[:,0] - 1.0, gspeeds[:,1])
    plt.subplot(3,1,3, sharex=ax)
    plt.plot(dists[:,0], dists[:,1])
    plt.show()
    """

def tests(*sdirs):
    data = []
    sids = []
    for sdir in sdirs:
        sid = os.path.basename(sdir)
        try:
            d = test(sdir)
            data.append(d)
            sids.append(sid)
        except ValueError:
            continue
        d.to_pickle("tmp/%s.pickle"%sid) 
    data = pd.concat(data, keys=sids)
    data.to_pickle('merged.pickle')
    #plt.show()

if __name__ == '__main__':
    argh.dispatch_command(tests)


