#!/usr/bin/env python3

import sys
import websockets
import asyncio
import argh
import json
import time
import cfmodels
import numpy as np
from data import get_malmi_sim, resample_session, mangle_sim, compute_stuffs
import subprocess
import io

async def get_slave():
    expdir = '/home/jampekka/pro/webtrajsim-vr'
    cmd = f"chromium --allow-file-access-from-files --user-data-dir=/tmp/chromium-data 'file://{expdir}/index.html?experiment=blindFollowViz&disableDefaultLogger=true'"
    browser = subprocess.Popen(cmd, shell=True)
    socket = asyncio.Future()
    the_end = asyncio.Future()

    async def thefuture(sock, path):
        socket.set_result(sock)
        await the_end
    await websockets.serve(thefuture, '0.0.0.0', 8080)
    socket = await socket
    return socket

import base64
import matplotlib.pyplot as plt
import skvideo.io
import scipy.ndimage
async def render_session(d, fname):
    slave = await get_slave()
    fps = 30
    dt = 1/fps
    d = resample_session(d, dt)
    writer = skvideo.io.FFmpegWriter(fname,
        {'-r': str(fps)},
        {'-vcodec': 'libx264', '-crf': '18'}
        )
    for i, (_, row) in enumerate(d.iterrows()):
        state = dict(leader=row.leader, player=row.player, player_speed=row.player_speed, is_blind=bool(row.isBlind))
        await slave.send(json.dumps(state))
        image = await slave.recv()
        mime, data = image.split(',', 1)
        data = base64.b64decode(data)
        
        img = scipy.ndimage.imread(io.BytesIO(data))
        writer.writeFrame(img)
        #plt.imshow(img)
        #plt.show()
    writer.close()

from compare_model import get_driver_params, get_bidm, simulate_follower_states
def main(subject, out, simulate=False):
    subject = int(subject)
    params = get_driver_params()
    params = params[params.session_id == subject].iloc[0]
    data = get_malmi_sim(f"experiment == 9 and session_id == {subject}")
    blocks = data.block.unique()
    for b in blocks[::-1]:
        bd = data.query('block == @b')
        duration = bd.ts.iloc[-1] - bd.ts.iloc[0]
        if duration < 200: continue
    #for b, bd in data.groupby(('session_id', 'block')):
        bd = mangle_sim(bd)
        s = dict(
                dt=0.1,
                x0=bd.player.iloc[0],
                v0=bd.player_speed.iloc[0],
                ts=bd.ts.values,
                leader=bd.leader.values,
                leader_speed=bd.leader_speed.values
                )
        params = params[['T_sim', 'a_sim', 'b_sim', 'uc_limit_sim']].values
        print(b, params)
        
        if simulate:
            player, *_, glances = simulate_follower_states(s, params)
            bd.player = player
            isBlind = ~np.array(glances)
            # Hack to extend the blind dur, as glances only have
            # the onsets
            for i in np.flatnonzero(~isBlind):
                isBlind[i:i+3] = False
            bd['isBlind'] = isBlind
            bd = compute_stuffs(bd)
            
        asyncio.get_event_loop().run_until_complete(render_session(bd, out))
        break

if __name__ == '__main__':
    argh.dispatch_command(main)
