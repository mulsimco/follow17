import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from collections import defaultdict

VEHICLE_LENGTH = 4.5

def get_malmi_sim(filt=None):
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.ts /= 2.0
    filts = ["session_id != 21 and (player > 100)"]
    if filt is not None:
        filts.append(f"({filt})")
    filts = " and ".join(filts)
    print(filts)
    data.query(filts, inplace=True)
    return data

FIELD_SKIPLIST = [
        (1,5),
        (1,6),
        (2,3),
        (3,1),
        (3,4),
        (4,8),
        ]
def get_malmi_field(filt=None, skiplist=FIELD_SKIPLIST):
    data = pd.DataFrame.from_records(np.load('../summary.npy'))
    filts = []
    if filt is not None:
        filts.append(f"({filt})")
    for s, b in skiplist:
        filts.append(f" not (session_id == {s} and block_number == {b})")
    filts = " and ".join(filts)
    print(len(data))
    data.query(filts, inplace=True)
    data.blind_dur[data.blind_dur < 0.3] = np.nan
    print(filts)
    print(len(data))
    return data

def default_methods(c):
    return dict(
        isBlind='nearest',
    ).get(c, 'linear')

def resample_session(s, dt=0.1, methods=default_methods):
    intcol = 'ts'
    ts = s[intcol].values
    nts = np.arange(ts[0], ts[-1], dt)
    import matplotlib.pyplot as plt
    nts = nts[(nts >= ts[0]) & (nts < ts[-1])] # OMG, there's a bug in arange!
    out = pd.DataFrame({'ts': nts})
    for col in s.columns:
        if col == intcol: continue
        method = methods(col)
        out[col] = interp1d(ts, s[col].values, kind=method)(nts)
    return out

def compute_stuffs(s):
    dt = np.gradient(s.ts)
    s['player_speed'] = np.gradient(s['player'])/dt
    s['player_accel'] = np.gradient(s['player_speed'])/dt
    s['leader_speed'] = np.gradient(s['leader'])/dt
    s['gap'] = s.leader - s.player - VEHICLE_LENGTH
    if 'isBlind' in s.columns:
        glance_idx = np.flatnonzero(np.diff(s.isBlind.values.astype(int)) < 0)
        glance_t = s.ts.values[glance_idx]
        blind_dur = np.diff(glance_t) - 0.3
        glance_idx = glance_idx[:-1]
        bd = np.repeat(np.nan, len(s))
        bd[glance_idx] = blind_dur
        s['blind_dur'] = bd

    return s

def mangle_sim(s):
    return compute_stuffs(resample_session(s))
