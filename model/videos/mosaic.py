import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)
import sys
import os

# OMG
def uridur(uri):
    pipeline = Gst.parse_launch(f"uridecodebin uri={uri} ! fakesink")
    pipeline.set_state(Gst.State.PAUSED)
    pipeline.get_state(Gst.CLOCK_TIME_NONE)
    return pipeline.query_duration(Gst.Format.TIME)[1]

def mosaic(participant, output):
    def bus_call(bus, message, loop):
        t = message.type
        if t == Gst.MessageType.EOS:
            sys.stout.write("End-of-stream\n")
            loop.quit()
        elif t == Gst.MessageType.ERROR:
            err, debug = message.parse_error()
            sys.stderr.write("Error: %s: %s\n" % (err, debug))
            loop.quit()
        return True
    
    D = participant
    DESC = f"""<b>Participant {D}</b>.
        The simulated and human VR drives have identical leader speeds.
        The parameters for the model have been estimated based on the participant's driving in the VR simulator.
        The real car video is from the same driver, but the leader speeds differ from the simulator videos.
    """
    PWD = os.getcwd()
    
    pipeline = f"""videomixer name=mix background=black
	sink_1::xpos=0 sink_1::ypos=0
	sink_2::xpos=0 sink_2::ypos=720
	sink_3::xpos=1280 sink_3::ypos=0
	sink_4::xpos=1280 sink_4::ypos=720
	
	uridecodebin uri=file://{PWD}/sim_{D}.mp4 name=simvid ! videoconvert ! videoscale ! video/x-raw,width=1280,height=720 ! textoverlay font-desc=Sans valignment=top text=\"Simulated VR driving\" ! mix.sink_1
	uridecodebin uri=file://{PWD}/driver_{D}.mp4 ! videoconvert ! videoscale ! video/x-raw,width=1280,height=720 ! textoverlay font-desc=Sans valignment=top text=\"Human VR driving\" ! mix.sink_2
	uridecodebin uri=file://{PWD}/field_{D}.mp4 ! videoconvert ! videoscale ! video/x-raw,width=1280,height=720 ! textoverlay font-desc=Sans valignment=top text=\"Human real car driving\" ! mix.sink_4
	videotestsrc pattern=black ! textoverlay font-desc=Sans valignment=center halignment=left text=\"{DESC}\" ! video/x-raw,width=1280,height=720 ! mix.sink_3 
	
	mix. ! queue ! tee name=src
	src. ! queue ! x264enc speed-preset=ultrafast pass=4 quantizer=18 ! mp4mux ! filesink sync=false location={output}
        """
	#src. ! queue ! autovideosink sync=false
    pipeline = Gst.parse_launch(pipeline)
    vid = pipeline.get_by_name("simvid")
    pipeline.set_state(Gst.State.PAUSED)
    wtf = pipeline.get_state(Gst.CLOCK_TIME_NONE)
    wtf, duration = vid.query_duration(Gst.Format.TIME)

    duration -= 1*Gst.SECOND
    
    wtf = pipeline.set_state(Gst.State.PLAYING)
    bus = pipeline.get_bus()
    while True:
        msg = bus.timed_pop_filtered(
            100*Gst.MSECOND,
            Gst.MessageType.ERROR | Gst.MessageType.EOS
        )
        if msg:
            break

        success, time = pipeline.query_position(Gst.Format.TIME)
        if time < duration:
            print(time/duration*100)
            continue
        
        pipeline.send_event(Gst.Event.new_eos())
        print("Sending EOS")
        msg = bus.timed_pop_filtered(
            Gst.CLOCK_TIME_NONE,
            Gst.MessageType.ERROR | Gst.MessageType.EOS
        )
        break


    pipeline.set_state(Gst.State.NULL)
    pipeline.get_state(Gst.CLOCK_TIME_NONE)

if __name__ == '__main__':
    import argh
    argh.dispatch_command(mosaic)

