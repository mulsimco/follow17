import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

"""
data = pd.DataFrame(np.load("/home/jampekka/pro/webtrajsim/analysis/sessions.npy"))
data.query('experiment == "followInTraffic"', inplace=True)
for g, d in data.groupby(('sessionId', 'block')):
    plt.plot(d.ts, d.throttle, '-')
    plt.show()
"""

data = pd.DataFrame(np.load("../simagg.npy"))
data.query('experiment == 9', inplace=True)
for g, d in data.groupby(('session_id', 'block')):
    plt.plot(d.ts, d.throttle, '.')
    plt.show()
