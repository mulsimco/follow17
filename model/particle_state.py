import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import cfmodels
import copy

class FollowState:
    def __init__(self, gap, myspeed, leader_speed):
        self.gap = gap
        self.myspeed = myspeed
        self.leader_speed = leader_speed

def softmax(v):
    r = v.copy()
    r -= np.max(v)
    np.exp(r, r)
    r /= np.sum(r)
    return r

def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    variance = np.average((values-average)**2, weights=weights)  # Fast and numerically precise
    return (average, np.sqrt(variance))

def median_and_iqr(values, weights):
    # TODO: Use the weights!
    l, m, h = np.percentile(values, [25, 50, 75])
    return m, (h - l)

def test():
    np.random.seed(0)
    n_particles = 1000

    dt = 0.1
    t = 0.0
    mypos = 0.0
    thw = 5.0
    targetspeed = 100.0/3.6
    #myspeed = targetspeed
    myspeed = 5.0
    myaccel = 0.0
    
    import pandas as pd
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.query('experiment == 9 and session_id == 19', inplace=True)
    blocks = data['block'].unique()
    data.query('block == @blocks[-1]', inplace=True)
    data.query('player > 1', inplace=True)
    data.ts /= 2.0
    data['ownspeed'] = np.gradient(data.player)/np.gradient(data.ts)
    data['ownaccel'] = np.gradient(data.ownspeed)/np.gradient(data.ts)

    tts = np.arange(data.ts.iloc[0], data.ts.iloc[-1], dt)
    player_position = scipy.interpolate.interp1d(data.ts.values, data.player.values)(tts)
    player_speed = np.diff(player_position)/dt
    player_accel = np.diff(player_speed)/dt
    myaccel = player_accel[0]

    tts = tts[2:]
    mypos = player_position[1]
    myspeed = player_speed[0]
    #myaccel = player_accel
    #plt.hist(data.ownaccel, bins=100)
    #plt.show()
    a, b = np.percentile(data.ownaccel.values, [99, 1])
    b = -b
    data['gap'] = data.leader - data.player - 4.5
    data['thw'] = data.gap/data.ownspeed
    thw = 1.0/np.mean(1.0/data.thw[data.player > 200])
    print(a, b, thw)
    
    print(thw)
    plt.show()
    #mypos = data.player.iloc[0]
    #myspeed = data.ownspeed.iloc[0]
    lspeed = np.gradient(data.leader)/np.gradient(data.ts)
    leaderspeeds = scipy.interpolate.interp1d(data.ts, lspeed)(tts)
    leaderpos = data.leader.iloc[0]
    #plt.plot(tts, leaderspeeds)
    #data.query('session_id == 1 and block == 2 and experiment == 1', inplace=True)
    #plt.plot(data.ts, data.leader)
    #plt.show()

    #nrounds = 2000
    #tts = np.arange(0, nrounds)*dt
    #leaderspeeds = 10.0 + np.sin(tts/5.0)*3.0
    #leaderpos = thw*targetspeed
    #leaderspeeds = np.zeros(len(tts)) + targetspeed - 2.0
    
    #leaderspeeds = np.zeros(len(tts)) + 30.0/3.6
    #leaderspeeds[500:] -= 5.0/3.6
    #plt.axvline(tts[500])
    #leaderpos = 100
    leaderspeed = leaderspeeds[0]

    state_est = cfmodels.FollowStatePredictor(cfmodels.FollowState(myspeed, leaderpos - mypos, leaderspeed), n_particles)
    particles = state_est.particles

    gap_measurement_dist = scipy.stats.norm(0, 0.01)
    speed_est_dist = scipy.stats.norm(0, 0.01)

    ts = []
    estgaps = []
    gaps = []
    allestgaps = []
    allaccels = []
    myaccels = []
    allrelspeeds = []
    relspeeds = []
    speeds = []
    accelerator = cfmodels.Idm(v0=targetspeed, T=thw, a=a, b=b)
    glances = [0.0]
    gt = None
    #plt.plot(tts, leaderspeeds)
    particle_log = []
    state_log = []
    for i, t in enumerate(tts):
        #if t > 40 and t < 45:
        #    leaderspeed -= 1.0*dt
        #leaderspeed = 10.0 + np.sin(t/5.0)*5.0
        #myaccel = player_accel[i]
        leaderspeed = leaderspeeds[i]
        myspeed += myaccel*dt
        mypos += myspeed*dt
        leaderpos += leaderspeed*dt
        
        #particles.myspeed += myaccel*dt + np.random.randn(n_particles)*0.1*dt
        #particles.myspeed[particles.myspeed < 0.0] = 0.0
        #particles.leadspeed += np.random.randn(n_particles)*1.0*dt
        #relspeed = particles.leadspeed - particles.myspeed
        #particles.gap += relspeed*dt
        gap = leaderpos - mypos
        state_log.append((myspeed, gap, leaderspeed, myaccel))
        
        #accels = [accelerator(dt, myspeed, gap, leaderspeed - myspeed) for p in particles]
        actual_accel = myaccel
        
        

        #speedlik = speed_est_dist.logpdf((myspeed - particles.myspeed)/(np.abs(myspeed) + 0.1))
        #particles.loglik += speedlik
        state_est.predict(dt, myaccel)
        state_est.measure_ownspeed(myspeed)
        state_est.finish_step()
        particles = state_est.particles
        weights = state_est.weights()
        accels = [accelerator(dt, p.state.ownspeed, p.state.gap, p.state.relspeed()) for p in particles]
        myaccel, saccel = weighted_avg_and_std(accels, weights)
        #print((a + b)*0.1)
        if (saccel > (a + b)*0.25 and gt is None) or (gt is not None and t - gt < 0.3):
            if gt is None:
                glances.append(t)
                gt = t
            state_est.measure_gap(gap)
            state_est.measure_relspeed(leaderspeed - myspeed, gap)
            state_est.finish_step()
            particles = state_est.particles
            weights = np.array([p.weight for p in particles])
            accels = [accelerator(dt, p.state.ownspeed, p.state.gap, p.state.relspeed()) for p in particles]
            myaccel, saccel = weighted_avg_and_std(accels, weights)
            #gaplik = gap_measurement_dist.logpdf((gap - particles.gap)/gap)
            #particles.loglik += gaplik
        else:
            gt = None
        #weights = np.array([p.weight for p in particles])
        #myaccel = np.mean(accels)
        #print(np.mean([p.state.gap for p in particles]))
        
        ts.append(t)
        particle_log.append(state_est.clone())
        allaccels.append(accels)
        
        if np.abs(actual_accel - myaccel) < 0.5:
            myaccel = actual_accel
        #allrelspeeds.append([p.state.relspeed() for p in particles])
        #relspeeds.append(leaderspeed - myspeed)
        #myaccel = np.mean(accels)
        #myaccels.append(myaccel)

        #gaps.append(gap)
        #allestgaps.append([p.state.gap for p in particles])
        #estgaps.append(np.mean(allestgaps[-1]))
        #speeds.append(myspeed)
        #plt.plot(np.array(t).reshape(-1, 1), particles.gap.reshape(1, -1), 'k.')
        t += dt
    #plt.plot(ts, allestgaps, '.', color='black', alpha=0.01)
    #plt.plot(ts, estgaps)
    #plt.plot(ts, gaps)
    
    #speeds = [[p.state.ownspeed for p in s.particles] for s in particle_log]
    ts = np.array(ts)
    states = np.rec.fromrecords(state_log, names='ownspeed,gap,leader_speed,ownaccel')
    
    ownspeeds = [s.ownspeeds() for s in particle_log]
    plt.plot(ts, np.array(ownspeeds)[:,::10], 'k.', alpha=0.01)
    plt.plot(ts, states.ownspeed)
    
    plt.figure()
    leader_speeds = [s.leader_speeds() for s in particle_log]
    plt.plot(ts, np.array(leader_speeds)[:,::10], 'k.', alpha=0.01)
    plt.plot(ts, states.leader_speed)
    
    plt.figure()
    gaps = [s.gaps() for s in particle_log]
    plt.plot(ts, np.array(gaps)[:,::10], 'k.', alpha=0.01)
    plt.plot(ts, states.gap)
    for t in glances: plt.axvline(t, color='black', alpha=0.1)

    plt.figure()
    blind_dur = np.diff(glances)
    plt.plot(glances[:-1][1:], blind_dur[1:], 'r-', label='Blind dur')
    thw = states.gap/states.ownspeed
    plt.plot(ts, thw, 'g-', label='Thw')
    
    lift_idx = np.flatnonzero(np.diff(data.isBlind.values.astype(int)) > 0)
    lift_times = data.ts.values[lift_idx]
    rblind_dur = np.diff(lift_times)
    print(lift_times)
    print(rblind_dur)
    plt.plot(lift_times[:-1], rblind_dur, 'r--')
    rthw = data.gap.values/data.ownspeed.values
    print(rthw)
    #plt.plot(lift_times, rthw[lift_idx], 'g--')
    plt.plot(data.ts, rthw, 'g--')
    
    #plt.semilogy()
    plt.legend()
    
    plt.figure()
    
    plt.plot(ts, np.array(allaccels)[:,::10], 'r.', alpha=0.01)
    plt.plot(ts, states.ownaccel)
    plt.plot(data.ts, data.ownaccel)
    for t in glances: plt.axvline(t, color='black', alpha=0.1)

    plt.show()

    """
    plt.ylabel('Relative speed (m/s)')
    plt.xlabel('Time (s)')

    allrelspeeds = np.array(allrelspeeds)[:,::10]
    plt.plot(ts, allrelspeeds, '.', color='red', alpha=0.01)[0]
    #plt.plot(ts, allrelspeeds[:,0], '.', color='red', alpha=0.1, label='Estimates')
    plt.plot(ts, relspeeds, color='blue', label='Thruth')
    for t in glances: plt.axvline(t, color='black', alpha=0.1)
    plt.axvline(0.0, color='black', alpha=0.1, label='Glance')
    plt.legend()
    plt.figure()
    plt.xlabel('Time (s)')
    plt.ylabel('Acceleration (m/s²)')
    allaccels = np.array(allaccels)[:,::10]
    plt.plot(ts, allaccels, '.', color='red', alpha=0.01)
    #plt.plot(ts, np.array(allaccels)[:,0], '.', color='red', alpha=0.1, label='Estimates')
    plt.plot(ts, myaccels, color='blue', label='Action')
    for t in glances: plt.axvline(t, color='black', alpha=0.1)
    plt.axvline(0.0, color='black', alpha=0.1, label='Glance')
    plt.legend()

    plt.figure()
    sample_i = np.searchsorted(ts, glances[1])
    plt.hist(allaccels[sample_i], bins=20, normed=True)
    plt.xlabel('Acceleration (m/s²)')

    plt.figure()
    plt.plot(ts, gaps)
    allestgaps = np.array(allestgaps)[:,::10]
    plt.plot(ts, allestgaps, 'r.', alpha=0.01)
    
    #blind_durs = np.diff(glances)
    #plt.plot(ts, speeds)
    #plt.plot(glances[:-1], blind_durs)
    #for g in glances:
    #    plt.axvline(g)
    #plt.twinx()
    #plt.plot(ts, np.array(gaps)/(np.array(speeds) + 1.0))

    plt.show()
    """

def simulate_thw_od():
    import pandas as pd
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.query('experiment == 8 and session_id == 4', inplace=True)
    blocks = data['block'].unique()
    data.query('block == @blocks[-1]', inplace=True)
    data.query('player > 1', inplace=True)
    data.ts /= 2.0
    dt = 0.1
    
    ts = np.arange(data.ts.iloc[0], data.ts.iloc[-1], dt)
    leader = scipy.interpolate.interp1d(data.ts.values, data.leader.values)(ts);
    orig_player = scipy.interpolate.interp1d(data.ts.values, data.player.values)(ts);
    leader_speed = np.diff(leader)/dt
    orig_player_speed = np.diff(orig_player)/dt
    orig_player_accel = np.gradient(orig_player_speed)/dt
    orig_player_jerks = np.gradient(orig_player_accel)/dt
    ts = ts[1:]
    leader = leader[1:]
    
    plt.plot(ts, orig_player_accel)
    plt.show()
    
    #thws = np.linspace(0.5, 10, 3)
    #thws = [1, 3, 10]
    thws = [2]*5
    init_player_x = 0.0
    init_leader_x = leader[0]
    init_leader_speed = leader_speed[0]
    init_player_speed = 0.0
    stats = []
    for thw in thws:
        accel = cfmodels.Idm(T=thw, a=2.0, b=2.0)
        state_est = cfmodels.FollowStatePredictor(
                cfmodels.FollowState(init_player_speed, init_leader_x - init_player_x, init_leader_speed), 1024)
        driver = cfmodels.DistractedAccelerator(1.0, state_est, accel)
        player_x = cfmodels.simulateFollower(driver, init_player_x, init_player_speed, ts, leader, leader_speed)
        player_speed = np.gradient(player_x)/dt
        player_accel = np.gradient(player_speed)/dt
        gap = leader - player_x
        if np.min(gap) == 0.0:
            print("CRASH!")
        thw = gap/player_speed
        #plt.plot(ts, thw)
        plt.plot(ts, player_speed, color='red', alpha=0.1)
        glances = np.array(driver.glances)
        gt = ts[1:][glances]
        od = np.diff(gt)
        print(1.0/np.mean(1/od))
        #stats.append((1.0/np.mean(1.0/thw[100:]), 1.0/np.mean(1.0/od[1:])))
        #plt.plot(gt[1:], od)
    player_x = cfmodels.simulateFollower(accel, init_player_x, init_player_speed, ts, leader, leader_speed)
    player_speed = np.gradient(player_x)/dt
    plt.plot(ts, player_speed, 'r--')
    plt.plot(ts, orig_player_speed)
    plt.plot(ts, leader_speed)
    leader_accel = np.gradient(leader_speed)/dt
    #plt.plot(ts, leader_accel, 'k--')
    plt.figure()
    #stats = np.array(stats)
    #plt.plot(stats[:,1], stats[:,0], 'o')
    #plt.plot([0, 5], [0, 5])
    plt.show()

from scipy.interpolate import interp1d
def piece_fit_follower(ts, leader, follower_truth, dt=0.1):
    nts = np.arange(ts[0], ts[-1], dt)
    leader = interp1d(ts, leader)(nts)
    follower_truth = interp1d(ts, follower_truth)(nts)
    ts = nts
    leader_speed = np.diff(leader)/dt
    follower_speed_truth = np.diff(follower_truth)/dt
    ts = ts[1:]
    follower_truth = follower_truth[1:]
    leader = leader[1:]

    nspans = 1000
    spanlen = int(5/dt)
    spanstarts = np.random.randint(0, len(ts) - spanlen, nspans)
    
    init_params=[120/3.6, 1.6, 0.73, 1.67, 4.0, 2.0]
    bounds = ([0.0]*len(init_params), [np.inf]*len(init_params))
    def getmodel(params):
        return cfmodels.Idm(*params)

    def geterror(params):
        model = getmodel(params)
        errors = []
        for start in spanstarts:
            span = slice(start, start+spanlen)
            sim_follower = cfmodels.simulateFollower(model,
                    follower_truth[start], follower_speed_truth[start],
                    ts[span], leader[span], leader_speed[span])
            errors.extend(sim_follower - follower_truth[span])
        return errors
    def fullerror(params):
        model = getmodel(params)
        sim_follower = cfmodels.simulateFollower(model,
                follower_truth[0], follower_speed_truth[0],
                ts, leader, leader_speed)
        
        errors = sim_follower - follower_truth
        return errors
    #initparams = [2.0, 2.0, 2.0, 4.0]
    res = scipy.optimize.least_squares(geterror, init_params, bounds=bounds, loss='soft_l1')
    model = getmodel(res.x)

    res2 = scipy.optimize.least_squares(fullerror, init_params, bounds=bounds, loss='soft_l1')
    model2 = getmodel(res2.x)
    
    print(res.x)
    print(res2.x)

    for start in spanstarts:
        span = slice(start, start+spanlen)
        sim_follower = cfmodels.simulateFollower(model,
                follower_truth[start], follower_speed_truth[start],
                ts[span], leader[span], leader_speed[span])
        plt.plot(ts[span], np.gradient(sim_follower)/dt, alpha=0.3)
    start = 0
    span = slice(start, None)
    sim_follower = cfmodels.simulateFollower(model,
            follower_truth[start], follower_speed_truth[start],
            ts[span], leader[span], leader_speed[span])
    sim_follower2 = cfmodels.simulateFollower(model2,
            follower_truth[start], follower_speed_truth[start],
            ts[span], leader[span], leader_speed[span])
    
    
    error_piecewise = np.mean(np.abs(sim_follower - follower_truth))
    error_full = np.mean(np.abs(sim_follower2 - follower_truth))
    print(error_piecewise, error_full)

    plt.plot(ts[span], np.gradient(sim_follower)/dt, color='red')
    plt.plot(ts, np.gradient(follower_truth)/dt, color='green')
    plt.plot(ts, np.gradient(sim_follower2)/dt, 'b--')
    plt.show()

import pandas as pd
def idmfit_nonblind():
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.query('experiment == 8', inplace=True)
    #blocks = data['block'].unique()
    #data.query('block == @blocks[-1]', inplace=True)
    data.query('player > 1', inplace=True)
    data.ts /= 2.0
    dt = 0.1

    for sid, sd in data.groupby('session_id'):
        for block, bd in sd.groupby('block'):
            ts = bd.ts.values
            dt = np.diff(ts)
            leader = bd.leader.values
            leader_speed = np.diff(leader)/dt
            player = bd.player.values
            player_speed = np.diff(player)/dt
            ts = ts[1:]
            player = player[1:]
            leader = leader[1:]
            piece_fit_follower(ts, leader, player)
            #plt.plot(ts, leader - player)
            #plt.plot(ts, leader_speed)
            #plt.plot(ts, player_speed)
            #plt.show()

if __name__ == '__main__':
    idmfit_nonblind()
    #simulate_thw_od()
    #test()
