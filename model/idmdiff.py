from sympy import *
init_printing(use_unicode=True)
from pprint import pprint
import matplotlib.pyplot as plt
import numpy as np

v0, T, a, b, aExp, s0, s1, v, dx = symbols("v0, T, a, b, aExp, s0, s1, v, dx", real=True, positive=True)
dv = symbols("dv", real=True)

desiredGap = s0 + s1*sqrt(v/v0) + T*v - (v*dv)/(2*sqrt(a*b));
idmAccel = a*(1.0 - pow(v/v0, aExp) - (desiredGap/dx)**2);

#pprint(idmAccel.simplify())

d_v = diff(idmAccel, v)
d_dx = diff(idmAccel, dx)
d_dv = diff(idmAccel, dv)
mag = d_v**2 + d_dx**2 + d_dv**2
print(d_v.simplify())
print(d_dx.simplify())
print(d_dv.simplify())
params = {v0: 120/3.6, T: 1.6, a: 0.73, b: 1.67, aExp: 4.0, s0: 2.0, s1: 0.0}
magtest = lambdify((v, dx, dv), mag.xreplace(params).simplify())
acceltest = lambdify((v, dx, dv), idmAccel.xreplace(params).simplify())

v = 10.0
dv = 0.0
dx = np.linspace(7.0, 300, 100)
plt.plot(dx, magtest(v, dx, dv))
plt.twinx()
plt.plot(dx, acceltest(v, dx, dv), color='red')
plt.show()
#print(mag)
