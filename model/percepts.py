import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import cfmodels

def test_distance():
    distance = np.linspace(0.0, 100, 1000)
    width = 2.0
    angle = 2*np.arctan((width/2.0)/(distance + 1))
    plt.plot(distance, np.degrees(angle))
    noise = np.random.randn(len(distance))*np.radians(0.1)
    percept = angle + noise
    plt.plot(distance, np.degrees(percept), '.')
    
    percept_distance = (width/2.0)/np.tan(np.abs(percept/2)) - 1
    plt.figure()
    plt.plot(distance, percept_distance - distance, '.')
    plt.show()

def simulate_distance_error(distance, nsteps=100):
    dt = 0.1
    
    gap_prop = scipy.stats.norm(distance, 10)
    n_particles = 1024
    est = cfmodels.FollowStatePredictor(cfmodels.FollowState(0.0, 0.0, 0.0), n_particles)
    est.ownspeeds()[:] = 0.0
    est.gaps()[:] = np.random.uniform(0, 1000, n_particles)
    est.leader_speeds()[:] = 0.0

    gests = []
    for i in range(nsteps):
        est.predict(dt, 0.0)
        est.measure_ownspeed(0.0)
        est.measure_relspeed(0.0, distance)
        est.measure_gap(distance)
        est.finish_step()
        gest = np.mean(est.gaps())
        gests.append(gest)
    return gests

# doi:10.1016/j.physa.2005.05.001
# 10.1007/978-3-642-32460-4_12
def test_distance_error():
    distances = np.linspace(5, 200, 10)
    niters = 100
    nsteps = 3
    errs = []
    for d in distances:
        ests = [simulate_distance_error(d, nsteps)[-1] for i in range(niters)]
        merr = np.mean(np.abs(np.array(ests) - d))
        #plt.plot([d]*len(ests), ests, '.')
        errs.append(merr)
    
    #plt.show()
    plt.plot(distances, errs, 'k', label="Angular error 0.2⁰, 0.3 s")

    nsteps = 10
    errs = []
    for d in distances:
        ests = [simulate_distance_error(d, nsteps)[-1] for i in range(niters)]
        merr = np.mean(np.abs(np.array(ests) - d))
        #plt.plot([d]*len(ests), ests, '.')
        errs.append(merr)

    plt.plot(distances, errs, 'k--', label="Angular error 0.2⁰, 1.0 s")
    plt.plot(distances, distances*0.1, color='black', ls='dotted', alpha=0.5, label="10% error")
    plt.legend()
    plt.xlabel('Distance (meters)')
    plt.ylabel('Average error (meters)')
    plt.show()

def simulate_relspeed_error(distance, relspeed, nsteps):
    dt = 0.1
    
    gap_prop = scipy.stats.uniform(distance, 100)
    speed_prop = scipy.stats.norm(relspeed, 100)

    n_particles = 1024
    est = cfmodels.FollowStatePredictor(cfmodels.FollowState(0.0, 0.0, 0.0), n_particles)
    est.ownspeeds()[:] = 0.0
    est.gaps()[:] = gap_prop.rvs(n_particles)
    est.logliks()[:] -= gap_prop.logpdf(est.gaps())
    est.leader_speeds()[:] = speed_prop.rvs(n_particles)
    est.logliks()[:] -= speed_prop.logpdf(est.leader_speeds())
    est.normalize_weights()
    
    print(distance)
    sests = []
    gests = []
    for i in range(nsteps):
        distance += relspeed*dt
        est.predict(dt, 0.0)
        #est.predict_known(dt, cfmodels.FollowState(0.0, distance, relspeed))
        est.measure_ownspeed(0.0)
        #est.measure_relspeed(relspeed, distance)
        est.measure_gap(distance)
        est.finish_step()
        sest = np.mean(est.leader_speeds() - est.ownspeeds())
        sests.append(sest)
        gests.append(np.mean(est.gaps()))


    return list(zip(sests, gests))


def test_relspeed_error():
    distances = np.linspace(10, 250, 5)
    relspeeds = np.array([0, 10, 20])
    nsteps = 3
    
    niters = 100

    for relspeed in relspeeds:
        errs = []
        for distance in distances:
            ests = [simulate_relspeed_error(distance, relspeed, nsteps)[-1] for i in range(niters)]
            sests, dests = zip(*ests)
            #plt.hist(ests)
            #plt.show()
            merr = np.mean(np.abs(np.array(sests) - relspeed))
            errs.append(merr)
            plt.plot([distance]*len(ests), np.array(dests), '.')
        plt.plot(distances, distances)
        plt.show()
        #plt.plot(distances, errs, label=relspeed)
    plt.legend()
    plt.show()


#    plt.plot(gests)
#    plt.show()

def transtest():
    distance = np.linspace(1.0, 150, 1000)
    width = 2.0
    viewer_distance = 1.5
    angle_dist = scipy.stats.norm(0.0, np.radians(0.2))

    distance_to_angle = lambda distance: 2*np.arctan((width/2.0)/(distance + viewer_distance))
    angle_to_distance = lambda angle: (width/2.0)/np.tan(angle/2.0) - viewer_distance
    
    test_distance = 20.0
    test_angle = distance_to_angle(test_distance)
    print(test_angle)
    angle_sample = test_angle + angle_dist.rvs(10000)
    distance_sample = angle_to_distance(angle_sample)
    plt.hist(distance_sample, bins=100)
    plt.twinx()
    distance_liks = angle_dist.pdf(distance_to_angle(distance) - test_angle)
    plt.plot(distance, distance_liks, 'r-')
    plt.show()
    #angles = distance_to_angle(distance)
    #angle_dist = scipy.stats.norm(0.0, np.radians(0.2))

def density_limit(ps, limit=0.5):
    ps = np.sort(ps.flatten())[::-1]
    cum = np.cumsum(ps)
    lim_i = cum.searchsorted(limit)
    return ps[lim_i]

def margins_axes():
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]

    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx, sharex=axScatter)
    axHistx.tick_params(
            axis='x',
            which='both',
            bottom='off',
            top='off',
            labelbottom='off'
            )
    axHistx.tick_params(
            axis='y',
            which='both',
            left='off',
            right='off',
            labelleft='off'
            )
    axHisty = plt.axes(rect_histy, sharey=axScatter)
    axHisty.tick_params(
            axis='y',
            which='both',
            left='off',
            right='off',
            labelleft='off'
            )
    axHisty.tick_params(
            axis='x',
            which='both',
            bottom='off',
            top='off',
            labelbottom='off'
            )
    
    return {'xy': axScatter, 'x': axHistx, 'y': axHisty}

def plot_percept_posterior():
    angle_dist = scipy.stats.norm(0.0, np.radians(0.1))
    angle_speed_dist = scipy.stats.norm(0.0, np.radians(0.1))
    
    width = 2.0
    viewer_distance = 1.5
    distance_to_angle = lambda distance: 2*np.arctan((width/2.0)/(distance + viewer_distance))
    angle_to_distance = lambda angle: (width/2.0)/np.tan(angle/2.0) - viewer_distance
    rel_speed_to_angle_speed = lambda dv, x: -(4*width)/(width**2 + 4*x**2)*dv
    angle_speed_to_rel_speed = lambda av, x: -av/((4*width)/(width**2 + 4*x**2))
    
    distances = np.linspace(1.0, 150, 1000)
    rel_speeds = np.linspace(-20, 20, 1000)

    test_distance = 100.0
    test_distances = [30, 70, 110]
    colors = ['r', 'g', 'b']
    axes = margins_axes()
    labels = []
    for color, test_distance in zip(colors, test_distances):
        test_rel_speed = 0.0
        test_angle = distance_to_angle(test_distance)
        test_angle_speed = rel_speed_to_angle_speed(test_rel_speed, test_distance)
        print(np.degrees(test_angle), np.degrees(test_angle_speed))

        dist_grid, speed_grid = np.meshgrid(distances, rel_speeds)
        angle_grid = distance_to_angle(dist_grid)
        angle_speed_grid = rel_speed_to_angle_speed(speed_grid, dist_grid)
        lik_grid = angle_dist.logpdf(angle_grid - test_angle) + angle_speed_dist.logpdf(angle_speed_grid - test_angle_speed)
        lik_grid -= np.max(lik_grid)
        lik_grid = np.exp(lik_grid)
        lik_grid /= np.sum(lik_grid)
        #plt.plot(test_distance, test_rel_speed, 'o')
        axes['xy'].contour(dist_grid, speed_grid, lik_grid, levels=[density_limit(lik_grid)], colors=color)
        margin = lik_grid.sum(axis=0)
        margin /= np.sum(margin)
        include = margin >= density_limit(margin, 0.999)
        margin /= np.max(margin)
        plot = axes['x'].plot(distances[include], margin[include], color=color)[0]
        labels.append((plot, f"d={test_distance} m, r={int(test_rel_speed)} m/s"))
        margin = lik_grid.sum(axis=1)
        margin /= np.sum(margin)
        include = margin >= density_limit(margin, 0.999)
        margin /= np.max(margin)
        axes['y'].plot(margin[include], rel_speeds[include], color=color)
    #plt.imshow(lik_grid, origin='lower',
    #        extent=(distances[0], distances[-1], rel_speeds[0], rel_speeds[-1])
    #        )
    #plt.colorbar()
    #plt.plot(speed_grid.flatten(), np.exp(lik_grid.flatten()), '.')
    axes['xy'].set_xlabel('Distance (meters)')
    axes['xy'].set_ylabel('Relative speed (m/s)')
    axes['xy'].legend(*zip(*labels), loc="upper left")
    plt.show()
    
def plot_perception():
    distance = np.linspace(1.0, 150, 1000)
    width = 2.0
    viewer_distance = 1.5

    distance_to_angle = lambda distance: 2*np.arctan((width/2.0)/(distance + viewer_distance))
    angle_to_distance = lambda angle: (width/2.0)/np.tan(angle/2.0) - viewer_distance
    #np.tan(angle/2.0) = (width/2.0)/(distance + 1)
    #angle = 2*np.arctan((width/2.0)/(distance + 1))
    
    angles = distance_to_angle(distance)
    plt.plot(distance, angle_to_distance(angles) - distance, 'k--', label='zero angular error')
    plt.plot(distance, angle_to_distance(angles - np.radians(0.1)) - distance, 'k-')
    plt.plot(distance, angle_to_distance(angles + np.radians(0.1)) - distance, 'k-', label='$\pm 0.1$ degree angular error')
    #plt.plot(np.degrees(angles), angle_to_distance(angles + np.radians(1.0)), 'k--')
    #plt.plot(np.degrees(angles), angle_to_distance(angles - np.radians(1.0)), 'k--')
    plt.legend()
    #plt.plot(distance + 1., np.degrees(angle), 'k--')
    plt.xlabel('Distance (meters)')
    plt.ylabel('Distance error (meters)')

    plt.figure()

    rel_speed = np.linspace(-10, 10, 1000)
    rel_speed_to_angle_speed = lambda dv, x: -(4*width)/(width**2 + 4*x**2)*dv
    angle_speed_to_rel_speed = lambda av, x: -av/((4*width)/(width**2 + 4*x**2))

    angle_dist = scipy.stats.norm(0.0, np.radians(0.1))
    angle_speed_dist = scipy.stats.norm(0.0, np.radians(0.1))
    
    #rel_speed_again = angle_speed_to_rel_speed(rel_speed_to_angle_speed(rel_speed, distance) + np.degrees(0.1), distance)

    #angle_speed = rel_speed_to_angle_speed(rel_speed, distance)
    
    test_distance = 75
    test_angle = distance_to_angle(test_distance)
    distance_liks = angle_dist.pdf(distance_to_angle(distance) - test_angle)
    plt.plot(distance, distance_liks/np.max(distance_liks))

    plt.axvline(test_distance)
    plt.show()

    Dgrid, Vgrid = np.meshgrid(distance, rel_speed)
    angle_speed_grid = rel_speed_to_angle_speed(Vgrid, Dgrid)
    angle_grid = angle_to_distance()
    plt.imshow(Agrid)
    plt.colorbar()
    #Vgrid_wrong = angle_speed_to_rel_speed(Agrid + np.degrees(0.1), Dgrid)
    #Verr_grid = Vgrid_wrong - Vgrid
    #plt.contour(Vgrid, Dgrid, Verr_grid)
    #plt.colorbar()
    #Egrid = 
    
    
    plt.show()

def softmax(x):
    x = x - np.max(x)
    x = np.exp(x)
    x /= np.sum(x)
    return x

def durationsim():
    stim_a = np.radians(1.0)
    stim_b = np.radians(1.015)
    #stim_b = 1.1
    
    angle_dist = scipy.stats.norm(0.0, np.radians(0.015))
    
    n_trials = 1000
    n_steps = 10
    was_right_m = []
    for i in range(n_trials):
        stim_hypotheses = np.radians(np.linspace(0.5, 1.5, 1000))
        stim_likelihoods_a = np.zeros(len(stim_hypotheses))
        stim_likelihoods_b = np.zeros(len(stim_hypotheses))

        was_right = []
        for i in range(n_steps):
            percept_a = stim_a + angle_dist.rvs()
            errors_a = stim_hypotheses - percept_a
            stim_likelihoods_a += angle_dist.logpdf(errors_a)
            
            percept_b = stim_b + angle_dist.rvs()
            errors_b = stim_hypotheses - percept_b
            stim_likelihoods_b += angle_dist.logpdf(errors_b)

            was_right.append(np.argmax(stim_likelihoods_b) > np.argmax(stim_likelihoods_a))

            #plt.plot(np.degrees(stim_hypotheses), softmax(stim_likelihoods_a), color='green')
            #plt.plot(np.degrees(stim_hypotheses), softmax(stim_likelihoods_b), color='red')
            #plt.show()
        was_right_m.append(was_right)

    was_right_m = np.array(was_right_m).sum(axis=0)/n_trials
    time = np.arange(1, n_steps+1)*0.1
    p = (was_right_m - 1/2.0)/(1 - 1/2.0)
    plt.plot(time, p)
    plt.show()

def test_rel_speed():
    distance = 10.0
    w = 2.0
    rel_speed_to_angle_speed = lambda dv, x: -(4*w)/(w**2 + 4*x**2)*dv
    for d in [5, 10, 20]:
        angle_speed = rel_speed_to_angle_speed(rel_speed, d)
        plt.plot(rel_speed, np.degrees(angle_speed))
    plt.show()

def test_distance_estimate():
    distance = np.linspace(1.0, 150, 10)
    width = 2.0
    viewer_distance = 1.5

    angle_dist = scipy.stats.norm(0.0, np.radians(0.3))
    distance_to_angle = lambda distance: 2*np.arctan((width/2.0)/(distance + viewer_distance))
    angle_to_distance = lambda angle: (width/2.0)/np.tan(angle/2.0) - viewer_distance
    
    n_hypos = 1000
    errors = angle_dist.rvs(n_hypos)
    ds = []
    for d in distance:
        angle = distance_to_angle(d)
        measurements = angle_to_distance(np.abs(angle + errors))
        #plt.hist(measurements, bins=100)
        #plt.show()
        mdist = np.mean(measurements)
        ds.append((d, mdist))

    ds = np.array(ds)
    plt.plot(ds[:,0], ds[:,0])
    plt.plot(ds[:,0], ds[:,1], '.')
    plt.show()

if __name__ == '__main__':
    test_relspeed_error()
    #test_distance_error()
    #test_distance_estimate()
    #durationsim()
    #plot_percept_posterior()
    #plot_perception()
    #test_rel_speed()
    #transtest()
