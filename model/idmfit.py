import cfmodels
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

def fit_idm(ts, player, init_speed, leader, blind=None):
    dt = np.gradient(ts)
    leader_speed = np.gradient(leader)/dt
    player_speed = np.gradient(player)/dt
    player_accel = np.gradient(player_speed)/dt
    gap = leader - player
    thw = 1.0/np.mean(player_speed/gap)
    a, b = np.percentile(player_accel, [99, 1])
    b = -b

    def fit_speed(thw, a, b):
        model = cfmodels.Idm(T=thw, a=a, b=b)
        sim_player = cfmodels.simulateFollower(model, player[0], init_speed, ts, leader, leader_speed)
        sim_speed = np.gradient(sim_player)/dt
        return sim_speed

    def fiterror(args):
        sim_speed = fit_speed(*args)
        return sim_speed - player_speed
    
    res = scipy.optimize.least_squares(fiterror, (thw, a, b),
            bounds=([0.5, 0.0, 0.0], [np.inf, 10.0, 10.0]),
            loss='soft_l1')
    if not res.success:
        return None
    thw, a, b = res.x
    #print(thw, a, b)
    #print(thw, a, b)
    #print("Done")
    sim_speed = fit_speed(thw, a, b)
    player_accel = np.gradient(player_speed)/dt
    sim_accel = np.gradient(sim_speed)/dt
    """
    plt.subplot(2,1,1)
    plt.plot(ts, player_speed)
    plt.plot(ts, sim_speed)
    plt.plot(ts, leader_speed)
    
    
    plt.subplot(2,1,2)
    plt.plot(ts, player_accel)
    plt.plot(ts, sim_accel)
    """
    aerr = sim_accel - player_accel
    zero_crossings = np.sum(np.sign(aerr[1:]) != np.sign(aerr[:-1]))/(ts[-1] - ts[0])
    #plt.plot(ts, aerr)
    #plt.show()
    #print(thw, zero_crossings)
    return thw, a, b, zero_crossings

def plot_controls():
    import pandas as pd
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.ts /= 2.0
    data.query('experiment == 9', inplace=True)

    for grp, d in data.groupby(('session_id', 'block')):
        dt = np.gradient(d.ts)
        speed = np.gradient(d.player)/dt
        leader_speed = np.gradient(d.leader)/dt
        glance_idx = np.flatnonzero(np.diff(d.isBlind.values.astype(int)) > 0)
        glance_times = d.ts.values[glance_idx]
        ax = plt.subplot(2,1,1)
        plt.plot(d.ts, d.throttle - d.brake)
        for t in glance_times: plt.axvline(t, color='black', alpha=0.1)
        plt.subplot(2,1,2, sharex=ax)
        for t in glance_times: plt.axvline(t, color='black', alpha=0.1)
        plt.plot(d.ts, speed*3.6)
        plt.plot(d.ts, leader_speed*3.6)
        plt.show()

def test():
    import pandas as pd
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.ts /= 2.0
    data.query('experiment == 8', inplace=True)
    
    params = []
    for grp, d in data.groupby(('session_id', 'block')):
        d = d[d.player > 1]
        if len(d) < 100: continue
        #ax = plt.subplot(2,1,1)
        ts = d.ts.values
        player = d.player.values
        leader = d.leader.values
        player_speed = np.gradient(player)/np.gradient(ts)
        fit = fit_idm(ts, player, player_speed[0], leader)
        if fit is None: continue
        params.append([grp[0]] + list(fit))
    
    params = pd.DataFrame.from_records(np.rec.fromrecords(params, names='sid,thw,a,b,absaccel'))

    plt.figure()
    params = params.groupby('sid').mean()
    plt.plot(params.thw, params.absaccel, 'o')
    import scipy.stats
    print(scipy.stats.spearmanr(params.thw, params.absaccel))
    plt.show()

if __name__ == '__main__':
    #plot_controls()
    test()
