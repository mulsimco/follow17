import matplotlib.pyplot as plt
import numpy as np

n_samples = 1000000
viewer_distance = 2.0
width = 2.0
distance_to_angle = lambda distance: 2*np.arctan((width/2.0)/(distance + viewer_distance))
angle_to_distance = lambda angle: (width/2.0)/np.tan(angle/2.0) - viewer_distance
def relspeed_to_anglespeed(d, dv):
    g = d + viewer_distance
    return -(4*width*dv)/(4*g**2 + width**2)

def anglespeed_to_relspeed(d, da):
    g = d + viewer_distance
    return da*(4*g**2 + width**2)/(-4*width)
    

def distance_error():
    ests = []
    dists = np.linspace(1, 200, 20)
    for d in dists:
        angles = distance_to_angle(d)
        seen_angles = np.abs(angles + np.radians(np.random.randn(n_samples)*0.3))
        ests.append(angle_to_distance(seen_angles))
    
    ests = np.array(ests)
    error = ests - dists.reshape(-1, 1)
    plt.plot(dists, np.median(np.abs(error), axis=1), 'k')
    plt.xlabel('Distance (m)')
    plt.ylabel('Distance estimate MAD (m)')

def speed_error():
    speeds = np.linspace(1, 100, 20)/3.6
    ests = []
    for s in speeds:
        Fs = np.log(s)
        ests.append(np.exp((Fs + np.random.randn(n_samples)*0.3)))
    
    ests = np.array(ests)
    error = ests - speeds.reshape(-1, 1)
    plt.plot(speeds*3.6, np.median(np.abs(error), axis=1)*3.6, 'k')
    plt.xlabel('Own speed (km/h)')
    plt.ylabel('Own speed estimate MAD (m)')

def relspeed_error():
    dvs = np.linspace(0, 100, 20)/3.6
    distances = np.array((25, 50, 75, 100))
    for distance in distances:
        ests = []
        for dv in dvs:
            angles = distance_to_angle(distance) + np.radians(np.random.randn(n_samples)*0.3)
            d = angle_to_distance(angles)
            da = relspeed_to_anglespeed(distance, dv) + np.radians(np.random.randn(n_samples)*0.3)
            est = anglespeed_to_relspeed(d, da)
            ests.append(est)
        ests = np.array(ests)
        error = np.median(np.abs(ests - dvs.reshape(-1, 1)), axis=1)
        plt.plot(dvs*3.6, error*3.6, 'k')
    plt.ylabel('Relative speed estimate MAD (km/h)')
    plt.xlabel('Relative speed (km/h)')

#def plot_errors():
    
plt.subplot(1,3,1)
speed_error()
plt.subplot(1,3,2)
distance_error()
plt.subplot(1,3,3)
relspeed_error()
plt.show()
#distance_error()
