import numpy as np
import cfmodels
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import noisyopt
import scipy.optimize

def get_model(params, initial_state):
    T, a, b, s = params
    accel = cfmodels.Idm(T=T, a=a, b=b)
    state_est = cfmodels.FollowStatePredictor(
        initial_state, 512)
    driver = cfmodels.DistractedAccelerator(s, state_est, accel)

    return driver

def get_det_model(params, initial_state):
    T, a, b = params
    accel = cfmodels.Idm(T=T, a=a, b=b)

    return accel

def get_stoch_model(det_params):
    def get_model(params, initial_state):
        accel = get_det_model(det_params, initial_state)
        state_est = cfmodels.FollowStatePredictor(initial_state, 512)
        driver = cfmodels.DistractedAccelerator(params[0], state_est, accel)
        return driver
    return get_model

def session_sampler(d, getmodel):
    dt = 0.1
    ts = d.ts.values
    nts = np.arange(ts[0], ts[-1], dt)
    
    player = interp1d(ts, d.player.values)(nts)
    leader = interp1d(ts, d.leader.values)(nts)
    ts = nts
    leader_speed = np.gradient(leader)/dt
    player_speed = np.gradient(player)/dt
    x0 = player[0]
    v0 = player_speed[0]
    init_state = cfmodels.FollowState(v0, leader[0] - v0, leader_speed[0])

    def run(params):
        model = getmodel(params, init_state)
        sim_player = cfmodels.simulateFollower(model, x0, v0, ts, leader, leader_speed)
        return sim_player - player

    return run, {'ts': ts, 'player': player, 'leader': leader, 'dt': dt, 'player_speed': player_speed}


def test():
    import pandas as pd
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.ts /= 2.0
    data.query('experiment == 9', inplace=True)
    init_params = [2.0, 2.0, 2.0, 1.0]
    bounds = [
            [0.5, 10.0],
            [0.5, 5.0],
            [0.5, 5.0],
            [0.5, 10.0]
            ]

    bounds = np.array(bounds)
    params = []
    for grp, d in data.groupby(('session_id', 'block')):
        od = d[d.player > 1]
        if len(d) < 100: continue
        if d.ts.iloc[-1] - d.ts.iloc[0] < 200: continue
        #ax = plt.subplot(2,1,1)

        det_sampler, d = session_sampler(od, get_det_model)
        sampler, d = session_sampler(od, get_model)
        def geterror(params):
            return np.mean(sampler(params)**2)
        
        def get_det_error(params):
            return np.mean(det_sampler(params)**2)

        
        
        res = scipy.optimize.least_squares(get_det_error, init_params[:-1], bounds=bounds[:-1].T)
        print(res.x)
        #res = noisyopt.minimizeSPSA(geterror, x0=init_params, niter=100, paired=False, bounds=bounds)
        det_params = res.x
        params = init_params
        
        stoch_sampler, d = session_sampler(od, get_stoch_model(det_params))
        def get_stoch_error(params):
            print(params)
            return np.mean(stoch_sampler(params)**2)
        
        #res = noisyopt.minimizeSPSA(get_stoch_error, x0=init_params[-1:], niter=500, paired=False, bounds=bounds[-1:])
        #stoch_params = res.x
        stoch_params = init_params[-1:]
        print("stoch", stoch_params)
        
        
        for i in range(10):
            sim_error = stoch_sampler(stoch_params)
            sim_player = d['player'] + sim_error
            sim_speed = np.gradient(sim_player)/d['dt']
            plt.plot(d['ts'], sim_speed, color='black', alpha=0.1)
        sim_error = det_sampler(det_params)
        sim_player = d['player'] + sim_error
        sim_speed = np.gradient(sim_player)/d['dt']
        plt.plot(d['ts'], sim_speed, color='red', alpha=0.5)
        plt.plot(d['ts'], d['player_speed'])
        plt.show()
        
    

if __name__ == '__main__':
    test()
