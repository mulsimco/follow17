import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cfmodels
import data as datatools
import scipy.optimize
import itertools
from scipy.ndimage import gaussian_filter1d
import seaborn as sns
sns.set(style='ticks')

def simulate_session(model, s):
    x = s.player.iloc[0]
    v = s.player_speed.iloc[0]
    player = cfmodels.simulateFollower(model, x, v, s.ts.values, s.leader.values, s.leader_speed.values)
    out = {'ts': s.ts, 'player': player, 'leader': s.leader}
    if(hasattr(model, 'glances')):
        out['isBlind'] =  ~np.array([True]+model.glances)
    out = pd.DataFrame(out)
    out = datatools.compute_stuffs(out)
    return out

def simulate_trajectory(model, s):
    x = s['x0']
    v = s['v0']
    player = cfmodels.simulateFollower(model, x, v, s['ts'], s['leader'], s['leader_speed'])
    out = {'ts': s['ts'], 'player': player, 'leader': s['leader']}
    if(hasattr(model, 'glances')):
        out['isBlind'] =  ~np.array([True]+model.glances)
    out = pd.DataFrame(out)
    out = datatools.compute_stuffs(out)
    return out

def simulate_mental_session(model, s, mental):
    player = s.player
    leader = np.array([np.mean(s.gaps()) for s in mental]) + player
    #leader_speed = np.gradient(leader)/np.gradient(s.ts.values)
    leader_speed = np.array([np.mean(s.leader_speeds()) for s in mental])
    x = s.player.iloc[0]
    v = s.player_speed.iloc[0]
    player = cfmodels.simulateFollower(model, x, v, s.ts, leader, leader_speed)
    out = pd.DataFrame({'ts': s.ts, 'player': player, 'leader': s.leader})
    out = datatools.compute_stuffs(out)
    return out

def fit_mental_idm(s, mental):
    ts = s.ts.values
    dt = np.gradient(ts)
    player = s.player.values
    player_speed = s.player_speed.values
    leader = np.array([np.mean(s.gaps()) for s in mental]) + player
    #leader_speed = np.gradient(leader)/np.gradient(s.ts.values)
    leader_speed = np.array([np.mean(s.leader_speeds()) for s in mental])
    x = player[0]
    v = player_speed[0]

    mental_ownspeeds = np.array([s.ownspeeds() for s in mental])
    mental_gaps = np.array([s.gaps() for s in mental])
    mental_thws = mental_gaps/mental_ownspeeds
    mental_thws[mental_gaps < 1] = np.nan
    mental_thws[mental_ownspeeds < 1] = np.nan
    mental_thw = np.nanmedian(mental_thws, axis=1)
    thw = np.nanmedian(mental_thw)
    
    true_thw = s.gap.values/player_speed
    true_thw[(s.gap.values < 1) | (player_speed < 1)] = np.nan 
    true_thw = np.nanmedian(true_thw)

    a, b = np.percentile(s.player_accel, [95, 5])
    #a = np.max(s.player_accel)
    #b = np.min(s.player_accel)
    #a = 3.0
    #b = -3.0
    #b = -b
    b = a/0.6 # See 10.3141/2249-09
    #v0 = np.max(mental_ownspeeds)
    v0 = 80/3.6
    s0 = 1.0
    #lag = 1.0
    #params = (v0, thw, a, b, s0)
    params = (thw,)
    def get_model(params):
        #v0, T, a, b, s0 = params
        T, = params
        accel = cfmodels.Idm(v0=v0, T=T, a=a, b=b, s0=s0)
        return accel
        #return cfmodels.LaggingAccelerator(1.0, accel)
    
    def get_sim_speed(params):
        #accel = cfmodels.Idm(*params)
        accel = get_model(params)
        sim_player = cfmodels.simulateFollower(accel, x, v, s.ts, leader, leader_speed)
        sim_speed = np.gradient(sim_player)/dt
        return sim_speed

    def get_speed_error(params):
        params = np.exp(params)
        return np.mean((get_sim_speed(params) - player_speed)**2)
    
    try:
        #fit = scipy.optimize.least_squares(get_speed_error, params, bounds=[[0.0], [np.inf]])
        fit = scipy.optimize.minimize(get_speed_error, np.log(params), method='Nelder-Mead')
        print(params, np.exp(fit.x))
        params = np.exp(fit.x)
    except ValueError as e:
        """
        print(params)
        plt.plot(leader_speed)
        plt.plot(get_sim_speed(params))
        plt.show()
        """
        raise
    
    #params = params[0], a, b
    params = thw, a, b
    return params, lambda: get_model(params)
    #plt.hist(s.player_accel, bins=100)
    #plt.axvline(a)
    #plt.axvline(-b)
    #plt.show()

def simulate_internal_state(s, dt=0.1):
    player_speed = s.player_speed.values
    player_accel = s.player_accel.values
    is_blind = s.isBlind.values
    gap = s.gap.values
    leader_speed = s.leader_speed.values
    relspeed = leader_speed - player_speed
    initial_state = cfmodels.FollowState(player_speed[0], gap[0], leader_speed[0])
    predictor = cfmodels.FollowStatePredictor(initial_state, 1024)
    
    particles = []
    states = []
    for i in range(len(s)):
        predictor.predict(dt, player_accel[i])
        #predictor.predict_known(dt,
        #        cfmodels.FollowState(player_speed[i], gap[i], leader_speed[i]))
        predictor.measure_ownspeed(player_speed[i])
        if not is_blind[i]:
            predictor.measure_gap(gap[i])
            predictor.measure_relspeed(relspeed[i], gap[i])
        predictor.finish_step()
        states.append(predictor.clone())
    return states
from data import VEHICLE_LENGTH
def simulate_accel_vs_blink_rate():
    thws = np.linspace(0.5, 10, 5)
    accels = np.linspace(0.5, 3, 5)
    limits = np.linspace(1.0, 3, 3)[::-1]
    #iterations = np.arange(5)

    data = datatools.get_malmi_sim('experiment == 8')
    n_sessions = 5
    n_particles = 1024
    sessions = []
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        d = datatools.mangle_sim(d)
        s = dict(
                x0=d.player.iloc[0],
                v0=d.player_speed[0],
                ts=d.ts.values,
                dt=0.1,
                leader=d.leader.values,
                leader_speed=d.leader_speed.values
            )
        sessions.append(s)
        if len(sessions) >= n_sessions: break
    i = 0
    for t, a, l, s in itertools.product(thws, accels, limits, sessions):
        b = -(-0.7*a - 0.2)
        accel = cfmodels.Idm(v0=80/3.6, T=t, a=a, b=b)
        init_state = cfmodels.FollowState(s['x0'],
                s['leader'][0] - s['x0'] - VEHICLE_LENGTH,
                s['leader_speed'][0]
                )
        state_est = cfmodels.FollowStatePredictor(init_state, n_particles)
        model = cfmodels.DistractedAccelerator(l, state_est, accel)
        player = cfmodels.simulateFollower(model,
                s['x0'], s['v0'], s['ts'], s['leader'], s['leader_speed'])
        gap = s['leader'] - player
        sim_speed = np.gradient(player)/s['dt']
        sim_accel = np.gradient(sim_speed)/s['dt']
        glances = np.array(model.glances, dtype=int)
        glance_idx = np.flatnonzero(np.diff(glances) > 0)
        glance_t = s['ts'][glance_idx]
        glance_durs = np.diff(glance_t)
        print(t, a, l, md, np.min(gap), np.min(sim_accel))

def follower_stats(ts, leader, player, is_blind):
    dt = np.gradient(ts)
    gap = leader - player - VEHICLE_LENGTH
    speed = np.gradient(player)/dt
    accel = np.gradient(speed)/dt
    glance_idx = np.flatnonzero(np.diff(is_blind.astype(int)) < 0)
    glance_t = ts[glance_idx]
    glance_durs = np.diff(glance_t) - 0.3
    
    thw = (gap/speed)
    valid_thw = (gap > 1.0) & (speed > 1.0)
    
    accel_1, accel_5, accel_25, accel_50, accel_75, accel_95, accel_99 = nanpercentile(accel, [1, 5, 25, 50, 75, 95, 99])
    thw_5, thw_25, thw_50, thw_75, thw_95 = nanpercentile(thw[valid_thw], [5, 25, 50, 75, 95])
    od_5, od_25, od_50, od_75, od_95 = nanpercentile(glance_durs, [5, 25, 50, 75, 95])
    return dict(thw_median=thw_50,
            od_median=od_50,
            gap_min=np.min(gap),
            accel_1=accel_1, accel_5=accel_5, accel_25=accel_25, accel_50=accel_50, accel_75=accel_75, accel_95=accel_95, accel_99=accel_99,
            thw_5=thw_5, thw_25=thw_25, thw_50=thw_50, thw_75=thw_75, thw_95=thw_95,
            od_5=od_5, od_25=od_25, od_50=od_50, od_75=od_75, od_95=od_95,
            accel_min=np.min(accel))

def get_bidm(T, a, b, l, v0=0):
    n_particles = 512
    accel = cfmodels.Idm(v0=80/3.6, T=T, a=a, b=b)
    init_state = cfmodels.FollowState(v0, 100.0, 0.0) # These get overwritten
    state_est = cfmodels.FollowStatePredictor(init_state, n_particles)
    state_est.gaps()[:] = np.random.uniform(5, 200, n_particles)
    state_est.leader_speeds()[:] = np.random.uniform(20/3.6, 60/3.6, n_particles)
    model = cfmodels.DistractedAccelerator(l, state_est, accel)
    return model, state_est, accel

def simulation_stats(s, t, a, b, l):
    #log2(a_r) = -0.2*log2(t) + 1.5
    #a - b = 2**(-0.2*log2(t) + 1.5)
    #a = (2**(-0.2*np.log2(t) + 1.5) - 0.2)/1.7
    #b = -(-0.7*a - 0.2)
    """
    accel = cfmodels.Idm(v0=80/3.6, T=t, a=a, b=b)
    init_gap = s['leader'][0] - s['x0'] - VEHICLE_LENGTH
    init_state = cfmodels.FollowState(s['v0'], init_gap, s['leader_speed'][0])
    state_est = cfmodels.FollowStatePredictor(init_state, n_particles)
    state_est.gaps()[:] = np.random.uniform(5, 200, n_particles)
    state_est.leader_speeds()[:] = np.random.uniform(20/3.6, 60/3.6, n_particles)
    """

    model, state_est, accel = get_bidm(t, a, b, l, v0=s['v0'])
    player = cfmodels.simulateFollower_nogil(model,
            s['x0'], s['v0'], s['ts'], s['leader'], s['leader_speed'])
    
    params = dict(uc_limit=l, a=a, b=b, T=t, pref_thw=t)
    stats = follower_stats(s['ts'], s['leader'], player, ~np.array(model.glances))
    stats.update(params)
    return stats, model, player

def simulate_params(ds, n_repeats=30):
    leaders = get_leader_trajectories()
    stats = []
    for param_i, d in ds.iterrows():
        for si in np.random.choice(len(leaders), size=n_repeats):
            s = leaders[si]
            sts = simulation_stats(s, d['T'], d['a'], d['b'], d['uc_limit'])[0]
            sts['param_i'] = param_i
            for n, v in d.iteritems():
                sts[n + "_orig"] = v
            sts['si'] = si
            stats.append(sts)
    return pd.DataFrame.from_records(stats)

import json
def simulate_dataset():
    grid_density = 30
    thws = np.linspace(0.3, 15, grid_density)
    accels = np.linspace(0.3, 8, grid_density)
    limits = np.linspace(0.0, 8, grid_density)
    n_sessions = grid_density
    skip_sessions = 0
    
    grid = itertools.product(thws, accels, limits, range(skip_sessions, skip_sessions + n_sessions))
    
    """
    i = 0
    for _ in itertools.product(thws, accels, limits, range(skip_sessions, skip_sessions + n_sessions)):
        i += 1
    print(i)
    return
    """
    sessions = get_leader_trajectories()

    if True:
        thw_rng = [0.1, 15]
        accel_rng = [0.1, 8]
        limit_rng = [0.0, 8]
        n_trials = 5000
        skip_sessions = n_sessions*2
        grid = ((
                    np.random.uniform(*thw_rng),
                    np.random.uniform(*accel_rng),
                    np.random.uniform(*limit_rng),
                    np.random.choice(range(len(sessions)))
                ) for i in range(n_trials))

    #iterations = np.arange(5)

        #for i in range(n_trials):
    #    T = np.random.uniform(*thw_rng)
    #    l = np.random.uniform(*limit_rng)
    #    a = np.random.uniform(*accel_rng)
        #T = np.random.choice(thws)
        #a = np.random.choice(accels)
        #l = np.random.choice(limits)
    
    def run_session(params):
        T, a, l, si = params
        b = a/0.6 # See 10.3141/2249-09
        si = np.random.choice(len(sessions))
        s = sessions[si]

        stats, model, _ = simulation_stats(s, T, a, b, l)
        stats['si'] = si
        return stats

    from concurrent.futures import ThreadPoolExecutor
    ex = ThreadPoolExecutor(max_workers=4)
    for stats in ex.map(run_session, grid):
        #b = -(-0.7*a - 0.2)
        print(json.dumps(stats))
        sys.stdout.flush()

def simperf():
    trajs = get_leader_trajectories()
    
    total_dur = 0.0
    n_trials = 1
    
    dt = 0.1
    ts = np.arange(0, 60*10, dt)
    leader_speed = np.zeros(ts.shape) + 60/3.6
    leader_speed[int(len(leader_speed)*0.7):] = 0.0
    leader = 300 + np.cumsum(leader_speed)*dt
    traj = dict(
            dt=dt,
            ts=ts,
            leader=leader,
            leader_speed=leader_speed,
            x0=0.0,
            v0=0.0/3.6,
            )

    for T in [1.0, 5.0, 10.0]:
        #traj = np.random.choice(trajs)
        total_dur += traj['ts'][-1] - traj['ts'][0]
        stats, model, player = simulation_stats(traj, T, 2.0, 2.0/0.6, 0.1)
        gap = leader - player - 4.5
        print(T, np.min(gap))
        player_speed = np.gradient(player)/traj['dt']
        plt.plot(traj['ts'], player_speed, alpha=0.5, label=T)
    plt.plot(traj['ts'], traj['leader_speed'], color='black')
    plt.legend()
    plt.show()
    print(total_dur)
        #model, state_est, accel = get_bidm(t, a, b, l)
        #simulate

def filter_unrealistic_stats(d):
    d = d[d.notnull().all(axis=1)]
    #d.query('accel_min > -20 and gap_min > 0.1', inplace=True)
    d = d[d.gap_min > 0.1]
    d = d[d.accel_min > -20.0]

    #d = d[(d.uc_limit <= 2.0)]
    #d = d[d['T'] > 1.0]
    
    return d

def get_bidm_training_set(filter_crashes=True):
    d = load_stats('simulated_trials_trainset_merged.json')
    if filter_crashes:
        d = filter_unrealistic_stats(d)
    return d

from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

def get_bidm_estimator():
    d = get_bidm_training_set()

    reg = make_pipeline(
            StandardScaler(),
            KNeighborsRegressor(n_neighbors=5, weights='uniform')
            )
    feat_cols = ['thw_50', 'accel_99', 'od_50']
    target_cols = ['T', 'a', 'b', 'uc_limit']
    feats = d[feat_cols].values
    reg.fit(d[feat_cols].values, d[target_cols].values)
    
    def predict(x):
        feats = x[feat_cols].values
        pred = reg.predict(feats)
        return pd.DataFrame(pred, columns=target_cols)

    return predict

def get_aggregate_predictor():
    d = get_bidm_training_set()

    reg = make_pipeline(
            StandardScaler(),
            KNeighborsRegressor(n_neighbors=50, weights='distance')
            )
    feat_cols = ['T', 'a', 'b', 'uc_limit']
    target_cols = list(d.columns)  #feat_cols = ['thw_50', 'accel_99', 'od_50']
    feats = d[feat_cols].values
    reg.fit(d[feat_cols].values, d[target_cols].values)
    
    def predict(x):
        feats = x[feat_cols].values
        pred = reg.predict(feats)
        return pd.DataFrame(pred, columns=target_cols)

    return predict

def test_bidm_estimator():
    d = []
    for l in open('simulated_trials_valset.json'):
        d.append(json.loads(l))
    d = filter_unrealistic_stats(pd.DataFrame.from_records(d))


    reg = get_bidm_estimator()
    pred = reg(d)

    plt.subplot(1, 3, 1) 
    plt.plot(pred['T'], d['T'], 'k.', alpha=0.5)
    rng = d['T'].min(), d['T'].max(); plt.plot(rng, rng, 'g--')
    plt.xlabel('Predicted $T$')
    plt.ylabel('Generating $T$')
    plt.subplot(1, 3, 2) 
    plt.plot(pred['a'].values, d['a'].values, 'k.', alpha=0.5)
    rng = d.a.min(), d.a.max(); plt.plot(rng, rng, 'g--')
    plt.xlabel('Predicted $a_{max}$')
    plt.ylabel('Generating $a_{max}$')

    plt.subplot(1, 3, 3)
    plt.plot(d['uc_limit'], pred['uc_limit'], 'k.', alpha=0.5)
    plt.xlabel('Predicted $u_{max}$')
    plt.ylabel('Generating $u_{max}$')
    rng = d.uc_limit.min(), d.uc_limit.max(); plt.plot(rng, rng, 'g--')
    cid = plt.gcf().canvas.mpl_connect('resize_event', lambda *args: plt.tight_layout())
    plt.tight_layout()
    plt.show()

def load_stats(path):
    d = []
    for l in open(path):
        d.append(json.loads(l))
    return pd.DataFrame.from_records(d)

def get_sim_grid_stats():
    d = []
    for l in open('simulated_trials_grid_7_20.json'):
        d.append(json.loads(l))
    d = filter_unrealistic_stats(pd.DataFrame.from_records(d))
    d = d[d.notnull().all(axis=1)]
    return d

def plot_simulated_results():
    d = get_sim_grid_stats()
    bins = np.percentile(d.od_median, np.linspace(0, 100, 5))
    d['od_median_dig'] = bins[np.digitize(d.od_median.values, bins, right=True)]

    for grp, gd in d.groupby(('T', 'od_median_dig')):
        ggd = gd.groupby('a').mean().reset_index()
        plt.plot(ggd['a'], ggd['uc_limit'], color='black')
    plt.show()

    #for a, ad in d.groupby('si'):
    #    gad = ad.groupby('a').mean().reset_index()
    #    #plt.plot(ad['a'], ad['accel_95'] - ad['accel_5'], '.')
    #    plt.plot(gad['a'], gad['accel_75'], '-')
    #    plt.plot(gad['a'], -gad['accel_25'], '-')
    #    plt.show()
    #plt.plot([0, 10], [0, 10])
    #plt.show()

def estimate_limit_estimator():
    d = []
    for l in open('simulated_trials_grid_7_20.json'):
        d.append(json.loads(l))
    d = pd.DataFrame.from_records(d)
    d = d[d.notnull().all(axis=1)]
    
    fit = KNeighborsRegressor(n_neighbors=20, weights='uniform')
    fit.fit(d[['od_median', 'pref_thw', 'a']], d['uc_limit'])
    """
    d = []
    for l in open('simulated_trials_random.json'):
        d.append(json.loads(l))
    d = pd.DataFrame.from_records(d)
    d = d[d.notnull().all(axis=1)]

    pred = fit.predict(d[['od_median', 'pref_thw', 'a']])
    plt.plot(d.uc_limit, pred, '.', alpha=0.1)
    plt.show()
    #plt.scatter(np.log(d.pref_thw), d.od_median, c=d.uc_limit)
    for T, td in d.groupby('a'):
        plt.scatter(td.od_median, td.uc_limit, c=td.pref_thw, alpha=0.3)
        plt.show()
    plt.show()
    """
    
    return fit

def simulate_pref_vs_blink_rate():
    thws = np.linspace(0.5, 10, 5)
    limits = np.linspace(1.0, 3, 3)[::-1]
    #iterations = np.arange(5)

    data = datatools.get_malmi_sim('experiment == 8')
    n_sessions = 5
    n_particles = 1024
    sessions = []
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        d = datatools.mangle_sim(d)
        s = dict(
                x0=d.player.iloc[0],
                v0=d.player_speed[0],
                ts=d.ts.values,
                dt=0.1,
                leader=d.leader.values,
                leader_speed=d.leader_speed.values
            )
        sessions.append(s)
        if len(sessions) >= n_sessions: break
    i = 0
    for t, l, s in itertools.product(thws, limits, sessions):
        #log2(a_r) = -0.2*log2(t) + 1.5
        #a - b = 2**(-0.2*log2(t) + 1.5)
        a = (2**(-0.2*np.log2(t) + 1.5) - 0.2)/1.7
        b = -(-0.7*a - 0.2)
        accel = cfmodels.Idm(v0=80/3.6, T=t, a=a, b=b)
        init_state = cfmodels.FollowState(s['x0'],
                s['leader'][0] - s['x0'] - VEHICLE_LENGTH,
                s['leader_speed'][0]
                )
        state_est = cfmodels.FollowStatePredictor(init_state, n_particles)
        model = cfmodels.DistractedAccelerator(l, state_est, accel)
        player = cfmodels.simulateFollower(model,
                s['x0'], s['v0'], s['ts'], s['leader'], s['leader_speed'])
        gap = s['leader'] - player
        sim_speed = np.gradient(player)/s['dt']
        sim_accel = np.gradient(sim_speed)/s['dt']
        glances = np.array(model.glances, dtype=int)
        glance_idx = np.flatnonzero(np.diff(glances) > 0)
        glance_t = s['ts'][glance_idx]
        glance_durs = np.diff(glance_t)
        md = np.median(glance_durs)
        
        thw = (gap/sim_speed)
        thw[(gap < 1.0) | (sim_speed < 1.0)] = np.nan
        thw = np.nanmedian(thw)
        print(t, thw, a, l, md, np.min(gap), np.min(sim_accel))

def getrng(v, steps=None):
    a, b = np.min(v), np.max(v)
    if steps is None: return a, b
    return np.linspace(a, b, steps)

def calibrate_subject_sim_model():
    fitter = get_bidm_estimator()
    data = datatools.get_malmi_sim('experiment == 9')
    stats = []
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        d = datatools.mangle_sim(d)
        s = follower_stats(d.ts.values, d.leader.values, d.player.values, d.isBlind.values)
        #params = fitter(pd.DataFrame.from_records([stats])).to_dict("records")[0]
        #stats.update(params)
        s['session_id'], s['block'] = g
        stats.append(s)

    stats = pd.DataFrame.from_records(stats)
    stats['accel_99'] *= 0.5 # !OMG!!
    #stats['accel_1'] = stats['accel_1']*0.5 # !OMG!!
    #stats['accel_99'] *= 0.5 # stats['accel_95'] # !OMG!!
    params = fitter(stats)
    stats = pd.concat((stats, params), axis=1)
    return stats

def calibrate_subject_field_model():
    fitter = get_bidm_estimator()
    stats = get_field_aggregates()
    """
    stats = []
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        d = datatools.mangle_sim(d)
        s = follower_stats(d.ts.values, d.leader.values, d.player.values, d.isBlind.values)
        #params = fitter(pd.DataFrame.from_records([stats])).to_dict("records")[0]
        #stats.update(params)
        s['session_id'], s['block'] = g
        stats.append(s)
    """

    #stats['accel_99'] *= 0.5 # !OMG!!
    #stats['accel_1'] = stats['accel_1']*0.5 # !OMG!!
    #stats['accel_99'] *= 0.5 # stats['accel_95'] # !OMG!!
    #plt.plot(stats.od_50, stats.thw_50, '.')
    #plt.plot([0, 10], [0, 10])
    #plt.show()
    params = fitter(stats)
    stats = pd.concat((stats, params), axis=1)
    return stats

def df_crossjoin(df1, df2, **kwargs):
    """
    Make a cross join (cartesian product) between two dataframes by using a constant temporary key.
    Also sets a MultiIndex which is the cartesian product of the indices of the input dataframes.
    See: https://github.com/pydata/pandas/issues/5401
    :param df1 dataframe 1
    :param df1 dataframe 2
    :param kwargs keyword arguments that will be passed to pd.merge()
    :return cross join of df1 and df2
    """
    df1['_tmpkey'] = 1
    df2['_tmpkey'] = 1

    res = pd.merge(df1, df2, on='_tmpkey', **kwargs).drop('_tmpkey', axis=1)
    res.index = pd.MultiIndex.from_product((df1.index, df2.index))

    df1.drop('_tmpkey', axis=1, inplace=True)
    df2.drop('_tmpkey', axis=1, inplace=True)

    return res

def closest(haystack, needles):
    haystack = np.asarray(haystack)
    needles = np.asarray(needles).reshape(-1, 1)
    dists = np.abs(haystack - needles)
    return haystack[np.argmin(dists, axis=1)]

import pickle
def get_leader_trajectories():
    return pickle.load(open('.leader_traj_cache.pickle', 'rb'))
    data = datatools.get_malmi_sim("experiment in [8, 9]")
    trajs = []
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        d = datatools.mangle_sim(d)
        trajs.append(dict(
            ts=d.ts.values,
            leader=d.leader.values,
            leader_speed=d.leader_speed.values,
            dt=0.1,
            x0=d.player.iloc[0],
            v0=d.player_speed.iloc[0]
            ))
    pickle.dump(trajs, open('.leader_traj_cache.pickle', 'wb'))
    return trajs

def od_thw_space_plot():
    data = get_bidm_training_set(filter_crashes=False)
    #plt.hist(np.log(data['od_median']))
    #plt.hist(np.log(-data['accel_min']), bins=100)
    #plt.hist(data['gap_min'])
    #data = data[data['uc_limit'] > 5.0]
    #data = data[np.abs(data.a - 3.0) < 0.5]
    #data = data[np.abs(data.uc_limit - 2.0) < 0.5]
    crashed = data['gap_min'] < 1.0
    crashed |= data['accel_min'] < -20
    print(np.sum(crashed))
    plt.hist(np.log(-data['accel_min']), bins=100)
    #plt.scatter(data['T'], data['uc_limit'], c=np.log(-data['accel_min']), marker='.', alpha=0.5)
    #plt.scatter(data['T'][crashed], data['uc_limit'][crashed], color='red', marker='.')

    #plt.scatter(data['od_50'], data['thw_50'], marker='.', alpha=0.1)
    #plt.scatter(data['od_50'][crashed], data['thw_50'][crashed], color='red', marker='.')
    #plt.plot([1, 10], [1, 10], color='black')
    
    plt.show()

def thw_vs_od():
    simstats = get_sim_grid_stats()
    simpred = get_aggregate_predictor()
    
    #stats_field = calibrate_subject_field_model()
    stats_sim = calibrate_subject_sim_model()
    
    """
    agg_field = stats_field.groupby('session_id').mean().reset_index()
    agg_sim = stats_sim.groupby('session_id').mean().reset_index()

    agg_full = agg_field.merge(agg_sim, on='session_id', suffixes=('', '_sim'))
    
    plt.set_cmap("viridis")

    print(scipy.stats.pearsonr(np.log(agg_full['uc_limit_sim']), np.log(agg_full['uc_limit'])))
    plt.scatter(agg_full['uc_limit_sim'], agg_full['uc_limit'], marker='o', c=agg_full['a_sim'])
    #rng = agg_full['uc_limit_sim']
    plt.loglog(base=2)
    plt.show()
    """
    
    trng = 0, 20
    urng = 0, 8
    #agg = agg[agg.od_50 < 6]

    stats = stats_sim; agg = stats.groupby('session_id').mean().reset_index()
    median_driver = agg.median()
    
    print(scipy.stats.pearsonr(np.log(agg['T']), np.log(agg['uc_limit'])))
    #plt.hist(stats['uc_limit'], bins=20)

    afit_log = np.poly1d(np.polyfit(np.log(agg['T']), np.log(agg['a']), 1))
    afit = lambda x: np.exp(afit_log(np.log(x)))

    """
    plt.scatter(agg['T'], agg['a'], marker='o', c=(agg['uc_limit']))
    rng = np.linspace(1.0, 10, 10)
    plt.plot(rng, afit(rng))
    plt.show()
    """
    
    #Trng = getrng(agg['T'], 100)
    Trng = np.linspace(0.5, 10, 20)
    #plt.hist(np.log(agg.uc_limit), bins=10)
    #plt.show()
    uc_range = np.percentile(stats.uc_limit, [0, 10, 50, 90, 100])

    grid = get_sim_grid_stats()
    l = grid.uc_limit.unique()
    grid = grid[grid.uc_limit.isin(closest(grid.uc_limit.unique(), uc_range))]
    
    grid = grid[grid.a.isin(closest(grid.a.unique(), median_driver.a))]
    sim_mean_stats = grid.groupby(('T', 'uc_limit')).mean()

    driver_range = pd.DataFrame.from_records([
            dict(T=T, a=median_driver.a, b=median_driver.b, uc_limit=uc)
            for T, uc in itertools.product(Trng, uc_range)
            ])
    driver_range['a'] = afit(driver_range['T'])
    driver_range['b'] = driver_range['a']/0.6
    print(driver_range)
    #driver_range = df_crossjoin(median_driver, Trng)
    #driver_range = df_crossjoin(driver_range, uc_range)
    #driver_range = pd.DataFrame(dict(T=Trng, a=median_driver.a, b=median_driver.b, uc_limit=median_driver.uc_limit))
    #sim_mean_stats = simpred(driver_range)
    #sim_mean_stats = simulate_params(driver_range)
    #sim_mean_stats.to_csv('.sim_mean_stats_cache.csv')
    sim_mean_stats = pd.read_csv('.sim_mean_stats_cache.csv')
    
    for pi, d in sim_mean_stats.groupby('uc_limit'):
        #plt.plot(m.od_50, m.thw_50)
        plt.scatter(d['T'], d['od_50'], marker='.', alpha=0.3, c=d['gap_min'])
        plt.show()

    
    sim_mean_stats = sim_mean_stats.groupby('param_i').mean().reset_index()
    sim_mean_stats = sim_mean_stats.join(driver_range, lsuffix='_pred')

    #plt.plot(Trng, Trng)
    
    #for s, sd in stats.groupby('session_id'):
    #    sd = sd.sort_values('od_50')
    #    plt.plot(sd['od_50'], sd['thw_50'], 'k-', alpha=0.1)
        
    sctr = plt.scatter(agg['od_50'], agg['thw_50'], marker='o', c=agg.uc_limit, cmap="viridis")
    odrng = np.linspace(0.5, agg.od_50.max(), 100)
    for uc, d in sim_mean_stats.groupby('uc_limit'):
        plt.plot(d['od_50'], d['thw_50'],
                color=sctr.to_rgba(uc)
                )
    plt.title('VR simulator')
    plt.plot(odrng, odrng + 1, 'k-', alpha=0.5)
    plt.plot(odrng, odrng, 'k--', alpha=0.5)
    plt.xlabel('Median occlusion duration (seconds)')
    plt.ylabel('Median time headway (seconds)')
    #plt.xlim(*odrng)
    #plt.ylim(*getrng(agg.thw_50))
    #plt.scatter(stats['od_50'], stats['thw_50'], marker='.', c=stats.uc_limit)
    #plt.colorbar()
    plt.show()
    
    for s, sd in stats.groupby('session_id'):
        plt.plot(sd['T'], sd.a, '.')
        plt.plot(sd['T'].mean(), sd.a.mean(), 'o')
        plt.xlim(*trng)
        plt.ylim(*urng)
        plt.show()
    plt.show()

def simulate_follower_states(s, params):
    x = s['x0']
    v = s['v0'] 
    dt = s['dt']
    ts = s['ts']
    leader = s['leader']
    leader_speed = s['leader_speed']
    vehicle_length = 4.5
    #n_particles = 1024
    
    model, state_est, accel = get_bidm(*params, v0=v)
    state_est.leader_speeds()[:] = leader_speed[0]
    state_est.gaps()[:] = leader[0] - x - VEHICLE_LENGTH
    
    all_states = []
    all_accels = []
    xs = []
    xs.append(x)
    all_states.append(state_est.clone())
    accels = accel.vec(dt, state_est.ownspeeds(), state_est.gaps(), state_est.leader_speeds() - state_est.ownspeeds())
    all_accels.append(accels)
    for i in range(1, len(leader)):
        dt = ts[i] - ts[i-1]
        lv = leader_speed[i]
        dv = lv - v
        gap = leader[i] - x - VEHICLE_LENGTH
        a = model(dt, v, gap, dv)
        v += a*dt
        x += v*dt
        
        all_states.append(state_est.clone())
        accels = accel.vec(dt, state_est.ownspeeds(), state_est.gaps(), state_est.leader_speeds() - state_est.ownspeeds())
        all_accels.append(accels)
        xs.append(x)

    return np.array(xs), all_states, all_accels, np.array([True] + model.glances)

from scipy.stats import gaussian_kde
def plot_kde(values, rng=None):
    kde = gaussian_kde(values)
    
    if rng is None:
        start, end = np.percentile(values, [1, 99])
    else:
        start, end = rng
    rng = np.linspace(start, end, 1000)
    plt.plot(rng, kde(rng), color='black')
    plt.xlim(rng[0], rng[-1])

def cartesian_to_angle_speed(dv, w, g):
    return -(4*w*dv)/(4*g*g + w*w)


def approach_demo():
    dt = 0.1
    ts = np.arange(0, 60*5, dt)
    #leader_speed = np.repeat(0/3.6, len(ts))
    leader_speed = np.sin(ts/10)*10/3.6 + 50/3.6
    leader = 50 + np.cumsum(leader_speed*dt)
    s = dict(
            dt=dt,
            x0=0.0,
            v0=30.0/3.6,
            ts=ts,
            leader=leader,
            leader_speed=leader_speed
            )
    
    params = get_driver_params()[['T', 'a', 'b', 'uc_limit']]
    params = np.exp(np.mean(np.log(params)))
    print(params.values)
    player, states, accels, glances = simulate_follower_states(s, params.values)
    player_speed = np.gradient(player)/dt
    gts = ts[glances]
    def pgts():
        for t in gts: plt.axvline(t, color='black', alpha=0.1)

    ax = plt.subplot(3,1,1)
    pgts()
    ests = [s.gaps() for s in states]
    color = 'blue'
    ps = np.percentile(ests, [50, 25, 75], axis=1)
    plt.fill_between(ts, ps[1], ps[2], color=color, alpha=0.3)
    #plt.plot(ts, ps[0], color=color)
    plt.plot(ts, leader - player - VEHICLE_LENGTH, '-', color=color)
    plt.ylabel('Distance (m)')
    plt.gca().xaxis.set_visible(False)

    plt.subplot(3,1,2, sharex=ax)
    pgts()
    ests = [s.leader_speeds() - s.ownspeeds() for s in states]
    ests = np.array(ests)*3.6
    color = 'red'
    ps = np.percentile(ests, [50, 25, 75], axis=1)
    plt.fill_between(ts, ps[1], ps[2], color=color, alpha=0.3)
    #plt.plot(ts, ps[0], color=color)
    plt.plot(ts, (leader_speed - player_speed)*3.6, '-', color=color)
    plt.ylabel('Rel. speed (km/h)')
    plt.gca().xaxis.set_visible(False)
    
    plt.subplot(3,1,3, sharex=ax)
    pgts()
    ests = accels
    accel = np.mean(ests, axis=1)
    color = 'green'
    ps = np.percentile(ests, [50, 25, 75], axis=1)
    plt.fill_between(ts, ps[1], ps[2], color=color, alpha=0.3)
    #plt.plot(ts, ps[0], color=color)
    plt.plot(ts, accel, '-', color=color)
    plt.ylabel('Accel. (m/s²)')
    plt.xlabel('Time (seconds)')
    plt.subplots_adjust(hspace=0, wspace=0)
    #plt.tight_layout()
    #cid = plt.gcf().canvas.mpl_connect('resize_event', lambda *args: plt.tight_layout())
    #plt.plot(ts, player_speed - leader_speed)
    plt.show()

def plot_follower_states():
    dt = 0.1
    ts = np.arange(0, 60, dt)
    leader_speed = np.repeat(10, len(ts))
    leader = 200 + np.cumsum(leader_speed*dt)
    accel = cfmodels.Idm(v0=80/3.6, T=2.0, a=2.0, b=1.5)
    limit = 2.0
    s = dict(
            dt=dt,
            x0=0.0,
            v0=0.0,
            ts=ts,
            leader=leader,
            leader_speed=leader_speed
            )
            
    player, states, accels, glances = simulate_follower_states(s, accel, limit)
    player_speed = np.gradient(player)/dt
    """
    plt.plot(ts, player_speed)
    plt.plot(ts, np.std(accels, axis=1))
    for i in np.flatnonzero(glances):
        plt.axvline(ts[i], color='black', alpha=0.1)
    plt.show()
    """
    print(len(glances))
    print(len(states))
    for i in range(2, len(states) - 1):
        t = ts[i]
        #if not glances[i+1]: continue
        if t < 40: continue
        gap = leader[i] - player[i] - VEHICLE_LENGTH
        relative_speed = leader_speed[i] - player_speed[i]
        plt.figure(1)
        plt.clf()
        state = states[i]
        plt.subplot(5,1,1)
        #plt.hist(state.ownspeeds(), bins=100, normed=True)
        plot_kde(state.ownspeeds(), (0, 20))
        plt.axvline(player_speed[i], alpha=0.3, color='black')
        plt.subplot(5,1,2)
        plot_kde(state.gaps(), (15, 35))
        plt.axvline(gap, alpha=0.3, color='black')
        plt.subplot(5,1,3)
        plot_kde(state.leader_speeds(), (0, 20))
        plt.axvline(leader_speed[i], alpha=0.3, color='black')
        plt.subplot(5,1,4)
        plot_kde(state.leader_speeds() - state.ownspeeds(), (-10, 10))
        plt.axvline(relative_speed, alpha=0.3, color='black')
        plt.subplot(5,1,5)
        plot_kde(accels[i], (-6, 2))
        plt.draw()
        plt.figure(2)
        plt.clf()
        
        plt.subplot(3,1,1)
        pred_flow = np.log(np.maximum(0.01, state.ownspeeds()))
        plot_kde(pred_flow)

        plt.subplot(3,1,2)
        driver_position = -2.0
        vehicle_width = 2.0
        pred_angle = 2*np.arctan((vehicle_width/2)/(state.gaps() - driver_position));
        plot_kde(np.degrees(pred_angle))

        plt.subplot(3,1,3)
        w = vehicle_width
        g = state.gaps() - driver_position
        dv = state.leader_speeds() - state.ownspeeds()
        #pred_angle_speed = -(4*w*dv)/(4*g*g + w*w)
        pred_angle_speed = cartesian_to_angle_speed(dv, w, g)
        plot_kde(np.degrees(pred_angle_speed))
        angle_speed = cartesian_to_angle_speed(relative_speed, w, gap)
        plt.axvline(np.degrees(angle_speed), color='black', alpha=0.3)
        
        plt.pause(0.1)

    plt.show()

def nonblind_uncertainty():
    data = datatools.get_malmi_sim('experiment == 8')
    for g, d in data.groupby(('session_id', 'block')):
        d['isBlind'] = False
        d = datatools.mangle_sim(d)
        mental = simulate_internal_state(d)
        #plt.plot(d.ts, d.player_speed)
        #plt.plot(d.ts, d.leader_speed)
        mlspeeds = np.array([s.leader_speeds() for s in mental])
        lspeed = np.percentile(mlspeeds, [25, 75], axis=1).T
        #plt.plot(d.ts, lspeed, '-')
        params, getmodel = fit_mental_idm(d, mental)
        s = {
            'dt': 0.1,
            'ts': d.ts.values,
            'leader': d.leader.values,
            'leader_speed': d.leader_speed.values,
            'v0': d.player_speed.iloc[0],
            'x0': d.player.iloc[0],
        }
        x, states, accels, glances = simulate_follower_states(s, getmodel(), 0.0)
        #al = np.std(accels, axis=1)
        print(g, np.median(np.std(accels, axis=1)))
        #ap = np.percentile(accels, [25, 75], axis=1).T
        #plt.plot(d.ts, ap[:,1] - ap[:,0], '-', alpha=0.5, color='black')
        #plt.plot(d.ts, np.std(accels, axis=1), '-', alpha=0.5, color='red')
        #accels = accel.vec(dt, predictor.ownspeeds(), predictor.gaps(), predictor.leader_speeds() - predictor.ownspeeds())
        plt.show()

def model_vs_driver():
    data = datatools.get_malmi_sim('experiment == 9')
    allstats = []
    limit_estimator = estimate_limit_estimator()
    prev_sid = None
    for g, d in data.groupby(('session_id', 'block')):
        if d.ts.iloc[-1] < 200: continue
        if g[0] != prev_sid:
            prev_sid = g[0]
            continue # Skip the first one as practice

        d = datatools.mangle_sim(d)
        dt = 0.1
        s = dict(
                x0=d.player.iloc[0],
                v0=d.player_speed[0],
                ts=d.ts.values,
                dt=dt,
                leader=d.leader.values,
                leader_speed=d.leader_speed.values
            )
        
        """
        thw = d['gap']/d['player_speed']
        thw[(d.gap < 1) | (d.player_speed < 1)] = np.nan
        thw = np.nanmedian(thw)
        a, b = np.percentile(d.player_accel, [90, 10])
        #l = 2.0
        """
        
        od = np.nanmedian(d.blind_dur)
        for retry in range(10):
            mental = simulate_internal_state(d)
            try:
                (thw, a, b), accel = fit_mental_idm(d, mental)
            except ValueError:
                continue
            b = -b
            break
        else:
            print(f"Skipping trial #{g}")
            continue

        uc_limit = limit_estimator.predict([[od, thw, a]])
        uc_limits = [uc_limit]*10
        uc_limits = uc_limits[:1]
        for l in uc_limits: #[1.0, 2.0, 3.0]:
            stats, model, sim_player = simulation_stats(s, thw, a, -b, l)

            gidx = np.flatnonzero(
                    np.diff(np.array(model.glances, dtype=int)) > 0
                    )
            gt = d.ts.values[gidx]
            bdur = np.diff(gt)
            gt = gt[:-1]
            plt.plot(gt, bdur, color='C0', alpha=0.5)


            gidx = d.blind_dur.notnull()
            #plt.show()
            od = np.nanmedian(d.blind_dur)
            stats['real_od_median'] = od
            stats['session_id'] = g[0]
            stats['block'] = g[1]
            stats['limit'] = l
            allstats.append(stats)
        gidx = d.blind_dur.notnull()
        plt.plot(d[gidx].ts, d[gidx].blind_dur, color='black')
        #plt.twinx()
        plt.plot(d.ts, (d.leader.values - sim_player - VEHICLE_LENGTH)/(np.gradient(sim_player)/dt), color='C1')
        plt.plot(d.ts, d.gap.values/(d.player_speed.values), '--', color='C1')
        plt.show()
        allstats.append(stats)

    #stats = pd.DataFrame.from_records(allstats)
    #stats.to_csv('sim_stats_est_limit.csv')
    #plt.plot(stats['real_od_median'], stats['od_median'], 'o')
    plt.show()

def anal_model_vs_driver():
    data = pd.read_csv('sim_stats_est_limit.csv')
    #data = data[data.limit == 2.0]
    #for d, sd in data.groupby('session_id'):
    #    plt.plot(sd.od_median, sd.real_od_median, '.')
    #    plt.show()
    
    plt.plot(data.od_median, data.real_od_median, 'k.', alpha=0.3)
    sagg = data.groupby('session_id').mean().reset_index()
    plt.plot(sagg['od_median'], sagg['real_od_median'], 'ko')
    rng = np.linspace(sagg.od_median.min(), sagg.od_median.max(), 10)
    plt.plot(rng, rng, '--')
    plt.show()

def gmean(x):
    return np.exp(np.nanmean(np.log(x)))

def fit_lagged_idm(d):
    thw = np.nanmedian(d.gap/d.player_speed)
    a, b = np.percentile(d.player_accel, [95, 5])
    b = -b
    lag = 0.1
    params = (lag, thw, a, b)
    #dt = np.gradient(d.ts)
    dt = 0.1

    def get_sim_speed(params):
        lag, thw, a, b = params
        model = cfmodels.Idm(T=thw, a=a, b=b)
        model = cfmodels.LaggingAccelerator(lag, model)
        sim_player = cfmodels.simulateFollower(model, d.player.iloc[0], d.player_speed.iloc[0],
            d.ts.values, d.leader.values, d.leader_speed.values)
        sim_speed = np.gradient(sim_player)/dt
        return sim_speed
    
    def get_speed_error(params):
        params = np.exp(params)
        return np.mean(np.abs(get_sim_speed(params) - d.player_speed))
    
    result = scipy.optimize.minimize(get_speed_error, np.log(params), method='Nelder-Mead')
    print(np.exp(result.x))
    params = np.exp(result.x)
    sim_speed = get_sim_speed(params)
    plt.plot(d.ts, sim_speed)
    plt.plot(d.ts, d.player_speed)
    plt.show()

def test_idm_fit():
    data = datatools.get_malmi_sim('experiment == 8')
    for g, d in data.groupby(('session_id', 'block')):
        d = datatools.mangle_sim(d)
        fit_lagged_idm(d)
        #d = datatools.mangle_sim(d)
        #plt.plot(d.ts, d.leader_speed)
        #plt.plot(d.ts, d.player_speed)
        #plt.show()

def sim_model_vs_field():
    data = datatools.get_malmi_field()
    ab = []
    thw = []
    stats = []
    for g, d in data.groupby(('session_id',)):
        dt = np.gradient(d.ts)
        gap = d.gap.copy()
        #gap[~d.block_type.isin([2])] = np.nan
        accel = np.gradient(d.wheelspeed)/dt
        accel = gaussian_filter1d(accel, 1/np.median(dt))
        #blind_ab = np.percentile(accel[accel.on_straight])
        #a, b = np.percentile([a, b])
        #print(d.block_type.unique())
        bi = (d.on_straight & (d.block_type == 3))
        a, b = np.percentile(accel[bi], [95, 5])
        bd = d[bi]
        od_median = np.nanmedian(bd.blind_dur)
        od_gmean = gmean(bd.blind_dur)
        valid = (d.gap > 1) & (d.wheelspeed > 1)
        thw = bd.gap/bd.wheelspeed
        thw_gmean = gmean(thw[valid])
        thw_median = np.nanmedian(thw[valid])
        stats.append(dict(
            a=a, b=b, od_median=od_median, od_gmean=od_gmean, thw_median=thw_median, thw_gmean=thw_gmean
            ))
        #plt.hist(accel[d.on_straight & (d.block_type == 2)], bins=100, normed=True, histtype='step')
        #plt.hist(accel[d.on_straight & (d.block_type == 3)], bins=100, normed=True, histtype='step')
        #plt.plot(d.ts, accel)
        #plt.plot(d.ts, d.gap)
        #plt.plot(d.ts, d.wheelspeed)
        #plt.show()
    #ab = np.array(ab)
    #plt.plot(ab[:,0], ab[:,1], '.')
    stats = pd.DataFrame.from_records(stats)
    plt.plot(stats.od_gmean, stats.thw_gmean, 'ko')
    #plt.plot(stats.od_median, stats.thw_median, 'ko')
    rng = np.min(stats.od_median), np.max(stats.od_median)
    plt.plot(rng, rng, '--')
    plt.show()

def analyze_accel_vs_attn():
    data = pd.read_csv('simattn.csv')
    for l, d in data.groupby('l'):
        d = d.groupby('thw').mean().reset_index()
        plt.plot(d.od, d.thw, '-', label=f"Uncertainty threshold {l}")
    plt.legend()
    rng = np.linspace(np.min(data.od), np.max(data.od))
    plt.plot(rng, rng, 'k--')
    plt.xlabel('Desired time-headway (s)')
    plt.ylabel('Median occlusion duration (s)')
    
    plt.figure()
    for l, d in data.groupby('l'):
        d = d.groupby('a').mean().reset_index()
        plt.plot(d.od, d.a, '-', label=f"Uncertainty threshold {l}")
    plt.legend()
    plt.xlabel('Desired max acceleration (m/s²)')
    plt.ylabel('Median occlusion duration (s)')
    
    data = pd.read_csv('simattn_pref.csv')
    
    #plt.plot(data['thw'], data['thw_emp'], '.')
    thwfit = np.poly1d(np.polyfit(data['thw'], data['thw_emp'], 1))

    plt.figure()

    agg = get_blind_aggregates()
    plt.plot(agg.od, agg.thw, 'k.', label='Trial', alpha=0.3)
    sagg = agg.groupby('session_id').mean()
    plt.plot(sagg.od, sagg.thw, 'ko', label='Participant')

    for l, d in data.groupby('l'):
        d = d.groupby('thw').mean().reset_index()
        plt.plot(d.od, thwfit(d.thw), '-', label=f"Uncertainty threshold {l}")
    rng = np.linspace(np.min(data.od), np.max(data.od))
    plt.plot(rng, rng, 'k--', label='y = x')
    
    plt.ylabel("Median time-headway (s)")
    plt.xlabel("Median occlusion duration (s)")
    plt.legend()
    plt.show()

import scipy.stats
def get_blind_aggregates():
    data = datatools.get_malmi_sim('experiment == 9')
    agg = []
    for g, d in data.groupby(('session_id', 'block')):
        d = datatools.mangle_sim(d)
        thw = d.gap/d.player_speed
        thw[(d.gap < 1) & (d.player_speed) < 0] = np.nan
        thw = np.nanmedian(thw)
        a, b = np.percentile(d.player_accel.values, [95, 5])
        od = np.nanmedian(d.blind_dur)
        agg.append(dict(
            session_id=g[0],
            block=g[1],
            duration=d.ts.iloc[-1] - d.ts.iloc[0],
            min_gap=np.min(d.gap),
            thw=thw,
            od=od,
            a=a,
            b=b,
            min_accel=d.player_accel.min
            ))

    agg = pd.DataFrame.from_records(agg)
    agg['accel_idr'] = agg.a - agg.b
    agg.query('duration > 200', inplace=True)
    
    return agg

def get_sim_aggregates(filter_bad=True):
    agg = pd.read_csv('.sim_stat_cache.csv')
    if filter_bad:
        agg = agg[agg.duration > 200]
    return agg
    data = datatools.get_malmi_sim('experiment == 8 or experiment == 9')
    agg = []
    for g, d in data.groupby(('session_id', 'block')):
        d = datatools.mangle_sim(d)
        is_blind = d.experiment.iloc[0] == 9
        thw = d.gap/d.player_speed
        thw[(d.gap < 1) & (d.player_speed < 1)] = np.nan
        thw = np.nanmedian(thw)
        a, b = np.percentile(d.player_accel.values, [95, 5])
        od = np.nanmedian(d.blind_dur)
        stats = follower_stats(d.ts.values, d.leader.values, d.player.values, d.isBlind.values)
        stats.update(dict(
            session_id=g[0],
            block=g[1],
            is_blind=is_blind,
            duration=d.ts.iloc[-1] - d.ts.iloc[0],
            min_gap=np.min(d.gap.values),
            min_accel=d.player_accel.min()
            ))
        agg.append(stats)
    agg = pd.DataFrame.from_records(agg)

    agg.to_csv('.sim_stat_cache.csv')
    agg = pd.read_csv('.sim_stat_cache.csv')
    if filter_bad:
        agg = agg[agg.duration > 200]
    return agg

def nanpercentile(x, ps):
    x = x[np.isfinite(x)]
    if len(x) == 0:
        return np.repeat(np.nan, len(ps))
    return np.percentile(x, ps)

def get_field_aggregates():
    #data = datatools.get_malmi_field('block_type == 2 or block_type == 3')
    data = datatools.get_malmi_field('block_type in [2, 3]')
    data.blind_dur[data.blind_dur < 0.3] = np.nan
    agg = []
    for g, d in data.groupby(('session_id', 'block_number')):
        #d = datatools.mangle_sim(d)
        d = d[d.wheelspeed.notnull()]
        d = d[d.ts.notnull()]
        is_blind = d.block_type.iloc[0] == 3
        dt = np.gradient(d.ts.values)
        d['wheelaccel'] = np.gradient(d.wheelspeed.values)/dt
        d.wheelaccel = gaussian_filter1d(d.wheelaccel, 0.5/np.median(dt))
        
        thw = d.gap/d.wheelspeed
        
        thw[(d.gap < 1) & (d.wheelspeed < 1)] = np.nan
        valid_thw = np.isfinite(thw)
        
        d = d[d.straight_number > 0]

        #thw = np.nanmedian(thw)

        a, b = np.percentile(d.wheelaccel.values, [95, 5])
        #glance_idx = np.flatnonzero(d.isBlind.values.astype(int).diff() > 1)
        #glance_t = d.ts.values[glance_idx]
        #glance_dur = np.diff(glance_t)
        #glance_idx = glance_idx[:-1]
        glance_durs = d.blind_dur.values
        glance_durs = glance_durs[np.isfinite(glance_durs)]
        accel = d.wheelaccel.values
        gap = d.gap.values
        accel_1, accel_5, accel_25, accel_50, accel_75, accel_95, accel_99 = nanpercentile(accel, [1, 5, 25, 50, 75, 95, 99])
        thw_5, thw_25, thw_50, thw_75, thw_95 = nanpercentile(thw[valid_thw], [5, 25, 50, 75, 95])
        od_5, od_25, od_50, od_75, od_95 = nanpercentile(glance_durs, [5, 25, 50, 75, 95])
        stats = dict(thw_median=thw_50,
            od_median=od_50,
            gap_min=np.min(gap),
            accel_1=accel_1, accel_5=accel_5, accel_25=accel_25, accel_50=accel_50, accel_75=accel_75, accel_95=accel_95, accel_99=accel_99,
            thw_5=thw_5, thw_25=thw_25, thw_50=thw_50, thw_75=thw_75, thw_95=thw_95,
            od_5=od_5, od_25=od_25, od_50=od_50, od_75=od_75, od_95=od_95,
            accel_min=np.min(accel))
        stats.update(dict(
            session_id=g[0],
            block=g[1],
            is_blind=is_blind,
            duration=d.ts.iloc[-1] - d.ts.iloc[0],
            min_gap=np.min(d.gap.values),
            min_accel=d.wheelaccel.min()
            ))
        agg.append(stats)
    agg = pd.DataFrame.from_records(agg)

    return agg

import statsmodels.api as sm
import statsmodels.formula.api as smf

def check_field_data():
    data = datatools.get_malmi_field('block_type in [2, 3]')
    #data = datatools.get_malmi_field()

    for s, sd in data.groupby(('session_id', 'block_number')):
        block_type = sd.block_type.iloc[0]
        
        plt.plot(sd['ts'], sd['straight_number'].notnull().astype(float)*0.5)
        plt.plot(sd['ts'], sd['on_straight'], alpha=0.5)
        sd['ts'][~(sd.straight_number > 0)] = np.nan
        plt.suptitle((s, block_type))
        plt.subplot(2,1,1)
        plt.plot(sd.ts, sd.gap, label='gap')

        plt.subplot(2,1,2)
        plt.plot(sd.ts, sd.thw, label='thw')
        bi = sd.blind_dur.notnull()
        plt.plot(sd.ts[bi], sd.blind_dur[bi], label='od')
        plt.legend()
        plt.show()
        
def check_sim_data():
    data = datatools.get_malmi_sim('experiment == 9')

    for s, sd in data.groupby(('session_id', 'block')):
        sd = datatools.mangle_sim(sd)

        valid = (sd.gap > 1) & (sd.player_speed > 1)
        sd['thw'] = sd.gap/sd.player_speed
        plt.plot(sd[valid].ts, sd[valid].thw, label='thw')
        bi = sd.blind_dur.notnull()
        plt.plot(sd.ts[bi], sd.blind_dur[bi], label='od')
        plt.legend()
        plt.show()

def get_aggregates():
    """
    field_aggregates = get_field_aggregates()
    field_aggregates.to_csv('.field_stat_cache.csv')
    means = field_aggregates.groupby(('session_id', 'is_blind')).mean().reset_index()
    nonblind, blind = means[~means.is_blind], means[means.is_blind]
    field = nonblind.merge(blind, on=('session_id'), suffixes=('', '_blind'))
    
    sim_aggregates = get_sim_aggregates()
    sim_aggregates.to_csv('.sim_stat_cache.csv')
    means = sim_aggregates.groupby(('session_id', 'is_blind')).mean().reset_index()
    nonblind, blind = means[~means.is_blind], means[means.is_blind]
    sim = nonblind.merge(blind, on=('session_id'), suffixes=('', '_blind'))
    
    data = field.merge(sim, on=('session_id'), suffixes=('', '_sim'))
    data.to_csv('.stat_cache.csv')
    """
    
    field = pd.read_csv('.field_stat_cache.csv')
    sim = pd.read_csv('.sim_stat_cache.csv')
    data = pd.read_csv('.stat_cache.csv')
    return data, field, sim

def getR():
    import rpy2.robjects as robj
    from rpy2.robjects.packages import importr
    #import rpy2.robjects.numpy2ri as numpy2ri
    #numpy2ri.activate()
    import rpy2.robjects.pandas2ri as pandas2ri
    pandas2ri.activate()
    R = robj.r
    R.library('deming')
    R.library('mcr')
    return R


def cmpplot(x, y, rng=None):
    R = getR()
    if rng is None:
        lim = np.array((min(np.min(x), np.min(y)), max(np.max(x), np.max(y))))
        diff = np.diff(lim)
        lim[0] -= diff*0.05
        lim[1] += diff*0.05
        rng = np.linspace(*lim, 100)
    
    fit = R.mcreg(x, y)
    res = R.calcResponse(fit, rng)
    X = res.rx(True, 'X')
    Y = res.rx(True, 'Y')
    lci = res.rx(True, 'Y.LCI')
    uci = res.rx(True, 'Y.UCI')
    
    plt.fill_between(X, lci, uci, color='black', alpha=0.1)
    plt.plot(rng, rng, 'k--', alpha=0.5)
    plt.plot(X, Y, 'k-')

    plt.plot(x, y, 'ko')
    
    lim = rng[0], rng[-1]
    plt.xlim(*lim)
    plt.ylim(*lim)

def check_free_flow():
    data = datatools.get_malmi_field('block_type in [2, 3]')
    for s, sd in data.groupby(('session_id', 'block_number')):
        block_type = sd.block_type.iloc[0]
        print(list(sd.columns))
        plt.plot(sd['ts'], sd['straight_number'].notnull().astype(float)*0.5)
        plt.plot(sd['ts'], sd['on_straight'], alpha=0.5)
        sd['ts'][~(sd.straight_number > 0)] = np.nan
        plt.suptitle((s, block_type))
        plt.subplot(2,1,1)
        plt.plot(sd.ts, sd.wheelspeed, label='speed')

        plt.subplot(2,1,2)
        plt.plot(sd.ts, sd.thw, label='thw')
        bi = sd.blind_dur.notnull()
        plt.plot(sd.ts[bi], sd.blind_dur[bi], label='od')
        plt.legend()
        plt.show()

def anal_measured_aggregates():
    data, field, sim = get_aggregates()
    field.query('is_blind', inplace=True)
    sim.query('is_blind', inplace=True)

    outliers = [17, 3, 18]
    inliers = data.query(f'session_id not in @outliers')
    outliers = data.query(f'session_id in @outliers')

    #data = data.query('session_id not in [17, 3, 18]')
    #x = data.od_blind
    #y = data.thw_blind
        #plt.subplot(1,3,1)
    """
    plt.subplot(1,3,1)
    plt.plot(data.thw_50_sim, data.thw_50, 'o')
    #for _, row in outliers.iterrows(): plt.text(row.thw_50_sim, row.thw_50, str(row.session_id))
    fit = smf.ols('thw_50 ~ thw_50_sim', data=data).fit()
    fit_in = smf.ols('thw_50 ~ thw_50_sim', data=inliers).fit()
    #rng = getrng(data.thw_50_sim, 30)
    rng = [0, 8]
    plt.plot(rng, rng, 'k--', alpha=0.5)
    plt.xlim(*rng); plt.ylim(*rng)
    #plt.plot(rng, fit.predict(dict(thw_50_sim=rng)), 'k')
    #plt.plot(rng, fit_in.predict(dict(thw_50_sim=rng)), 'k')
    plt.title('Non-occluded THW median (seconds)')
    plt.ylabel('Real car measurement')
    """
    
    """
    cmpplot(data.thw_50_blind_sim, data.thw_50_blind, rng=np.linspace(0, 12, 100))
    plt.show()
    
    fit = R.mcreg(data.thw_50_blind_sim, data.thw_50_blind)
    plt.plot(data.thw_50_blind_sim, data.thw_50_blind, 'o')
    rng = np.linspace(0, 12, 10)
    res = R.calcResponse(fit, rng)
    print(res)
    plt.plot(np.array(res.rx(True, 'X')), np.array(res.rx(True, 'Y')))
    plt.plot(np.array(res.rx(True, 'X')), np.array(res.rx(True, 'Y.LCI')))
    plt.plot(np.array(res.rx(True, 'X')), np.array(res.rx(True, 'Y.UCI')))
    plt.plot(rng, rng)
    print(np.array(res.rx(True, 'X')))
    plt.show()
    return
    """
    
    plt.subplot(1,3,1)
    cmpplot(data.thw_50_blind_sim, data.thw_50_blind, rng=np.linspace(0, 12, 100))
    print("THW", scipy.stats.pearsonr(data.thw_50_blind, data.thw_50_blind_sim))

    plt.title('Time headway median (seconds)')
    plt.ylabel('Real car measurement')
    
    plt.subplot(1,3,2)
    cmpplot(data.od_50_blind_sim, data.od_50_blind, rng=np.linspace(0, 7, 100))
    print("OD", scipy.stats.pearsonr(data.od_50_blind, data.od_50_blind_sim))
    plt.title('Occlusion duration median (seconds)')
    plt.xlabel('VR simulator measurement')
    
    plt.subplot(1,3,3)
    cmpplot(data.accel_99_blind_sim, data.accel_99_blind)
    print("Accel", scipy.stats.pearsonr(data.accel_99_blind, data.accel_99_blind_sim))
    plt.title('Acceleration 99th percentile (m/s²)')
    plt.gcf().canvas.mpl_connect('resize_event', lambda *args: plt.tight_layout())
    
    
    """
    plt.plot(data.od_50_blind_sim, data.od_50_blind, 'o')
    #fit = smf.ols('od_50_blind ~ od_50_blind_sim', data=data).fit()
    fit = R.pbreg('od_50_blind ~ od_50_blind_sim', data=data)
    fit = np.poly1d(list(R.coef(fit))[::-1])
    rng = [0, 8]
    plt.plot(rng, rng, 'k--', alpha=0.5)
    plt.xlim(*rng); plt.ylim(*rng)
    #for _, row in outliers.iterrows():
    #    plt.text(row.od_50_blind_sim, row.od_50_blind, str(row.session_id))
    #print(fit.summary())
    plt.plot(rng, fit(rng), 'k')
    #plt.plot(rng, fit.predict(dict(od_50_blind_sim=rng)), 'k')

    
    plt.subplot(1,3,3)
    plt.plot(data.accel_95_blind_sim, data.accel_95_blind, 'o')
    fit = smf.ols('accel_95_blind ~ accel_95_blind_sim', data=data).fit()
    rng = getrng(data.accel_95_blind_sim, 10)
    #rng = [0, 8]
    #plt.plot(rng, rng, 'k--', alpha=0.5)
    #plt.xlim(*rng); plt.ylim(*rng)
    #for _, row in outliers.iterrows():
    #    plt.text(row.accel_95_blind_sim, row.accel_95_blind, str(row.session_id))
    #print(fit.summary())
    plt.plot(rng, fit.predict(dict(accel_95_blind_sim=rng)), 'k')
    plt.title('Acceleration 95th percentile (m/s²)')
    """


    
    plt.figure()
    predfit = lambda x: np.array(x) - 1.0
    rng = np.linspace(0, 12, 100)
    plt.subplot(1,2,1)
    plt.title('Real car experiment')
    plt.xlabel('Time headway median (seconds)')
    plt.ylabel('Occlusion duration median (seconds)')
    cmpplot(data.thw_50_blind, data.od_50_blind, rng)
    print("Real od-thw", scipy.stats.pearsonr(data.thw_50_blind, data.od_50_blind))
    plt.plot(rng, predfit(rng), 'k-', alpha=0.5)
    """
    plt.plot(data.thw_50_blind, data.od_50_blind, 'C0o')
    #plt.plot(field.thw_50, field.od_50, 'C0.', alpha=0.5)
    print(scipy.stats.pearsonr(data.thw_50_blind, data.od_50_blind)[0])
    #rng = getrng(data.thw_50_blind, 10)
    rng = [0, 12]
    plt.plot(rng, rng, 'k--')
    plt.plot(rng, predfit(rng), 'k-', alpha=0.5)
    plt.xlim(*rng)
    plt.ylim(*rng)
    """

    plt.subplot(1,2,2)
    plt.title('Simulator experiment')
    plt.xlabel('Time headway median (seconds)')
    plt.ylabel('Occlusion duration median (seconds)')
    cmpplot(data.thw_50_blind_sim, data.od_50_blind_sim, rng)
    print("Sim od-thw", scipy.stats.pearsonr(data.thw_50_blind_sim, data.od_50_blind_sim))
    plt.plot(rng, predfit(rng), 'k-', alpha=0.5)
    """
    plt.plot(data.thw_50_blind_sim, data.od_50_blind_sim, 'C0o')
    #plt.plot(sim.thw_50, sim.od_50, 'C0.', alpha=0.5)
    print(scipy.stats.pearsonr(data.thw_50_blind_sim, data.od_50_blind_sim)[0])
    rng = [0, 12]
    plt.plot(rng, rng, 'k--')
    plt.plot(rng, predfit(rng), 'k-', alpha=0.5)
    plt.xlim(*rng)
    plt.ylim(*rng)
    """

    plt.gcf().canvas.mpl_connect('resize_event', lambda *args: plt.tight_layout())
    
    plt.show()


def thw_vs_od_timeseries_sim():
    R = getR()
    params = get_driver_params()
    sessions = get_leader_trajectories()
    stats = []
    for _, p in params.iterrows():
        ds = []
        #if not np.isfinite(p['T_sim']): continue
        for i in range(3):
            leader = np.random.choice(sessions)
            model = get_bidm(*p[['T', 'a', 'b', 'uc_limit']].values, v0=leader['v0'])[0]
            #model = get_bidm(*p[['T_sim', 'a_sim', 'b_sim', 'uc_limit_sim']].values, v0=leader['v0'])[0]
            d = simulate_trajectory(model, leader)
            if(d.gap.min() <= 0.0): continue
            ds.append(d)
        d = pd.concat(ds, axis=0, ignore_index=True)
        d['thw'] = d.gap/d.player_speed
        #plt.plot(d.ts, d.thw)
        #plt.show()
        d['thw'][(d.gap < 1) | (d.player_speed < 1)] = np.nan
        gi = d.blind_dur.notnull()
        gd = d[gi]
        valid = gd.blind_dur.notnull() & gd.thw.notnull()
        gd = gd[valid]
        gd['thw_trend'] = smf.rlm('thw ~ ts', data=gd).fit().predict(d)
        gd['blind_dur_trend'] = smf.rlm('blind_dur ~ ts', data=gd).fit().predict(gd)
        gd['thw_detrend'] = gd['thw'] - gd['thw_trend']
        gd['blind_dur_detrend'] = gd['blind_dur'] - gd['blind_dur_trend']
        #gap = leader['leader'] - player - VEHICLE_LENGTH
        #player_speed = np.gradient(player)/leader['dt']
        thw = d.gap/d.player_speed
        gi = np.flatnonzero(np.array(model.glances))
        gt = leader['ts'][gi]
        blind_dur = np.diff(gt) - 0.3
        gi = gi[:-1]
        gt = gt[:-1]
        x = gd.thw_detrend
        y = gd.blind_dur_detrend
        fit = R.pbreg('blind_dur_detrend ~ thw_detrend', data=gd)
        s = {}
        s['intercept'], s['slope'] = R.coef(fit)
        #plt.plot(leader['ts'], thw)
        #plt.plot(gt, blind_dur)
        #plt.plot(gd.thw_detrend, gd.blind_dur_detrend, '.')
        #plt.show()
        s['cor'] = scipy.stats.spearmanr(gd.thw_detrend, gd.blind_dur_detrend)[0]
        #plt.show()
        stats.append(s)
    
    stats = pd.DataFrame.from_records(stats)
    print(stats['cor'])
    stats = stats[stats['cor'] > 0.1]
    print(stats.median())
    plt.hist(stats['slope'])
    plt.show()

def plot_thw_vs_od_timeseries(data, is_sim=False):
    #sim = datatools.get_malmi_sim('experiment == 9')
    #field = datatools.get_malmi_field('block_type == 3')#; drop_after_last_glance(field)
    corrs = []
    #for g, d in sim.groupby(('session_id', 'block')):
    for s, sd in data.groupby('session_id'):
        thw_trends = []
        thws = []
        od_trends = []
        ods = []
        for b, d in sd.groupby('block'):
            d['ts'] -= d.ts.iloc[0]
            if d['ts'].iloc[-1] < 200: continue
            if is_sim:
                d = datatools.mangle_sim(d); d.query('player > 100', inplace=True)
            d['thw'] = d.gap/d.player_speed
            d['thw'][(d.gap < 1) | (d.player_speed < 1)] = np.nan
            gi = d.blind_dur.notnull()
            gd = d[gi]
        
            valid = gd.blind_dur.notnull() & gd.thw.notnull()
            gd = gd[valid]

            thw_trend = smf.rlm('thw ~ ts', data=gd).fit().predict(gd)
            od_trend = smf.rlm('blind_dur ~ ts', data=gd).fit().predict(gd)
            thw_dt = gd.thw - thw_trend
            od_dt = gd.blind_dur - od_trend
            thws.extend(thw_dt)
            ods.extend(od_dt)
            thw_trends.extend(thw_trend)
            od_trends.extend(od_trend)
            #plt.plot(gd.ts, gd.blind_dur - od_trend, color='red')
            #plt.plot(gd.ts, gd.thw - thw_trend, color='green')
            #plt.plot(thw_dt, od_dt, '.')
            #plt.show()
            """
            corr = scipy.stats.spearmanr(gd[valid].thw, gd[valid].blind_dur)[0]
            print(corr)
            corrs.append((*g, corr))
            plt.plot(gd.ts, gd.thw)
            plt.plot(gd.ts, gd.blind_dur)
            plt.show()
            """
        corrs.append((s, scipy.stats.spearmanr(thws, ods)[0]))
        #plt.plot(thws, ods, '.')
        #plt.show()
        
        #plt.plot(gd.blind_dur, gd.thw, 'ko', alpha=0.1)
    #plt.loglog()
    plt.show()
    corrs = pd.DataFrame.from_records(corrs, columns=['session_id', 'corr'])
    corrs = corrs[corrs['corr'].notnull()]
    #plt.hist(corrs['corr'], bins=20)
    #corrs = corrs.groupby('session_id').median().reset_index()
    plt.hist(corrs['corr'])
    print(np.median(corrs['corr']))
    print(len(corrs), (corrs['corr'] > 0).sum())

    #plt.xlim(-1, 1)
    plt.show()

def thw_vs_od_timeseries():
    plot_thw_vs_od_timeseries(datatools.get_malmi_sim('experiment == 9'), is_sim=True)
    return
    field = datatools.get_malmi_field('block_type == 3')
    field.query('straight_number > 0', inplace=True)
    field['player_speed'] = field.wheelspeed
    field['block'] = field.block_number
    plot_thw_vs_od_timeseries(field, is_sim=False)

def plot_blind_aggregates():
    plt.plot(agg.a, agg.b, 'k.', alpha=0.3, label='Trial')
    sagg = agg.groupby('session_id').mean()
    plt.plot(sagg.a, sagg.b, 'ko', label='Participant')
    print(scipy.stats.pearsonr(sagg.a, sagg.b))
    plt.ylabel('10th percentile acceleration (m/s²)')
    plt.xlabel('90th percentile acceleration (m/s²)')
    fit = np.poly1d(np.polyfit(sagg.a, sagg.b, 1))
    
    rng = np.array([sagg.a.min(), sagg.a.max()])
    label = f"y = {fit[1]:.1f}x - {-fit[0]:.1f}"
    plt.plot(rng, fit(rng), 'k-', label=label)
    plt.legend()
    plt.figure()

    plt.plot(agg.thw, agg.accel_idr, 'k.', alpha=0.3, label='Trial')
    plt.plot(sagg.thw, sagg.accel_idr, 'ko', label='Participant')
    
    logthw = np.log2(sagg.thw)
    loga = np.log2(sagg.accel_idr)
    print(scipy.stats.pearsonr(logthw, loga))
    #plt.plot(logthw, loga, '.')
    fit = np.poly1d(np.polyfit(logthw, loga, 1))

    thwrng = np.linspace(np.min(sagg.thw), np.max(sagg.thw), 100)
    print(fit)
    label = f"log_2(y) = {fit[1]:.1f}log_2(x) + {fit[0]:.1f}"
    plt.plot(thwrng, 2**fit(np.log2(thwrng)), 'k-', label=label)
    plt.ylabel('Acceleration interdecile range (m/s²)')
    plt.xlabel('Median time-headway (s)')
    #plt.loglog(base=2)

    plt.legend()
    
    
    plt.show()


def compare():
    data = datatools.get_malmi_sim('experiment == 8')
    for g, gd in data.groupby(('session_id', 'block')):
        gd = datatools.mangle_sim(gd)

        mental = simulate_internal_state(gd)
        #mental_gap = np.array([np.mean(p.gaps()) for p in mental])
        #mental_leader_speed = np.array([np.mean(p.leader_speeds()) for p in mental])
        accel = fit_mental_idm(gd, mental)
        continue
        msess = simulate_mental_session(accel(), gd, mental)
        sd = simulate_session(accel(), gd)
        """
        plt.plot(msess.ts, msess.player_speed)
        plt.plot(gd.ts, gd.player_speed)
        plt.plot(sd.ts, sd.player_speed, '--')
        plt.show()
        """
        
        for limit in [1.0, 1.5, 2]:
            state_est = cfmodels.FollowStatePredictor(
                    cfmodels.FollowState(gd.player_speed[0], gd.gap.iloc[0], gd.leader_speed.iloc[0]),
                    1024)
            model = cfmodels.DistractedAccelerator(limit, state_est, accel())
            bsd = simulate_session(model, gd)
            bsd['isBlind'] = ~np.array([True] + list(model.glances), dtype=bool)
            bsd = datatools.compute_stuffs(bsd)
            gt = bsd.blind_dur.notnull()
            plt.plot(bsd.ts[gt], bsd.blind_dur[gt], '-', label=limit)

        #plt.plot(sd.ts, sd.player_speed)
        #plt.plot(gd.ts, gd.player_speed)
        #plt.plot(bsd.ts, bsd.player_speed)
        #plt.twinx()
        #plt.plot(bsd.ts, bsd.isBlind, color='black', alpha=0.1)
        
        mental_ownspeeds = np.array([s.ownspeeds() for s in mental])
        mental_gaps = np.array([s.gaps() for s in mental])
        mental_thws = mental_gaps/mental_ownspeeds
        mental_thws[mental_gaps < 1] = np.nan
        mental_thws[mental_ownspeeds < 1] = np.nan
        mental_thw = np.nanmedian(mental_thws, axis=1)
        gt = gd.blind_dur.notnull()
        plt.plot(gd.ts[gt], gd.blind_dur[gt], 'k-')
        
        plt.axhline(np.nanmedian(mental_thw), color='black', linestyle='dashed')
        #plt.plot(gd.ts, mental_thw, 'k--')
        plt.show()

def anal_leader_accels():
    sessions = get_leader_trajectories()
    dt = sessions[0]['dt']
    n_samples = 1000
    def get_random_speed_traj():
        s = np.random.choice(sessions)['leader_speed']
        start = np.random.randint(0, len(s) - n_samples - 1)
        return s[start:(start + n_samples)]

    speeds = np.array([get_random_speed_traj() for i in range(1000)])
    print(speeds.shape)
    t = np.arange(speeds.shape[1])*dt
    speeds = speeds - speeds[:,0].reshape(-1, 1)
    #plt.plot(t, speeds.T, color='black', alpha=0.1)
    std = np.std(speeds, axis=0)
    plt.plot(t, std)

    gspeeds = np.random.normal(size=(2000, n_samples))*4.0
    gspeeds = np.cumsum(gspeeds*dt, axis=1)
    plt.plot(t, np.std(gspeeds, axis=0))

    plt.show()
    """
    for s in sessions:
        accel = np.gradient(s['leader_speed']/s['dt'])
        print(np.std(accel))
        plt.hist(accel)
        plt.show()
    """

def plot_simulated_aggregates(is_sim=False):
    params = get_driver_params().copy()
    if is_sim:
        for p in ['T', 'a', 'b', 'uc_limit', 'thw_50', 'od_50']:
            params[p] = params[p + "_sim"]
        params = params[params.notnull().all(axis=1)]
    logUdist = scipy.stats.norm(*scipy.stats.norm.fit(np.log(params['uc_limit'])))
    Trng = np.linspace(0.1, 11, 10)
    params['rel_uc_limit'] = params['uc_limit']#/params['a']
    mean_driver = np.exp(np.log(params[['T', 'a', 'b', 'uc_limit']]).mean())
    print(mean_driver)
    uPs = np.exp(logUdist.ppf([0.25, 0.5, 0.75]))
    
    driver_range = pd.DataFrame.from_records([
            dict(T=T, a=mean_driver.a, b=mean_driver.b, uc_limit=uc)
            for T, uc in itertools.product(Trng, uPs)
            ])
    
    stats = simulate_params(driver_range, 100)
    stats = filter_unrealistic_stats(stats)
    stats['rel_uc_limit'] = stats['uc_limit']#/stats['a']
    from matplotlib.colors import LogNorm
    sctr = plt.scatter(params.thw_50, params.od_50, c=params.rel_uc_limit,
            #norm=LogNorm(vmin=params.uc_limit.min(), vmax=params.uc_limit.max())
            #vmin=0.0, vmax=1.5
            vmin=0.0, vmax=6
            )
    #plt.plot(saggs.thw_50_blind, saggs.od_50_blind, 'ko', alpha=0.3)
    #plt.plot(saggs.thw_50_blind_sim, saggs.od_50_blind_sim, 'ko', alpha=0.3)
    lim = [0, 12]
    plt.plot(lim, lim, 'k--', alpha=0.5)
    
    #Trng = np.linspace(0.0, 12, 10)
    #plt.plot(Trng, 0.9*Trng**(0.7), color='black')
    
    gmean = lambda x: np.exp(np.mean(np.log(x)))
    for uc, s in stats.groupby('rel_uc_limit'):
        gs = s.groupby('T').agg(gmean).reset_index()
        plt.plot(gs.thw_50, gs.od_50, color=sctr.to_rgba(uc))
    plt.xlim(*lim)
    plt.ylim([0, 7])


def anal_sim_aggregates():
    plt.set_cmap('viridis_r')
    
    plt.subplot(1,2,1)
    plt.title('Real car')
    plot_simulated_aggregates(is_sim=False)
    plt.xlabel('Time headway median (seconds)')
    plt.ylabel('Occlusion duration median (seconds)')
    
    plt.subplot(1,2,2)
    plt.title('VR simulator')
    plot_simulated_aggregates(is_sim=True)
    plt.gca().yaxis.set_visible(False)
    plt.xlabel('Time headway median (seconds)')
    #plt.ylabel('Occlusion duration median (seconds)')
    
    plt.colorbar(ax=(plt.subplot(1,2,1), plt.subplot(1,2,2))).set_label('Uncertainty threshold (m/s²)')
    plt.show()

def anal_sim_aggregates_old():
    from scipy.interpolate import interp2d
    stats = load_stats('simulated_trials_grid_10.json')
    #stats = stats[stats.al == stats.al.unique()[3]]
    #stats = stats[stats.uc_limit == stats.uc_limit.unique()[3]]
    plt.set_cmap('viridis_r')
    #plt.plot(stats['thw_50'], stats['od_50'], '.')
    #for T, s in stats.groupby('uc_limit'):
    #    plt.scatter(s['thw_50'], s['od_50'], c=s['al'])
    #plt.show()
    s = stats
    #print(stats['uc_limit'])
    #plt.scatter(stats['uc_limit'], stats['od_50'])
    print((stats.gap_min < 0).sum()/len(stats))
    stats = stats[stats.a == stats.a.unique()[4]]
    

    stats['has_crashed'] = (stats.gap_min < 0) | (stats.accel_min < -20)
    stats = stats[~stats.has_crashed]
    
    params = get_driver_params()
    logUdist = scipy.stats.norm(*scipy.stats.norm.fit(np.log(params['uc_limit'])))
    Trng = np.linspace(0.1, 10, 10)
    mean_driver = np.exp(np.log(params[['T', 'a', 'b', 'uc_limit']]).mean())
    print(mean_driver)
    uPs = np.exp(logUdist.ppf([0.05, 0.5, 0.95]))
    
    driver_range = pd.DataFrame.from_records([
            dict(T=T, a=mean_driver.a, b=mean_driver.b, uc_limit=uc)
            for T, uc in itertools.product(Trng, uPs)
            ])
    
    stats = simulate_params(driver_range, 10)
    stats = filter_unrealistic_stats(stats)


    from matplotlib.colors import LogNorm
    sctr = plt.scatter(params.thw_50, params.od_50, c=params.uc_limit,
            #norm=LogNorm(vmin=params.uc_limit.min(), vmax=params.uc_limit.max())
            )
    #plt.plot(saggs.thw_50_blind, saggs.od_50_blind, 'ko', alpha=0.3)
    #plt.plot(saggs.thw_50_blind_sim, saggs.od_50_blind_sim, 'ko', alpha=0.3)
    lim = [0, 10]
    plt.plot(lim, lim, 'k--', alpha=0.5)
    
    #Trng = np.linspace(0.0, 12, 10)
    #plt.plot(Trng, 0.9*Trng**(0.7), color='black')
    
    gmean = lambda x: np.exp(np.mean(np.log(x)))
    for uc, s in stats.groupby('uc_limit'):
        print(s['T'])
        gs = s.groupby('T').agg(gmean).reset_index()
        plt.plot(gs.thw_50, gs.od_50, color=sctr.to_rgba(uc))
    plt.xlim(*lim)
    plt.ylim(*lim)
    plt.xlabel('Time headway median (seconds)')
    plt.ylabel('Occlusion duration median (seconds)')
    plt.colorbar().set_label('Uncertainty threshold (m/s²)')
    #plt.scatter(stats['T'], stats['thw_50'], c=stats['a'])
    plt.show()

def get_driver_params():
    return pd.read_csv('.driver_params_cache.csv').reset_index()

def estimate_driver_params():
    stats, field, sim = get_aggregates()
    
    model_fitter = get_bidm_estimator()
    
    field = field[field.is_blind].reset_index()
    sim = sim[sim.is_blind].reset_index()

    #field = field[field.is_blind].groupby('session_id').mean().reset_index()
    field = pd.concat((field, model_fitter(field)), axis=1)
    #sim = sim[sim.is_blind].groupby('session_id').mean().reset_index()
    sim = pd.concat((sim, model_fitter(sim)), axis=1)
    
    field = field.groupby('session_id').mean().reset_index()
    sim = sim.groupby('session_id').mean().reset_index()
    
    stats = field.merge(sim, how='left', on='session_id', suffixes=('', '_sim'))

    stats.to_csv('.driver_params_cache.csv')

    plt.plot(stats['T'], stats.thw_50, '.')
    plt.show()
    
    x = (stats['T'])
    y = (stats['T_sim'])
    valid = x.notnull() & y.notnull()
    x = x[valid]; y = y[valid]
    print(scipy.stats.spearmanr(x, y))
    plt.plot(x, y, '.')
    
    x = (stats['uc_limit'])
    y = (stats['uc_limit_sim'])
    valid = x.notnull() & y.notnull()
    x = x[valid]; y = y[valid]
    print(scipy.stats.spearmanr(x, y))
    
    x = (stats['a'])
    y = (stats['a_sim'])
    valid = x.notnull() & y.notnull()
    x = x[valid]; y = y[valid]
    print(scipy.stats.spearmanr(x, y))
    plt.show()


    params = stats[['T', 'a', 'uc_limit']]
    logparams = np.log(params)
    means = logparams.mean()
    cov = logparams.cov()
    print(np.exp(means))
    print(cov)
    plt.hist(logparams['T'])
    plt.show()

    #plt.hist(np.log(stats['T']), bins=15)
    #plt.show()
    #plt.scatter(np.log(stats['T']), np.log(stats['a']), c=np.log(stats['uc_limit']))
    #print(scipy.stats.spearmanr(stats['T'], stats['a']))
    #plt.show()

def plot_driver_samples():
    params = get_driver_params()
    params = params[params[['T', 'T_sim']].notnull().all(axis=1)]
    
    """
    plt.subplot(1,2,1)
    plt.scatter(params.thw_50, params.od_50)
    for _, p in params.iterrows(): plt.annotate(p.session_id, (p.thw_50, p.od_50))
    plt.subplot(1,2,2)
    plt.scatter(params.thw_50_sim, params.od_50_sim)
    for _, p in params.iterrows(): plt.annotate(p.session_id, (p.thw_50_sim, p.od_50_sim))
    plt.show()
    """
    
    subjs = [20, 27, 17]
    params = params[params.session_id.isin(subjs)]
    params.sort_values('T', inplace=True)

    data = datatools.get_malmi_sim(f'experiment == 9 and session_id in {subjs}')
    
    ncols = len(params)
    sax = plt.subplot2grid((3, ncols), (0, 0))
    tax = plt.subplot2grid((3, ncols), (1, 0))
    bax = plt.subplot2grid((3, ncols), (2, 0))
    labels = []
    for i, (_, param) in enumerate(params.iterrows()):
        s = param.session_id
        sd = data.query('session_id == @s')
        blocks = sd.block.unique()
        d = sd[sd.block == blocks[-1]]
        d = datatools.mangle_sim(d)
        d['thw'] = d.gap/d.player_speed
        gd = d[d.blind_dur.notnull()]
        #plt.plot(d.ts, d.thw)

        sax = plt.subplot2grid((3, ncols), (0, i), sharey=sax)
        tax = plt.subplot2grid((3, ncols), (1, i), sharex=sax, sharey=tax)
        bax = plt.subplot2grid((3, ncols), (2, i), sharex=sax, sharey=bax)
        sax.set_xlim(d.ts.iloc[0], d.ts.iloc[-1])
        sax.get_xaxis().set_visible(False)
        tax.get_xaxis().set_visible(False)
        sax.get_yaxis().set_visible(i == 0)
        tax.get_yaxis().set_visible(i == 0)
        bax.get_yaxis().set_visible(i == 0)

        if i == 0:
            sax.set_ylabel('Speed (km/h)')
            tax.set_ylabel('THW (s)')
            bax.set_ylabel('Occl. dur. (s)')
        if i == 1:
            bax.set_xlabel('Time (s)')

        
        param = params[params.session_id == s].iloc[0]
        n_sims = 3
        alpha = max(1.0/n_sims, 0.01)
        
        for j in range(n_sims):
            model, state_est, accel = get_bidm(
                    *param[['T_sim', 'a_sim', 'b_sim', 'uc_limit_sim']],
                    v0=d.player_speed.iloc[0])
            sd = simulate_session(model, d)
            if np.min(sd['gap']) <= 0.0:
                print("Crash!")
                continue
            sd['thw'] = sd.gap/sd.player_speed
            sax.plot(sd.ts, sd.player_speed*3.6, 'g-', alpha=alpha)
            tax.plot(sd.ts, sd.thw, 'b-', alpha=alpha)
            bsd = sd[sd.blind_dur.notnull()]
            bax.plot(bsd.ts, bsd.blind_dur, 'r-', alpha=alpha)
            
        sd = simulate_session(accel, d)
        sd['thw'] = sd.gap/sd.player_speed
        p = sax.plot(sd.ts, sd.player_speed*3.6, 'k--', alpha=0.5)
        if(i == 0): labels.append(('IDM', p[0]))
        tax.plot(sd.ts, sd.thw, 'k--', alpha=0.5)
        
        #sax.plot(d.ts, d.leader_speed*3.6, '--', color='orange', alpha=0.5)
        p = sax.plot(d.ts, d.player_speed*3.6, color='black', alpha=0.7)
        if(i == 0): labels.append(('Subject', p[0]))
        tax.plot(d.ts, d.thw, color='black', alpha=0.7)
        bax.plot(gd.ts, gd.blind_dur, color='black', alpha=0.7)
        #plt.plot(d.ts, d.leader_speed)
    labels.append(('Proposed model', plt.plot([], [], 'r-', alpha=0.5)[0]))
    names, ps = zip(*labels[::-1])
    #sax = plt.subplot2grid((3, ncols), (0, 1))
    plt.figlegend(ps, names, loc='upper center',
            ncol=len(ps),
            #bbox_to_anchor=(0., 1.02, 1., .102),
            #borderaxespad=0.0
            )
    sax.set_ylim(10, 80)
    tax.set_ylim(0, 30)
    bax.set_ylim(0, 14)
    plt.subplots_adjust(wspace=0.01, hspace=0)
    plt.show()

def estimate_crash_rate():
    n_repeats = 100
    params = get_driver_params()
    
    #plt.plot(params.thw_50, params.od_50, 'g.')
    #plt.plot(params.thw_50_sim, params.od_50_sim, 'r.')

    #stats = simulate_params(params, n_repeats)
    #stats.to_csv('.subj_param_sim_cache_phys.csv')
    stats = pd.read_csv('.subj_param_sim_cache.csv')
    #stats = pd.read_csv('.subj_param_sim_cache_phys.csv')
    stats['has_crashed'] = (stats.gap_min <= 0)
    crash_rate = stats.groupby('session_id_orig')['has_crashed'].agg(lambda x: float(x.sum())/len(x)).reset_index()
    field_crash_global = stats.has_crashed.sum()/len(stats)
    print(field_crash_global*100)
    
    simparams = params.copy()
    for p in ['T', 'a', 'b', 'uc_limit']:
        simparams[p] = simparams[p + "_sim"]

    #simstats = simulate_params(simparams, n_repeats)
    #simstats.to_csv('.subj_sim_param_sim_cache_phys.csv')
    simstats = pd.read_csv('.subj_sim_param_sim_cache.csv')
    #simstats = pd.read_csv('.subj_sim_param_sim_cache_phys.csv')
    simstats = simstats[simstats['T'].notnull()]
    simstats['has_crashed'] = (simstats.gap_min <= 0)

    #plt.plot(stats[~stats.has_crashed]['T'], stats[~stats.has_crashed]['thw_50'], '.')
    #plt.show()
    
    sim_crash_global = simstats.has_crashed.sum()/len(simstats)
    print(sim_crash_global*100)
    crash_rate_sim = simstats.groupby('session_id_orig')['has_crashed'].agg(lambda x: float(x.sum())/len(x)).reset_index()
    crash_rate = crash_rate.merge(crash_rate_sim, on='session_id_orig', suffixes=('', '_sim'))
    crash_rate = crash_rate.merge(params, left_on='session_id_orig', right_on='session_id')
    plt.plot(crash_rate.has_crashed, crash_rate.has_crashed_sim, '.')
    print(scipy.stats.spearmanr(crash_rate.has_crashed, crash_rate.has_crashed_sim))
    
    #diff = crash_rate['has_crashed'] - crash_rate['has_crashed_sim']
    #plt.hist(diff)
    #plt.plot(crash_rate['uc_limit']/crash_rate['a'], crash_rate['has_crashed'], 'g.')
    #plt.plot(crash_rate['uc_limit_sim']/crash_rate['a_sim'], crash_rate['has_crashed_sim'], 'r.')
    
    plt.show()
    #print(crash_rate.mean())
    print(scipy.stats.spearmanr(crash_rate.has_crashed, crash_rate.has_crashed_sim))

    true_sim = get_sim_aggregates(filter_bad=False).query('is_blind')
    true_road = get_aggregates()[1].query('is_blind')
    from statsmodels.stats.proportion import proportion_confint
    sim_crashes = (true_sim['gap_min'] <= 0.0).sum()
    road_crashes = (true_road['gap_min'] <= 0.0).sum()
    print("Simcrash", scipy.stats.binom_test(sim_crashes, len(true_sim), p=sim_crash_global))
    print("Simcrash", 100*np.array(proportion_confint(sim_crashes, len(true_sim), method='beta')))
    print("Roadcrash", scipy.stats.binom_test(road_crashes, len(true_road), p=field_crash_global))
    print("Roadcrash", 100*np.array(proportion_confint(road_crashes, len(true_road), method='beta')))
    print((true_sim['gap_min'] <= 0.0).sum()/len(true_sim), len(true_sim))
    print((true_road['gap_min'] <= 0.0).sum()/len(true_road), len(true_road))
    plt.show()


def estimate_car_physics():
    data = datatools.get_malmi_sim()
    for s, d in data.groupby(('session_id', 'block')):
        d = datatools.mangle_sim(d)
        plt.scatter(d.throttle - d.brake, d.player_accel, marker='.', c=d.player_speed, vmin=-10/3.6, vmax=80/3.6)
        #plt.scatter(d.throttle - d.brake, d.player_speed, marker='.', c=d.player_accel, vmin=-25, vmax=25)
        #plt.colorbar()
    plt.show()

def analyze_uc_vs_od():
    params = get_driver_params()
    pgrid = load_stats('simulated_trials_grid_20.json')
    pgrid = filter_unrealistic_stats(pgrid)
    pgrid = pgrid[pgrid.a > 1.0]
    #plt.plot(pgrid.thw_50, pgrid.od_50, '.', alpha=0.1)
    p = pgrid
    
    #plt.scatter(np.log(p.uc_limit), diff, marker='.', c=p.a)
    #plt.show()

    #plt.plot(
    #plt.scatter(np.log(p.thw_50), np.log(p.od_50), c=p.uc_limit/p.a, marker='.', alpha=0.5)
    #plt.colorbar()
    #plt.show()


def estimate_danger_zone():
    pgrid = load_stats('simulated_trials_grid_20.json')
    pgrid['has_crashed'] = (pgrid.gap_min <= 0).astype(float)
    pgrid = pgrid[pgrid.a > 1.0]
    pgrid = pgrid[pgrid.uc_limit > 0.0]
    pgrid = pgrid[pgrid['T'] > 0.1]
    ad = pgrid
    cr = ad.groupby(('T', 'uc_limit')).mean().reset_index()
    print(cr)
    plt.scatter(cr['thw_50'], cr['od_50'], c=cr['has_crashed'])
    plt.plot([0, 10], [0, 10])
    plt.show()

if __name__ == '__main__':
    #plot_follower_states()
    #test_idm_fit()
    #plot_driver_samples()
    #estimate_crash_rate()
    #analyze_uc_vs_od()
    #anal_sim_aggregates()
    #estimate_danger_zone()
    #estimate_car_physics()
    #anal_leader_accels()
    #nonblind_uncertainty()
    #sim_model_vs_field()
    #simulate_dataset()
    #simperf()
    #plot_simulated_results()
    #test_bidm_estimator()
    #calibrate_subject_model()
    #thw_vs_od()
    #od_thw_space_plot()
    #anal_measured_aggregates()
    check_free_flow()
    #thw_vs_od_timeseries()
    #thw_vs_od_timeseries_sim()
    #check_field_data()
    #check_sim_data()
    #estimate_limit_estimator()
    #model_vs_driver()
    #anal_model_vs_driver()
    #analyze_accel_vs_attn()
    #simulate_accel_vs_blink_rate()
    #simulate_pref_vs_blink_rate()
    #get_blind_aggregates()
    #compare()
    #estimate_driver_params()
    #approach_demo()
