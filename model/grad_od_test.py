import matplotlib.pyplot as plt
import numpy as np
import cfmodels
import scipy.optimize
import scipy.stats
from scipy.interpolate import interp1d
import inspect

def fit_idm(ts, player, init_speed, leader, leader_speed, blind=None):
    dt = np.gradient(ts)
    player_speed = np.gradient(player)/dt
    player_accel = np.gradient(player_speed)/dt
    gap = leader - player
    valid = player_speed > 1.0
    thw = 1.0/np.mean(player_speed[valid]/gap[valid])
    a, b = np.percentile(player_accel, [99, 1])
    b = -b
    
    plt.plot(ts, player_speed)
    plt.twinx()
    plt.plot(ts, gap, color='red')
    plt.show()

    print(thw, a, b)
    #a = 2.0
    #b = 2.0

    def fit_distance(thw, a, b):
        model = cfmodels.Idm(T=thw, a=a, b=b)
        sim_player = cfmodels.simulateFollower(model, player[0], init_speed, ts, leader, leader_speed)
        #sim_speed = np.gradient(sim_player)/dt
        return sim_player

    def fiterror(args):
        sim_distance = fit_distance(*args)
        return sim_distance - player
    
    res = scipy.optimize.least_squares(fiterror, (thw, a, b),
            bounds=([0.5, 0.5, 0.5], [np.inf, np.inf, np.inf]),
            )
    if not res.success:
        return None
    print(res)
    thw, a, b = res.x
    sim_distance = fit_distance(thw, a, b)
    sim_speed = np.gradient(sim_distance)/dt
    return cfmodels.Idm(T=thw, a=a, b=b), sim_speed

def fit_idm_accel(v, dx, dv, accel):
    init = [120/3.6, 2.0]
    def get_accel(params):
        return cfmodels.Idm(*params, a=2.0, b=2.0).vec(0.1, v, dx, dv)

    def get_error(params):
        return get_accel(params) - accel
    
    bounds = list(zip(*[[0.0, np.inf]]*len(init)))
    fit = scipy.optimize.least_squares(get_error, init, bounds=bounds)
    plt.plot(get_accel(fit.x))
    plt.plot(accel)
    plt.twinx()
    plt.plot(dv)
    print(fit.x)
    plt.show()
    return cfmodels.Idm(*fit.x)

def fit_fvdm_trajectory(ts, player, init_speed, leader, leader_speed):
    dt = np.gradient(ts)
    player_speed = np.gradient(player)/dt
    player_accel = np.gradient(player_speed)/dt
    gap = leader - player

    def fit_distance(*params):
        model = cfmodels.Fvdm(*params)
        sim_player = cfmodels.simulateFollower(model, player[0], init_speed, ts, leader, leader_speed)
        return sim_player
    
    def fiterror(args):
        sim_distance = fit_distance(*args, 0.0)
        #sim_speed = np.gradient(sim_distance)/dt
        return sim_distance - player
    
    #init_params = [6.75, 7.91, 0.13, 1.57, 0.41, 0.4]
    init_params = [6.75, 7.91, 0.13, 1.57, 0.41]
    _, optvel_p = fit_optimal_velocity(gap, player_speed)
    #init_params = list(optvel_p) + [0.41, 0.4]

    bounds = list(zip(*[[0.0, np.inf]]*len(init_params)))
    res = scipy.optimize.least_squares(fiterror, init_params, bounds=bounds)
    
    sim_distance = fit_distance(*res.x)
    sim_speed = np.gradient(sim_distance)/dt
    print(res)
    return cfmodels.Fvdm(*res.x, 0.0), sim_speed

def fit_lagged_stimulus(ts, relspeed, accel):
    prec = 0.1
    max_lag = 3.0
    
    relspeedint = scipy.interpolate.interp1d(ts, relspeed, bounds_error=False)
    corrs = []
    lags = np.arange(-max_lag, max_lag, prec)
    for lag in lags:
        r = relspeedint(ts - lag)
        valid = np.isfinite(r)
        c = scipy.stats.spearmanr(r[valid], accel[valid])[0]
        corrs.append(c)
    lag = lags[np.argmax(corrs)]
    plt.plot(ts, relspeedint(ts - lag))
    return (np.max(corrs), lag)


def simulate_mental_state(ts, player, leader, is_blind):
    dt = 0.1
    nts = np.arange(ts[0], ts[-1], dt)
    player = interp1d(ts, player)(nts)
    leader = interp1d(ts, leader)(nts)
    is_blind = interp1d(ts, is_blind, kind='nearest')(nts)
    ts = nts
    
    player_speed = np.gradient(player)/dt
    player_accel = np.gradient(player_speed)/dt
    leader_speed = np.gradient(leader)/dt
    gap = leader - player
    relspeed = leader_speed - player_speed
    
    initial_state = cfmodels.FollowState(player_speed[0], gap[0], leader_speed[0])
    predictor = cfmodels.FollowStatePredictor(initial_state, 2048)
    
    particles = []
    states = []
    for i in range(len(ts)):
        predictor.predict(dt, player_accel[i])
        #predictor.predict_known(dt,
        #        cfmodels.FollowState(player_speed[i], gap[i], leader_speed[i]))
        predictor.measure_ownspeed(player_speed[i])
        if not is_blind[i]:
            predictor.measure_gap(gap[i])
            predictor.measure_relspeed(relspeed[i], gap[i])
        predictor.finish_step()
        particles.append([
            predictor.weights().copy(),
            predictor.ownspeeds().copy(),
            predictor.gaps().copy(),
            predictor.leader_speeds().copy(),
        ])
        states.append(predictor.clone())

    particles = np.array(particles)
    #plt.plot(ts, particles[:,1], '.', alpha=0.01)
    estspeed = np.mean(particles[:,1], axis=1)
    estgap = np.mean(particles[:,2], axis=1)
    est_leaderspeed = np.mean(particles[:,3], axis=1)
    
    relspeeds = particles[:,3] - particles[:,1]
    est_relspeed = np.mean(relspeeds, axis=1)

    plt.subplot(3,1,1)
    plt.plot(ts, est_leaderspeed, 'g-')
    #plt.plot(ts, np.percentile(particles[:,3], [25, 75], axis=1).T, 'r--')
    plt.plot(ts, player_speed, 'r-')
    plt.plot(ts, leader_speed, 'b-')
    plt.subplot(3,1,2)
    plt.plot(ts, np.percentile(particles[:,2], [25, 75], axis=1).T, 'r--')
    plt.plot(ts, gap, 'g-')
    plt.plot(ts, estgap, 'r-')
    plt.subplot(3,1,3)
    plt.plot(ts, np.percentile(particles[:,1], [25, 75], axis=1).T, 'r--')
    #plt.plot(ts, particles[:,2], 'k.', alpha=0.01)
    plt.plot(ts, player_speed, 'g-')
    plt.plot(ts, estspeed, 'r-')

    #plt.plot(ts, player_speed)
    #plt.show()
    plt.figure()
    return dict(ts=ts, particles=particles, ownspeed=estspeed, relspeed=est_relspeed, gap=estgap, leaderspeed=est_leaderspeed, player_pos=player, is_blind=is_blind, state=states, relspeed_truth=relspeed)

def simulate_blind_evolution(predictor, accel, dt, prev_accel, speed, nsteps=100):
    states = []
    for i in range(nsteps):
        accels = accel.vec(dt, predictor.ownspeeds(), predictor.gaps(), predictor.leader_speeds() - predictor.ownspeeds())
        states.append([predictor.clone(), accels])
        prev_accel = np.median(accels)
        predictor.predict(dt, prev_accel)
        predictor.measure_ownspeed(speed)
        predictor.finish_step()
        speed += prev_accel*dt
        #accel_s = np.std(accel)

    return states


import pandas as pd
def get_data():
    data = pd.DataFrame.from_records(np.load('../simagg.npy'))
    data.ts /= 2.0
    data.query('experiment == 8', inplace=True)
    return data

def get_data_old():
    data = pd.DataFrame.from_records(np.load('../attadapt.combined.npy'))
    #data.ts /= 2.0
    data.query('experiment == "blindFollowInTraffic"', inplace=True)
    data.rename(columns=dict(sessionId='session_id', blind='isBlind'), inplace=True)
    return data

# Params from 10.1103/PhysRevE.58.133
def optimal_velocity_bando(V1=6.75, V2=7.91, C1=0.13, C2=1.57):
    def vopt(x):
        return V1 + V2*np.tanh(C1*x - C2)
    return vopt

# Params from https://journals.aps.org/pre/pdf/10.1103/PhysRevE.64.017101
def fvdm(V1=6.75, V2=7.91, C1=0.13, C2=1.57, k=0.41, l=0.4):
    vopt = optimal_velocity_bando(V1, V2, C1, C2)
    def accel(x, dv):
        return k*(vopt(x)) + l*dv
    return accel

def fit_optimal_velocity(gap, speed):
    def error(args):
        return optimal_velocity_bando(*args)(gap) - speed
    
    init = [v.default for v in inspect.signature(optimal_velocity_bando).parameters.values()]
    bounds = list(zip(*[[0.0, np.inf]]*len(init)))
    fit = scipy.optimize.least_squares(error, init, bounds=bounds)
    return optimal_velocity_bando(*fit.x), fit.x

def fit_fvdm(v, gap, dv, accel):
    def get_accel(args):
        return cfmodels.Fvdm(*args).vec(0.1, v, gap, dv)
    def error(args):
        return get_accel(args) - accel
    init = [6.75, 7.91, 0.13, 1.57, 0.41, 0.4]
    #init = [6.75, 7.91, 0.13, 1.57, 0.41]
    fit = scipy.optimize.least_squares(error, init, loss='soft_l1')
    print(fit)
    plt.plot(get_accel(fit.x))
    plt.plot(accel)
    plt.show()
    return cfmodels.Fvdm(*fit.x)

def Iqrs(s):
    return np.std(s, axis=1)
    ps = np.percentile(s, [75, 25], axis=1)
    return ps[0] - ps[1]

def test():
    dt = 0.1
    data = get_data()
    
    params = []
    for grp, d in data.groupby(('session_id', 'block')):
        d = d[d.player > 1]
        if len(d) < 100: continue
        
        if d.experiment.iloc[0] == 8:
            d.isBlind.iloc[:] = False # A bug...

        ts = d.ts.values
        glance_idx = np.flatnonzero(np.diff(d.isBlind.values.astype(int)) < 0)
        glance_t = ts[glance_idx]
        blind_dur = np.diff(glance_t)
        glance_t = glance_t[:-1]
        glance_idx = glance_idx[:-1]
        
        dts = np.gradient(ts)
        player_speed = np.gradient(d.player.values)/dts
        leader_speed = np.gradient(d.leader.values)/dts
        rel_speed = leader_speed - player_speed
        gap = (d.leader - d.player).values
        
        mental = simulate_mental_state(ts, d.player.values, d.leader.values, d.isBlind.values)

        mental_leader = np.cumsum(mental['leaderspeed']*dt) + mental['player_pos'][0] + mental['gap'][0]
        
        #mental_leader = d.player.values[0] + mental.gaps[0]
        player_speed_ds = np.gradient(mental['player_pos'])/dt
        player_accel = np.gradient(player_speed_ds)/dt
    
        sres = fit_lagged_stimulus(mental['ts'], mental['relspeed'], player_accel)
        vres = fit_lagged_stimulus(mental['ts'], mental['relspeed_truth'], player_accel)
        print(sres, vres)
        plt.plot(mental['ts'], player_accel)
        plt.show()
        continue

        #optimal_velocity = fit_optimal_velocity(mental['gap'], mental['ownspeed'])
        try:
            accel = fit_fvdm(mental['ownspeed'], mental['gap'], mental['relspeed'], player_accel)
            #res = fit_fvdm_trajectory(mental['ts'], mental['player_pos'], player_speed[0], mental_leader, mental['leaderspeed'])
            #res = fit_idm(mental['ts'], mental['player_pos'], player_speed[0], mental_leader, mental['leaderspeed'])
            #accel = fit_idm_accel(mental['ownspeed'], mental['gap'], mental['relspeed'], player_accel)
            #if res is None: continue
            #accel, sim_speed = res
        except ValueError as e:
            raise(e)
            continue
        
        #plt.plot(mental['gap'], mental['ownspeed'], '.')
        #plt.plot(mental['gap'], accel.optimal_velocity(mental['gap']), '.')
        #plt.figure()
        #fvdm_fit = fit_fvdm(mental['gap'], mental['relspeed'], player_accel)
        #plt.plot(fvdm_fit(mental['gap'], mental['relspeed']), player_accel)

        #plt.show()
        #plt.plot(mental['ts'], sim_speed)
        #plt.plot(ts, player_speed)
        #plt.figure()

        accels = []
        #print(mental['particles'].shape)
        for p in mental['particles']:
            a = accel.vec(dt, p[1], p[2], p[3] - p[1])
            accels.append(a)
        
        accels = np.array(accels)
        mean_accels = np.mean(accels, axis=1)
        aps = np.percentile(accels, [50, 25, 75], axis=1)

        #curt = (aps[1] - aps[-1])/(aps[2] - aps[-2])
        #plt.plot(mental['ts'], (aps.T - aps[0].reshape(-1, 1)*0))
        #plt.twinx()
        #plt.plot(mental['ts'], mental['ownspeed'], 'r-')
        #for t in glance_t:
        #    plt.axvline(t, color='black', alpha=0.3)
        #plt.plot(mental['ts'], curt)
        #accels = np.clip(accels, -10, 10)
        #plt.plot(mental['ts'], np.std(accels, axis=1))
        #unthws = mental['particles'][:,1]/mental['particles'][:,2]
        #unthws = mental['particles'][:,2]/mental['particles'][:,1]
        relspeeds = mental['particles'][:,3] - mental['particles'][:,1]
        ttc = relspeeds#/np.sqrt(mental['particles'][:,2])
        tps = np.percentile(ttc, [50, 25, 75], axis=1)
        plt.plot(mental['ts'], (tps - tps[0]).T)
        #plt.plot(mental['ts'], np.std(ttc, axis=1))
        plt.twinx()
        plt.plot(mental['ts'], mental['gap'], color='red')
        plt.show()
        #iqr = (aps[-1] - aps[1])
        #accel_std = iqr
        #plt.plot(mental['ts'], (tps.T - tps[0].reshape(-1, 1)))
        accel_std = np.std(accels, axis=1)
        mental_glance_idx = np.flatnonzero(np.diff(mental['is_blind'].astype(int)) < 0)
        mental_blind_idx = np.flatnonzero(np.diff(mental['is_blind'].astype(int)) > 0)
        
        #plt.plot(mental['ts'][mental_blind_idx], -accel_std[mental_blind_idx])
        #plt.twinx()
        #plt.plot(glance_t, blind_dur, color='red')
        #plt.show()
        #accel_std = iqr
        #relspeeds = (mental['particles'][:,3] - mental['particles'][:,1])/(mental['particles'][:,2]**2)
        #accel_std = np.std(unthws, axis=1)/np.mean(unthws)
        state_uncertainty = Iqrs(relspeeds)
        #state_uncertainty = Iqrs(accels)
        plt.plot(mental['ts'], state_uncertainty, '-', alpha=0.3)
        plt.plot(mental['ts'][mental_glance_idx], state_uncertainty[mental_glance_idx], '.')
        plt.plot(mental['ts'][mental_blind_idx], state_uncertainty[mental_blind_idx], '.')
        uc_limit = np.median(state_uncertainty[mental_glance_idx])*1.1
        plt.axhline(uc_limit)
        plt.show()
        
        """
        for i in range(len(mental['ts'])):
            if not mental['is_blind'][i]: plt.cla()
            print(i)
            plt.hist(accels[i], bins=100, histtype='step')
            plt.xlim(-5, 5)
            plt.pause(0.1)
            #plt.show()
        """
        #plt.show()
        #plt.plot(ts, thw)
        #plt.figure()
        
        """
        for i in mental_blind_idx[:100]:
            predictor = mental['state'][i]
            accels = accel.vec(dt, predictor.ownspeeds(), predictor.gaps(),
                    predictor.leader_speeds() - predictor.ownspeeds())
            astd = np.std(accels)
            plt.plot(mental['ts'][i], astd, 'o', color='black', markerfacecolor='none')
        """

        """
        blind_times = []
        uc_limits = []
        
        for i in mental_blind_idx:
            evo = simulate_blind_evolution(mental['state'][i].clone(), accel, dt, mean_accels[i-1], mental['ownspeed'][i-1])
            states, accels = zip(*evo)
            relspeed = np.array([s.leader_speeds() - s.ownspeeds() for s in states])
            state_uncertainty = Iqrs(relspeed)
            try:
                glance_i = mental_glance_idx[mental_glance_idx.searchsorted(i)]
            except IndexError:
                continue
            glance_dur = glance_i - i
            if glance_dur >= len(accels): continue
            uc_limits.append(state_uncertainty[glance_dur])
            blind_times.append(mental['ts'][i])
            #accels = np.std(accels, axis=1)
        plt.figure()
        plt.plot(blind_times, uc_limits)
        uc_limit = np.median(uc_limits)
        """
        plt.figure()
        
        sim_blind_durs = []
        sim_blind_times = []
        for i in mental_blind_idx:
            evo = simulate_blind_evolution(mental['state'][i], accel, dt, mean_accels[i-1], mental['ownspeed'][i-1])
            states, accels = zip(*evo)
            relspeed = np.array([s.leader_speeds() - s.ownspeeds() for s in states])
            aps = np.percentile(accels, [25, 50, 75], axis=1)
            #accels = np.std(accels, axis=1)
            state_uncertainty = Iqrs(relspeed)
            #accels = aps[-1] - aps[0]
            lim_hit = np.flatnonzero(state_uncertainty > uc_limit)
            if(len(lim_hit) == 0):
                lim_hit = 0
            else:
                lim_hit = lim_hit[0]
            if lim_hit <= 0: lim_hit = 1
            valid = slice(0, lim_hit)
            t = np.arange(len(states))*dt + mental['ts'][i]
            #plt.plot(t, accels)
            #accelguess.append(accels)
            #accelts.append(t)
            #valid = accels < 10.

            sim_blind_times.append(t[0])
            sim_blind_durs.append(t[valid][-1] - t[0])
            plt.plot(t[valid], state_uncertainty[valid])
        
        plt.figure()
        sim_blind_times = np.array(sim_blind_times)
        sim_blind_durs = np.array(sim_blind_durs)
        
        #ps = np.array([player_speed, gap, rel_speed]) 

        #accels = np.array([accel(dt, *p) for p in ps.T])
        #accel_g = np.array([accel.gradient_magnitude(dt, *p) for p in ps.T])
        #accels = [accel(dt, *p) for p in ps.T]

        thw = gap/player_speed
        thw[player_speed < 1.0] = np.nan
        plt.plot(glance_t, blind_dur, color='red')
        plt.plot(glance_t, thw[glance_idx], '-', color='green')
        plt.plot(sim_blind_times, sim_blind_durs, '-', color='blue')
        #plt.plot(mental['ts'], np.nanmedian(unthws, axis=1))

        valid = np.isfinite(thw[glance_idx])
        print("THW", scipy.stats.spearmanr(thw[glance_idx][valid], blind_dur[valid]))

        plt.figure()
        try:
            true_blind_dur = interp1d(glance_t, blind_dur, bounds_error=False)(sim_blind_times)
            valid = np.isfinite(true_blind_dur)
            print("Sim", scipy.stats.spearmanr(true_blind_dur[valid], sim_blind_durs[valid]))
            plt.plot(sim_blind_durs, interp1d(glance_t, blind_dur, bounds_error=False)(sim_blind_times), '.')
        except ValueError as e:
            print(e)
            continue

        plt.show()

        """
        plt.figure()
        plt.twinx()
        #plt.plot(ts[glance_idx], 1.0/accel_g[glance_idx], color='red')
        #plt.semilogy()
        #plt.figure()
        #plt.plot(1.0/accel_g[glance_idx], blind_dur, '.')
        ag = 1.0/accel_g[glance_idx]
        valid = np.isfinite(ag)
        print(scipy.stats.spearmanr(ag[valid], blind_dur[valid]))
        plt.loglog()
        plt.show()
        """

    

if __name__ == '__main__':
    test()
