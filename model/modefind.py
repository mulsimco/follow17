import numpy as np
import matplotlib.pyplot as plt

def itermean(x, weights=None):
    ms = []
    for i in range(100):
        m = np.average(x, weights=weights)
        ms.append(m)
        weights = 1.0/np.sqrt((x - m)**2)
        weights /= np.sum(weights)
    return m

N = 10000
stuff = np.exp(np.random.randn(N)*0.8 + 1.0)
mean = np.mean(stuff)
mode = itermean(stuff)
plt.hist(stuff, bins=300)
plt.axvline(mean, color='red')
plt.axvline(mode, color='green')
plt.show()
