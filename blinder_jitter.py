import ujson
import sys
import re
import numpy as np
import matplotlib.pyplot as plt

status = []
for line in sys.stdin:
    hdr, data = ujson.loads(line)
    if 'controller' not in data: continue
    data = data['controller']
    match = re.search(r'time: (\d+)', data)
    if match is None: continue
    t = int(match.groups()[0])
    status.append((hdr['ts'], t))
    #if not 'time' in data: continue
    #print(data['time'])

status = np.rec.fromrecords(status, names='trusas,arduino')
status.trusas -= status.trusas[0]
tsfit = np.poly1d(np.polyfit(status.arduino, status.trusas, 1))
error = status.trusas - tsfit(status.arduino)



plt.plot(status.trusas, error, '.')
plt.show()
