import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats

data = pd.DataFrame(np.load('simagg.npy'))

stuff = []
data.query('experiment == 8 and player > 100', inplace=True)
data.ts /= 2.0
for g, d in data.groupby(('session_id', 'block')):
    if d.ts.iloc[-1] < 10: continue
    hassle = np.std(d.steering)
    dt = np.gradient(d.ts.values)
    speed = np.gradient(d.player.values)/dt
    accel = np.gradient(speed)/dt
    gap = d.leader.values - d.player.values
    thw = gap/speed
    mthw = 1.0/np.mean(1.0/thw)
    msteering = np.std(d.steering)
    absaccel = np.mean(np.abs(accel))
    absaccel = np.percentile(accel, 99)
    #plt.hist(accel, bins=100)
    #plt.show()
    #plt.plot(mthw, absaccel, 'o', color='black')
    stuff.append((g[0], mthw, absaccel, msteering))
    #plt.plot(np.mean(np.abs(accel)), np.std(d.steering), 'o', color='black')

stuff = np.rec.fromrecords(stuff, names='sid,thw,accel,steering')
stuff = pd.DataFrame(stuff)
stuff = stuff.groupby('sid').mean()
print(scipy.stats.pearsonr(stuff.thw, stuff.accel))
plt.scatter(stuff.thw, stuff.accel, c=stuff.steering)
plt.show()
