from binascii import unhexlify, b2a_hex
import re
import struct

def wheel_rotation(sensor_no):
    def parse(d):
        a, b, c = struct.unpack('<HHH', d)
        return dict(
            wheel_sensor=sensor_no,
            wheel_speed_a=a,
            wheel_speed_b=b
        )
    return parse

can_handlers = {
    0x0b0: wheel_rotation(1),
    0x0b2: wheel_rotation(2),
}

NONHEX=re.compile(r"[^0-9a-fA-F]")
def decode_can_message(obj):
    hdr, msg = obj
    if 'can' not in msg:
        return {}
    msg = msg['can']
    if NONHEX.match(msg): return {}

    msg = "0" + msg
    msg = unhexlify(msg)
    can_hdr, data = msg[:2], msg[2:]
    sender = int.from_bytes(can_hdr, byteorder='big', signed=False)
    if sender not in can_handlers:
        return {}
    return can_handlers[sender](data)

import json
import os
def jsons(lines):
    for line in lines:
        yield json.loads(line)


#import scipy.interpolate
#import numpy as np
def get_gps_speeds(objs):
    res = []
    for hdr, o in objs:
        if o['mProvider'] != 'gps':
            continue
        res.append((hdr['ts'], o['mSpeed']))
    return np.array(res)

def calibrate_sensors(directory):
    def getobjs(f):
        return jsons(open(os.path.join(directory, f)))
    #gspeed = get_gps_speeds(getobjs('location.jsons'))
    #gspeed = scipy.interpolate.interp1d(gspeed[:,0], gspeed[:,1], bounds_error=False)
    
    can = getobjs('obd.jsons')
    
    # Do some "rewinding" so this won't take ages
    #[next(can) for i in range(100000)]
    #can = [next(can) for i in range(300000)]

    for i, msg in enumerate(can):
        pass
        #print(decode_can_message(msg))
    print(i)


if __name__ == '__main__':
    import sys
    calibrate_sensors(sys.argv[1])
    #argh.dispatch_command(calibrate_sensors)
