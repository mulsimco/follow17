import numpy as np
import ujson as json
import argh
import os
import scipy.interpolate
import pandas as pd
import matplotlib.pyplot as plt
import pyproj
import itertools
from scipy.interpolate import interp1d
import scipy.stats
import scipy.optimize
import quats

from can_reverse_engineer import decode_can

#proj = pyproj.Proj(init='EPSG:3067')
proj = pyproj.Proj(init='EPSG:3132')


def jsons(lines):
    for line in lines:
        if(hasattr(line, 'decode')):
            line = line.decode('utf-8')
        yield json.loads(line)

#def get_glance_times(objs):
#    lift_times = []
#    for hdr, o in objs:
#
#        if 'controller' not in o: continue
#        if o['controller'].strip() == 'Lifting':
#            lift_times.append(hdr['ts'])
#    return np.array(lift_times)

def sumsample(t, x, nt):
    dt = np.ediff1d(t, to_begin=1)
    cum = np.cumsum(x*dt)
    nidx = t.searchsorted(nt)
    ndt = np.ediff1d(nt, to_begin=1)
    sampled = np.ediff1d(cum.take(nidx, mode='clip'), to_begin=0.0)/ndt
    #plt.plot(t, cum)
    invalid = (nt <= t[0]) | (nt >= t[-1])
    sampled[invalid] = np.nan
    return sampled

def crosscorr(ts, target, interp, rng=np.arange(-0.1, 0.1, 0.001)):
    corrs = []
    for lag in rng:
        x = interp(ts + lag)
        valid = np.isfinite(x)
        corrs.append(scipy.stats.pearsonr(x[valid], target[valid])[0])
    
    plt.plot(rng, corrs)
    print(rng[np.argmax(corrs)])
    plt.show()

def read_positions(getfile):
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')   
    gpsobjs = jsons(open(getfile('location.jsons')))
    gps = []
    for hdr, d in gpsobjs:
        if d['mProvider'] != 'gps': continue

        gps.append(dict(
            ts=hdr['ts'],
            gps_ts=d['mTime'],
            android_ts=d['mElapsedRealtimeNanos'],
            latitude=d['mLatitude'],
            longitude=d['mLongitude'],
            speed=d['mSpeed'],
            bearing=d['mBearing'],
            altitude=d['mAltitude'],
            ))
    
    gps = pd.DataFrame.from_records(gps)
    gps['x'], gps['y'] = proj(gps.longitude.values, gps.latitude.values)
    #gps['x'], gps['y'], gps['z'] = pyproj.transform(
    #        lla, ecef,
    #        gps.longitude.values, gps.latitude.values, gps.altitude.values,
    #        radians=False)

    #print(gps['x'])
    return gps
    """
    reference = gps.gps_ts/1e3
    other = gps.android_ts/1e9
    #other = gps.ts
    offset = np.median(reference - other)
    print(offset)
    fit = np.poly1d(np.polyfit(reference, other, 1))
    print(fit)
    #plt.plot(reference, (other + offset) - reference)
    odiff = (other + offset) - reference
    reference = gps.gps_ts/1e3
    #other = gps.android_ts/1e9
    other = gps.ts
    offset = np.median(reference - other)
    print(offset)
    plt.plot(reference, (other + offset) - reference - odiff)

    plt.show()
    #plt.plot((gps.android_ts - fit(gps.ts))/1e9)
    #plt.show()
    #print(gps)
    """
def read_telemetry(getfile):
    can = open(getfile('obd.jsons'))
    start = int(2e6 + 1e5)
    dur = int(1.5e5)
    #can = itertools.islice(open(getfile('obd.jsons')), int(1e6), None)
    #can = itertools.islice(open(getfile('obd.jsons')), start, start+dur)
    can = decode_can(jsons(can))
    """
    s = can[can.sender == 178] # Front?
    #s = can[can.sender == 0x0b0]
    # Use only "Burst end" values"
    dt = np.diff(s.ts)
    mid = np.mean(dt)
    changes = dt > mid
    s = s[1:][changes]
    s1 = s.b0*255 + s.b1 # Probably left wheel
    s2 = s.b2*255 + s.b3 # Probably right wheel
    """
    dt = np.ediff1d(can.ts, to_begin=0)
    changes = dt > np.mean(dt)
    changes[0] = True; changes[-1] = True
    rng = np.arange(len(can.ts))
    can.ts = interp1d(rng[changes], can.ts[changes])(rng)

    """
    sr = can[can.sender == 176] # Rear?

    dt = np.diff(sr.ts)
    mid = np.mean(dt)
    rng = np.arange(len(sr))

    sr.ts[:] = interp1d(rng[changes], sr.ts[changes])(rng)

    sr1 = sr.b0*255 + sr.b1 # Probably left wheel
    sr2 = sr.b2*255 + sr.b3 # Probably right wheel
    """
    
    
    #speed = pd.DataFrame(dict(ts=sr.ts, left=sr1, right=sr2))
    
    # TODO:
    # Speed: (180, 5, '>u2')
    # Yaw rate: (36, 0, '>u2')
    # ---
    # Longitudal acceleration (186, 0, '>u2')
    s = can[can.sender == 180]
    wheelspeed = s.b5*255 + s.b6
    speed = pd.DataFrame(dict(ts=s.ts, wheelspeed=wheelspeed))
    
    s = can[can.sender == 36]
    yaw_rate = s.b0*255 + s.b1
    yaw_rate = pd.DataFrame(dict(ts=s.ts, yaw_rate=yaw_rate))

    #speed = pd.DataFrame(dict(ts=s.ts, left=s1, right=s2))
    return {
        "wheelspeed": speed,
        "yaw_rate": yaw_rate
    }

def odrlinfit(x, y):
    from scipy.odr import Data, Model, ODR
    b0 = np.polyfit(x, y, 1)
    model = Model(lambda B, x: B[0]*x + B[1])
    data = Data(x, y)
    odr = ODR(data, model, b0)
    result = odr.run()
    return result.beta

def multishoothack(t, dx, x, shootlen=10):
    dt = np.ediff1d(t, to_begin=0)
    edx = np.ediff1d(x, to_begin=0)/dt
    #edx = np.ediff1d(x, to_begin=0)/np.diff(
    valid = np.isfinite(t + dx + x + edx)
    t = t[valid]; dx = dx[valid]; x = x[valid]; edx = edx[valid]; dt = dt[valid]
    nshoots = len(t)//shootlen
    shoots = [slice(i*shootlen, (i+1)*shootlen) for i in range(nshoots)]

    def error(ab, plot=False):
        merrs = []
        for shoot in shoots:
            a, b = ab
            mdx = dx[shoot]
            mdt = dt[shoot]
            cumdx = np.cumsum((mdx[:-1]*a + b)*mdt[:-1]) + x[shoot][0]
            err = np.mean(np.abs(x[shoot][1:] - cumdx))
            if plot:
                plt.plot(t[shoot][1:], cumdx - x[shoot][1:])
            merrs.append(err)
        #plt.show()
        return np.mean(merrs)
    
    
    plt.show()
    a, b = odrlinfit(edx, dx)
    #a, b = np.polyfit(dx, edx, 1)
    plt.plot(dx, edx, '.', alpha=0.1)
    plt.plot(dx, a*dx + b, '-')
    print(a, b)
    """
    #fit = scipy.optimize.minimize(error, [a, b])
    #print(fit)
    a, b = fit.x
    plt.plot(dx, a*dx + b, '-')
    """
    plt.show()
    error([a, b], plot=True)
    plt.show()
    cumdx = np.cumsum((dx*a + b)*dt) + x[0]
    #plt.plot(t, x)
    plt.plot(t, np.degrees(cumdx - x))
    plt.show()

sensor_mapping = {
    
}

def read_sensors(getfile):
    sensors = open(getfile('sensors.jsons'))
    sensors = (json.loads(l) for l in sensors)
    data = {
        'rotation_vector': [],
        'gravity': [],
        'gyro': [],
        'magnetic_field': []
    }
    types = set()
    for hdr, obj in itertools.islice(sensors, int(1e6), int(1e6 + 6e5)):
        stype = obj['sensor_type']
        if stype == 11:
            data['rotation_vector'].append(dict(
                ts=hdr['ts'],
                android_ts=obj['timestamp'],
                x=obj['values'][0],
                y=obj['values'][1],
                z=obj['values'][2]
                ))
        if stype == 9:
            data['gravity'].append(dict(
                ts=hdr['ts'],
                android_ts=obj['timestamp'],
                x=obj['values'][0],
                y=obj['values'][1],
                z=obj['values'][2]
                ))
        elif stype == 4:
            data['gyro'].append(dict(
                ts=hdr['ts'],
                android_ts=obj['timestamp'],
                x=obj['values'][0],
                y=obj['values'][1],
                z=obj['values'][2]
                ))
        elif stype == 14:
            data['magnetic_field'].append(dict(
                ts=hdr['ts'],
                android_ts=obj['timestamp'],
                x=obj['values'][0],
                y=obj['values'][1],
                z=obj['values'][2]
                ))

    data = {n: pd.DataFrame(v) for n, v in data.items()}
    gyro = data['gyro']
    gravity = data['gravity']
    gdir = gravity[['x', 'y', 'z']].values
    gdir /= np.linalg.norm(gdir, axis=1).reshape(-1, 1)
    gdirint = interp1d(gravity.android_ts.values, gdir, bounds_error=False, axis=0)
    yaw = np.sum(gdirint(gyro.android_ts.values)*gyro[['x','y','z']].values, axis=1)
    gyro['yaw'] = yaw
    return data
    #print(yaw)
    #valid = np.isfinite(yaw)
    #plt.plot(np.degrees(np.cumsum(yaw[valid])))
    #pca = PCA().fit_transform(data['gyro'][['x', 'y', 'z']].values)

    plt.show()
    print(len(r))
    ts = r['android_ts'].values
    ts = ts/1e9
    ts -= ts[0]
    #rate = np.linalg.norm(r[['x', 'y', 'z']].values, axis=1)
    #plt.plot(ts, r['yaw'])
    #plt.plot(np.diff(data['rotation_vector']['android_ts']))
    plt.show()
    #print(data['rotation_vector'][['x', 'y', 'z']].values)

def parse_directory(sdir):
    def getfile(path):
        return os.path.join(sdir, path)
    
    sensors = read_sensors(getfile)
    can = read_telemetry(getfile)
    wheel = can['wheelspeed']
    yaw = can['yaw_rate']
    
    gps = read_positions(getfile)
    #gps.ts -= 0.06
    #gps.ts += 0.2
    #gps.ts += 0.65
    #gps.ts += 1.0
    #gps.ts += 0.65

    """
    dt = np.diff(gps.gps_ts.values)/1e3
    diffs = np.diff(gps[['x', 'y']].values, axis=0)
    angles = np.radians(gps.bearing.values)
    adiff = np.diff(angles)
    yaw_change = np.arctan2(np.sin(adiff), np.cos(adiff))
    yaw = can['yaw_rate']
    yaw_rate = yaw_change/dt
    
    yawfit = interp1d(gps.ts[1:], yaw_rate, bounds_error=False)(yaw.ts)
    
    plt.plot(gps.ts[1:], (yaw_rate))
    valid = np.isfinite(yawfit) & np.isfinite(yaw.yaw_rate)
    yf = np.poly1d(np.polyfit(yaw.yaw_rate[valid], yawfit[valid], 1))
    print(yf)
    plt.plot(yaw.ts, yf(yaw.yaw_rate))
    #plt.plot(can['yaw_rate'].ts, (512 - can['yaw_rate'].yaw_rate)*0.25, color='red')
    plt.figure()

    plt.plot(yaw.yaw_rate, yawfit, '.', alpha=0.1)
    rng = np.linspace(np.min(yaw.yaw_rate), np.max(yaw.yaw_rate), 10)
    plt.plot(rng, yf(rng), '-')
    plt.show()
    #dist = np.linalg.norm(diffs, axis=1)
    #est_speed = np.linalg.norm(np.gradient(gps[['x', 'y']].values, axis=0), axis=1)/(np.gradient(gps.gps_ts)/1e3)
    #gps.ts -= 0.65
    #displacement = np.gradient(gps[['x', 'y']].values, axis=0)
    #bearing = np.arctan2(displacement[:,0], displacement[:,1])
    #gps.bearing = bearing
    #plt.plot(gps.ts, np.cos(bearing), label='manual')
    #plt.plot(gps.ts, np.cos(np.radians(gps.bearing)), label='gps')
    #plt.show()
    #plt.legend()
    #plt.show()
    
    #plt.plot(gps.ts.values[1:] - 0.65, est_speed, color='red')
    #plt.plot(gps.ts.values[1:] - 0.65, yaw_rate, color='red')
    
    wheel = can['wheelspeed']
    #wheeldiff = (wheel.right - wheel.left).values.astype(np.float)
    
    est_speed = np.linalg.norm(np.diff(gps[['x', 'y']].values, axis=0), axis=1)/(np.diff(gps.gps_ts)/1e3)
    wfit = interp1d(wheel.ts, wheel.wheelspeed, bounds_error=False)(gps.ts[1:])
    valid = np.isfinite(wfit)
    speedmult = np.sum(est_speed[valid]*wfit[valid])/np.sum(wfit[valid]**2)
    print(speedmult)
    plt.plot(gps.ts[1:], est_speed)
    plt.plot(wheel.ts, wheel.wheelspeed*speedmult, color='red')
    plt.show()
    """

    #speedmult = 0.00285340031864
    #speedmult = 0.00285489567088
    speedmult = 0.00286092065095
    landspeed = speedmult*wheel.wheelspeed
    #yawfit = lambda x: -0.004114*x + 2.099
    #yawfit = lambda x: -0.004009*x + 2.044
    #yawfit = lambda x: -0.004111*x + 2.098
    #yaw_rate = yawfit(yaw.yaw_rate.values)

    #yaw_rate = (yaw.yaw_rate.values - 512)*0.004
    #-1.08124454, -0.00721143
    yaw_rate = -1.08463675839*(yaw.yaw_rate.values - 512)*0.004 - 0.00653597348884
    gyro = sensors['gyro']
    gyro = gyro[gyro.yaw.notnull()]
    yaw_rate = gyro.yaw.values
    #yaw_rate = -1.05125181*yaw_rate + 0.01170069
    #yaw_rate = -0.959241139294*yaw_rate + 0.0102860236027
    yaw_rate -= 0.01
    yaw_rate *= -1

    db = sumsample(gyro.android_ts.values/1e9, yaw_rate, gps.android_ts.values/1e9)
    #dbint = lambda ts: sumsample(yaw.ts.values, yaw_rate, ts)
    bearing = np.radians(gps.bearing.values)
    displacement = np.gradient(gps[['x', 'y']].values, axis=0)
    bearing = np.arctan2(displacement[:,0], displacement[:,1])

    #adiff = np.ediff1d(bearing, to_begin=0)
    adiff = np.ediff1d(bearing, to_begin=0)
    dbearing = np.arctan2(np.sin(adiff), np.cos(adiff))


    gyaw = dbearing/np.ediff1d(gps.android_ts.values.astype(float)/1e9, to_begin=0)
    #crosscorr(gps.ts.values, -gyaw, dbint)
    #plt.show()

    #plt.plot(gps.android_ts, -db)
    #plt.plot(gps.android_ts, gyaw)
    #plt.show()

    bearing = np.cumsum(dbearing) + bearing[0]
        #db = interp1d(yaw.ts, yaw.yaw_rate, bounds_error=False)(gps.ts) # TODO: Average over span!
    #multishoothack(gps.ts.values, db, bearing)
    """
    #plt.plot(gps.ts, np.gradient(gps.bearing.values))
    turning = np.abs(np.gradient(gps.bearing.values)) > 10.0
    tidx = np.flatnonzero(turning); turning[tidx + 1] = True; turning[tidx - 1] = True;
    tidx = np.flatnonzero(turning); turning[np.clip(tidx + 1, 0, len(turning)-1)] = True; turning[tidx - 1] = True;
    tidx = np.flatnonzero(turning); turning[np.clip(tidx + 1, 0, len(turning)-1)] = True; turning[tidx - 1] = True;
    turning = interp1d(gps.ts, turning, kind='nearest')
    wt = turning(wheel.ts).astype(np.bool)
    #plt.plot(wheel.ts[~wt], wheeldiff[~wt])
    #wheeldiff += 3.0
    plt.hist(wheeldiff, bins=200)
    plt.show()
    """
    
    """
    meanwheel = wheel.left*0.5 + wheel.right*0.5
    
    awd = (wheeldiff)
    ayr = (yaw_rate)
    wdint = interp1d(wheel.ts, awd, bounds_error=False)
    wd = wdint(gps.ts.values[1:])
    ayr = (ayr)
    wd = (wd)
    valid = np.isfinite(ayr) & np.isfinite(wd)
    #valid &= ayr > -2
    #valid &= wd > 4
    #plt.plot(ayr[valid], wd[valid], '.')
    estdiff = ayr[valid] - wd[valid]
    estmult = np.exp(np.median(estdiff))
    plt.plot(ayr, ayr, '.', alpha=0.1)

    fit = np.poly1d(np.polyfit(wd[valid], ayr[valid], 1))
    print(fit)
    wd_to_yaw = fit
    rng = np.linspace(np.min(wd[valid]), np.max(wd[valid]), 1000)
    plt.plot(ayr[valid], fit(wd[valid]), '.')
    plt.show()
    """
    """
    #wd_to_yaw = lambda x: 0.001592*x - 0.002287
    #wd_to_yaw = lambda x: 0.001725*x + 0.001797

    print(estmult)
    #yawmult = 0.00183878523367
    #yawmult = 0.0018
    yawmult = 0.00175
    speedmult = 1/(99.0*3.6)
    landspeed = meanwheel*speedmult
    #yaw_rate = wheeldiff*yawmult
    yaw_rate = wd_to_yaw(wheeldiff)
    
    plt.plot(gps.ts, est_speed)
    plt.plot(wheel.ts, landspeed)
    
    plt.show()
    """

    #yaw_rate = interp1d(yaw.ts, yaw_rate, kind='nearest', fill_value='extrapolate')(wheel.ts)
    landspeed = interp1d(wheel.ts, landspeed, fill_value='extrapolate')(gyro.ts)
    from location_filter import LocationFilter
    
    gps = gps[gps.ts > gyro.ts.iloc[0]]
    gps = gps.iloc[:-1]
    measurement_idx = gyro.ts.searchsorted(gps.ts.values)
    measurement_idx = [m for m in measurement_idx if m > 0]
    dt = np.diff(gyro.ts.values)
    j = 0
    filt = LocationFilter([gps.x.iloc[j], gps.y.iloc[j]], np.radians(gps.bearing.iloc[j]))
    j += 1
    
    xs = []; ys = []; ws = []
    xs.append(filt.particles.x.copy())
    ys.append(filt.particles.y.copy())
    ws.append(filt.weights())
    bws = []
    prev_measurement = 0
    for i in range(len(gyro.ts) - 1):
        filt.telemetry(dt[i], landspeed[i], yaw_rate[i])
        if i >= measurement_idx[j]:
            bw = filt.measurement([gps.x.iloc[j], gps.y.iloc[j]])
            bn = i - prev_measurement
            prev_measurement = i
            bws.extend([bw]*bn)
            bws.append(filt.weights())
            j += 1
        xs.append(filt.particles.x.copy())
        ys.append(filt.particles.y.copy())
        ws.append(filt.weights())
    
    gps = gps[gps.ts < gyro.ts.iloc[-1]]
    x = [np.dot(xs[i], bws[i]) for i in range(len(xs))]
    y = [np.dot(ys[i], bws[i]) for i in range(len(xs))]
    #x = np.mean(xs, axis=1)
    #y = np.mean(ys, axis=1)
    plt.plot(x, y, label='Filtered')
    
    plt.plot(gps.x, gps.y, label='GPS')
    plt.plot(gps.x.iloc[0], gps.y.iloc[0], 'o')
    xs, ys = (np.array(v).T for v in (xs, ys))
    for i in range(0):
        plt.plot(xs[i], ys[i])
    plt.legend()
    #plt.figure()
    
    diffs = np.diff(np.vstack((x, y)).T, axis=0)

    plt.figure()

    #cumyaw = np.array([-np.radians(gps.bearing.iloc[0])] + list(np.cumsum(yaw_rate[1:]*dt)))
    cumyaw = np.array([0] + list(np.cumsum(yaw_rate[1:]*dt)))
    cumyaw += np.radians(gps.bearing.iloc[0])
    direction = np.vstack((np.sin(cumyaw), np.cos(cumyaw))).T
    velocity = landspeed.reshape(-1, 1)*direction
    displacement = velocity[1:]*dt.reshape(-1, 1)
    location = np.cumsum(displacement, axis=0)
    location[:,0] += gps.x.iloc[0]
    location[:,1] += gps.y.iloc[0]
    plt.plot(location[:,0], location[:,1])
    
    plt.plot(gps.x, gps.y)

    plt.show()
    
    #fit = np.poly1d(np.polyfit(wd, yaw_rate, 1))
    #print(fit)
    #plt.plot(wd, yaw_rate, '.')
    #plt.plot(yaw_rate, yaw_rate)
    #plt.plot(fit(wd), yaw_rate, '.')
    #plt.show()

    #plt.plot(wheel.ts, wheel.right - wheel.left, color='red')
    #plt.plot(can['wheelspeed'].ts, can['wheelspeed'].left/(100*3.6), color='red')
    #plt.plot(wheel.ts, yawest, color='red')
    #plt.twinx()
    #plt.plot(gps.ts[1:], yaw_rate)
    #plt.show()
    
if __name__ == '__main__':
    argh.dispatch_command(parse_directory)
