def lineseg_point_projection(p, a, b):
	(p, a, b) = map(np.array, (p, a, b))
	segd = b - a
	seglen = np.linalg.norm(segd)
	normstart = p - a
	t = np.dot(normstart, segd)/(seglen**2)
	if t > 1:
		error = np.linalg.norm(p - b)
		t = 1.0
		return t, error, b

	if t < 0:
		error = np.linalg.norm(normstart)
		t = 0.0
		return t, error, a
	
	proj = a + t*segd
	error = np.linalg.norm(p - proj)
	return t, error, proj

def closester(haystack):
    tree = scipy.spatial.KDTree(haystack)
