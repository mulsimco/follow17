from pyproj import Proj
from owslib.wms import WebMapService

import numpy as np

proj_name = "EPSG:3132" # DEPRECATED!
#proj_name = "epsg:3879"
proj = Proj(init=proj_name)
hackproj = "CRS:84"

extent = np.array([(502617.07339377742, 503192.332358744), (6681963.1860977644, 6682140.6621814948)]).T
print(extent)
#(25502617.073393777, 25503192.332358744) (6681963.1860977644, 6682140.6621814948)
meters_per_pixel = 0.08
size = np.round(np.diff(extent, axis=0)/meters_per_pixel).astype(np.int).ravel()

#extent = proj(extent[0], extent[1], inverse=True)

wms = WebMapService("http://kartta.hel.fi/ws/geoserver/avoindata/wms", version="1.1.1")
layer = wms.contents['avoindata:Ortoilmakuva_2017_8cm']
bbox = (*extent[0], *extent[1])
print(bbox)
print(size)
tile = wms.getmap(
        layers=['avoindata:Ortoilmakuva_2017_8cm'],
        styles=['raster'],
        bbox=bbox,
        size=size,
        srs=proj_name,
        #crs=proj_name,
        format='image/png'
        )
print(tile.geturl())
import matplotlib.pyplot as plt
with open('/tmp/tmphack.png', 'wb') as f:
    f.write(tile.read())
plt.imshow(plt.imread('/tmp/tmphack.png'))
plt.show()
