import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import json
import scipy.stats

data = pd.read_pickle('merged.pickle')
glance_times = pd.read_pickle('glance_times.pickle')
track = json.load(open('track.json'))

west_span = [80, 360]
east_span = [560, 830]


track_len = 0
for i in range(len(track)-1):
    track_len += np.linalg.norm(np.subtract(track[i+1], track[i]))

track_len += np.linalg.norm(np.subtract(track[-1], track[0]))

agg = []
for sid, sd in data.groupby(level=0):
    if len(sd) < 10000:
        continue
    
    sd = sd.copy()
    sd['occlusion_duration'] = np.nan
    sd['thw'] = sd.leader_distance/sd.landspeed
    glances = glance_times[glance_times.session_id == sid].glance_times.values
    occlusion_durations = np.diff(glances) - 0.3
    sd['occlusion_duration'].ix[sd.ts.values.searchsorted(glances[:-1])] = occlusion_durations
    
    invalid = ~sd.block_type.isin(['blind', 'unblind'])
    sd = sd[~invalid]
    
    thw = sd.leader_distance/sd.landspeed
    valid = sd.landspeed > 3.0
    lap_pos = sd.lap_pos - sd.lap*track_len
    valid = valid & (lap_pos.between(*west_span) | lap_pos.between(*east_span))
    sd = sd[valid]
    ts = np.array(sd['ts'])
    
    ubd = sd[sd.block_type == 'unblind']
    bd = sd[sd.block_type == 'blind']
    #bd = bd[bd.ts > bd.ts.ix[0] + 60*7]
    #ubd = ubd[ubd.ts > ubd.ts.ix[0] + 60*7]
    #bd = bd[np.isfinite(bd['occlusion_duration'])]
    
    median_od = np.nanmedian(bd.occlusion_duration)
    ubd_thw = np.nanmedian(ubd.thw[ubd.landspeed > 3.0])
    bd_thw = np.nanmedian(bd.thw[bd.landspeed > 3.0])
    agg.append((median_od, ubd_thw, bd_thw))
    continue
    #plt.plot(ubd.ts, ubd.thw)
    #plt.show()
    print(ubd_thw)
    print(median_od)

    #ts[invalid] = np.nan
    #thw[invalid] = np.nan
    for lap, ld in sd.groupby('lap'):
        plt.plot(ld.lap_pos - lap*track_len, ld.landspeed)
    plt.show()

agg = pd.DataFrame.from_records(agg, columns="od,unblind_thw,blind_thw".split(','))
agg['thw_delta'] = agg['blind_thw'] - agg['unblind_thw']
plt.plot(agg['od'], agg.thw_delta, '.', label='Participant')
r = scipy.stats.spearmanr(agg.od, agg.thw_delta)[0]
fit = np.poly1d(np.polyfit(agg.od, agg.thw_delta, 1))
print(r)
xrng = np.array([np.min(agg.od), np.max(agg.od)])
plt.plot(xrng*0.5, xrng*0.5, 'k--', alpha=0.5, label='y = x')
plt.plot(xrng, fit(xrng), 'k-', alpha=0.5, label="y = %.2fx + %.2f"%tuple(fit))
plt.title("VERY PRELIMINARY!!\nOcclusion duration vs THW increase, Spearman r=%.2f"%r)
plt.xlabel('Median occlusion duration (seconds)')
plt.ylabel('Median time headway increase (seconds)')
#plt.gca().annotate("VERY PRELIMINARY", xycoords="figure fraction", xytext=(0.5, 0.5), xy=(0.5, 0.5), alpha=0.2)
plt.legend()

#plt.plot(data.lap_pos%track_len, data.landspeed, '.', alpha=0.01)
plt.show()
