import numpy as np
import scipy.stats

def softmax(x):
    e = np.exp(x-np.max(x))
    return e/np.sum(e)

# From filterpy
def systematic_resample(weights):
    """ Performs the systemic resampling algorithm used by particle filters.

    This algorithm separates the sample space into N divisions. A single random
    offset is used to to choose where to sample from for all divisions. This
    guarantees that every sample is exactly 1/N apart.

    Parameters
    ----------
    weights : list-like of float
        list of weights as floats

    Returns
    -------

    indexes : ndarray of ints
        array of indexes into the weights defining the resample. i.e. the
        index of the zeroth resample is indexes[0], etc.
    """
    N = len(weights)

    # make N subdivisions, and choose positions with a consistent random offset
    positions = (np.random.random() + np.arange(N)) / N

    indexes = np.zeros(N, 'i')
    cumulative_sum = np.cumsum(weights)
    i, j = 0, 0
    while i < N:
        if positions[i] < cumulative_sum[j]:
            indexes[i] = j
            i += 1
        else:
            j += 1
    return indexes


class LocationSmoother:
    def __init__(self, init_loc, init_bearing,
            loc_noise=10.0,
            vel_noise=1.0,
            yaw_drift=np.radians(0.01),
            yaw_noise=np.radians(30.0), 
            n_particles=100):
        fields = "x,y,dx,dy,bearing,lik,yaw_bias".split(',')
        dtype = [(n, float) for n in fields]
        self.particles = np.recarray(n_particles, dtype=dtype)
        self.particles.x, self.particles.y = init_loc
        #self.particles.bearing = np.random.uniform(0.0, 2*np.pi, size=n_particles)
        self.particles.bearing = init_bearing
        self.particles.lik = 0.0
        self.particles.yaw_bias = 0.0
        self.vel_noise = vel_noise
        self.yaw_noise = yaw_noise
        self.yaw_drift = yaw_drift
        self.n_particles = n_particles
        self.loc_dist = scipy.stats.norm(scale=loc_noise)
        self.history = []

    #def step(self, dt, telemetry=None, measurement=None):
    #    self.history.append((dt, telemetry, measurement))
    #    if telemetry is not None:
    #        bias_noise = 

def getdist(v):
    if hasattr(v, 'logpdf') and hasattr(v, 'rvs'):
        return v
    else:
        return scipy.stats.norm(scale=v)

class LocationFilter:
    def __init__(self, init_loc, init_bearing,
            loc_noise=10.0,
            vel_noise=1.0,
            yaw_drift=np.radians(0.01),
            yaw_noise=np.radians(30.0), 
            n_particles=100):
        fields = "x,y,dx,dy,bearing,lik,yaw_bias".split(',')
        dtype = [(n, float) for n in fields]
        self.particles = np.recarray(n_particles, dtype=dtype)
        self.particles.x, self.particles.y = init_loc
        #self.particles.bearing = np.random.uniform(0.0, 2*np.pi, size=n_particles)
        self.particles.bearing = init_bearing
        self.particles.lik = 0.0
        self.particles.yaw_bias = 0.0
        self.vel_noise = vel_noise
        self.yaw_noise = yaw_noise
        self.yaw_drift = yaw_drift
        self.n_particles = n_particles
        self.loc_dist = scipy.stats.norm(scale=loc_noise)
        #self.yaw_dist = getdist()
        self.history = []
        self.telemetries = []
                
    def telemetry(self, dt, velocity, yaw_rate):
        self.particles.yaw_bias += np.random.randn(self.n_particles)*self.yaw_drift
        yaw_noise = np.random.randn(self.n_particles)*self.yaw_noise - self.particles.yaw_bias
        self.particles.bearing += dt*(yaw_rate + yaw_noise)
        vel_noise = np.random.randn(self.n_particles)*self.vel_noise
        self.particles.velocity = velocity + vel_noise
        self.particles.dx = np.sin(self.particles.bearing)
        self.particles.dy = np.cos(self.particles.bearing)
        self.particles.x += dt*self.particles.dx*velocity
        self.particles.y += dt*self.particles.dy*velocity

    def measurement(self, pos):
        dx = pos[0] - self.particles.x
        dy = pos[1] - self.particles.y
        self.particles.lik += self.loc_dist.logpdf(dx)
        self.particles.lik += self.loc_dist.logpdf(dy)
        prev_weights = self.weights()
        neff = 1.0/np.sum(prev_weights**2)
        self.resample()
        #if neff < self.n_particles/2:
        #    self.resample()
        return prev_weights

    def resample(self):
        # TODO: Do systematic sampling
        weights = softmax(self.particles.lik)
        idx = systematic_resample(weights)
        #idx = np.random.choice(self.n_particles, self.n_particles, p=weights)
        self.particles[:] = self.particles[idx]
        self.particles.lik = 0.0

    def weights(self):
        return softmax(self.particles.lik)

