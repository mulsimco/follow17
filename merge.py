import os
import pandas as pd
import argh

def merge(*files):
    data = []
    sids = []
    for f in files:
        sid = os.path.basename(f).split('.')[0]
        print(sid)
        d = pd.read_pickle(f)
        data.append(d)
        sids.append(sid)

    data = pd.concat(data, keys=sids)
    data.to_pickle('merged.pickle')

if __name__ == '__main__':
    argh.dispatch_command(merge)
