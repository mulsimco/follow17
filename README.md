# Data and code for the article "A computational model for driver's cognitive state, visual perception and intermittent attention in a distracted car following task"

The code is released for transparency and reproducibility and is of dubious quality and
apart from this file totally undocumented. The main analysis file is `model/compare_model.py`
and different analyses can be run by uncommenting function calls from the end of the line.
The code requires also the `cfmodels` package, which is in a [separate repository](https://gitlab.com/mulsimco/cfmodels)
because git subprojects suck. 

Running the analyses requires the files `summary.npy` and `simagg.npy`, which are available at
a [separate repository](https://figshare.com/articles/follow17/5851884) due to their large sizes. The code to produce these files from the
raw data is included, but the raw data is available only upon request because of its even larger size.
The tool for semiautomatic detection of the leading vehicle from the LIDAR data is
available in a [separate repository](https://github.com/prinkkala/ibeo_manual_follow).

The code is released under the GNU AGPL-3.0 license, copyright by committers.
