import numpy as np
import scipy.stats

def interpret(rows, offset, dt):
    size = dt.itemsize
    raw = rows[:,offset:(offset + size)].tobytes(order='C')
    d = np.frombuffer(raw, dtype=dt)
    return d

def interpretations(data, refrerence_signal, dtypes):
    msglen = data.shape[1]
    for dtype in dtypes:
        dt = np.dtype(dtype)
        size = dt.itemsize
        for offset in range(msglen):
            if offset + size > msglen: break
            #raw = data[:,offset:(offset + size)].tobytes(order='C')
            #d = np.frombuffer(raw, dtype=dt)
            param = (offset, dt)
            yield param, interpret(data, *param)

DEFAULT_DTYPES = [np.int8, 'd']

def brutalize_matches(data, reference, dtypes=DEFAULT_DTYPES):
    corrs = []
    for idx, d in interpretations(data, reference, dtypes):
        c = scipy.stats.pearsonr(d, reference)[0]
        corrs.append((c, idx))
    corrs.sort(key=lambda x: -abs(x[0]))
    return corrs

def test():
    import struct
    t = np.linspace(0, np.pi*2, 100)
    os1 = np.sin(t)
    s2 = (np.cos(t)*100).astype(np.int8)

    s1 = os1 + np.random.randn(len(t))
    
    fmt = "=bd"
    msglen = struct.calcsize(fmt)
    data = b"".join(struct.pack(fmt, b, a) for a, b in zip(s1, s2))
    dt = np.dtype(dict(
        names=['val'],
        formats=['d'],
        offsets=[2],
        itemsize=msglen
    ))
    data = np.frombuffer(data, dtype=dt)
    import matplotlib.pyplot as plt
    plt.plot(data['val'])
    plt.show()
    print(data['val'])
    #data = np.frombuffer(data, dtype=np.uint8)
    #data = data.reshape(-1, msglen)
    #matches = brutalize_matches(data, os1)
    #print(matches)

if __name__ == '__main__':
    test()
