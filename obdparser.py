import numpy as np

foo = [[0.0, b'fooo'], [1.0, b'bar']]
foo = np.rec.fromrecords(foo, dtype=[('ts', float), ('data', bytes)])
print(foo[0].data.tobytes())
print(foo[1].data.tobytes())
