import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)

speed = 10.0
radius = 300.0
duration = 5*radius*2*np.pi/speed
dt = 0.1

gps_noise = 10.0
speed_noise = 0.1
yaw_rate_noise = np.radians(10.0)

t = np.arange(0, duration, dt)
distance = t*speed
circle_distance = radius*2*np.pi
angle = distance/circle_distance*2*np.pi
x = radius*np.sin(angle)
y = radius*np.cos(angle)

pos = np.vstack((x, y)).T
yaw_rate = np.gradient(angle)/dt

yaw_noise = np.random.randn(*yaw_rate.shape)*yaw_rate_noise - np.radians(0.0)
yaw_rate += yaw_noise
est_angle = np.cumsum((yaw_rate*dt)) + np.pi/2.0


est_speed = np.zeros_like(t) + speed
est_speed += np.random.randn(*est_speed.shape)*speed_noise

#direction = np.vstack((np.sin(est_angle), np.cos(est_angle))).T
#est_pos = np.cumsum(direction*dt*est_speed.reshape(-1, 1), axis=0) + pos[0]
#plt.plot(est_pos[:,0], est_pos[:,1], color='red', label='est')

gps_idx = list(range(0, len(t), 10))
gps_t = t[gps_idx]
gps_pos = pos[gps_idx]
gps_pos += np.random.randn(*gps_pos.shape)*gps_noise
gi_map = {i: gi for gi, i in enumerate(gps_idx)}

from location_filter import LocationFilter
filt = LocationFilter(pos[0], np.pi/2.0, vel_noise=speed_noise, yaw_noise=yaw_rate_noise, loc_noise=gps_noise)
xs = []; ys = []; ws = []
xs.append(filt.particles.x.copy())
ys.append(filt.particles.y.copy())
ws.append(filt.weights())
mbias = []
bws = []

prev_measurement = 0
for i in range(len(t)):
    filt.telemetry(dt, est_speed[i], yaw_rate[i])
    if i in gi_map:
        bw = filt.measurement(gps_pos[gi_map[i]])
        bn = i - prev_measurement
        prev_measurement = i
        bws.extend([bw]*bn)
        bws.append(filt.weights())
    xs.append(filt.particles.x.copy())
    ys.append(filt.particles.y.copy())
    ws.append(filt.weights())
    mbias.append(np.sum(filt.particles.yaw_bias*ws[-1]))
#plt.plot(x, y)
#plt.hist(filt.particles.yaw_bias, weights=filt.weights())
print(np.degrees(np.sum(filt.particles.yaw_bias*filt.weights())))
plt.plot(np.degrees(mbias))
plt.figure()
#plt.figure()
x = np.array([np.dot(xs[i], ws[i]) for i in range(len(ws))])
y = np.array([np.dot(ys[i], ws[i]) for i in range(len(ws))])
plt.plot(pos[:,0], pos[:,1], color='green', label='thruth')
plt.plot(gps_pos[:,0], gps_pos[:,1], '.')
plt.plot(x, y, '-')

plt.show()
