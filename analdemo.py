import sys
import numpy as np
import os
import matplotlib.pyplot as plt
import pyproj
import json
import scipy.interpolate
import scipy.spatial
import nslr
import pandas as pd

#proj = pyproj.Proj(init="epsg:3879")

def clicktrack():
    track = []
    trackp, = plt.plot([], [], 'o-')
    def onclick(event):
        if event.button == 1:
            track.append([event.xdata, event.ydata])
        elif event.button == 3:
            track.pop()
        if len(track) == 0:
            trackp.set_data([], [])
        else:
            trackp.set_data(*zip(*track))
        trackp.figure.canvas.draw()

    trackp.figure.canvas.mpl_connect('button_press_event', onclick)
    return track

def closester(haystack):
    tree = scipy.spatial.cKDTree(haystack, 10)
    return lambda x: tree.query(x)[1]

def track_projector(track, resolution=0.1):
    track = np.asarray(track)
    dists = np.linalg.norm(np.diff(track, axis=0), axis=1)
    distance = np.cumsum([0.0] + list(dists))
    rng = np.arange(distance[0], distance[-1], resolution)
    track_interp = scipy.interpolate.interp1d(distance, track, axis=0)
    track = track_interp(rng)
    distance = rng
    closest_i = closester(track)

    def dists(d):
        return distance[closest_i(d)]

    return dists, track_interp

def retrack(data, track, resolution=0.1):
    track = np.asarray(track)
    dists = np.linalg.norm(np.diff(track, axis=0), axis=1)
    distance = np.cumsum([0.0] + list(dists))
    rng = np.arange(distance[0], distance[-1], resolution)
    track = scipy.interpolate.interp1d(distance, track, axis=0)(rng)
    distance = rng

    ddist = distance[closester(track)(data)]

    #plt.figure()
    #plt.plot(distance, track[:,0])
    #x = lowess(data[:,0], ddist, return_sorted=False, frac=0.01, it=0)
    #y = lowess(data[:,1], ddist, return_sorted=False, frac=0.01, it=0)
    #plt.plot(track[:,0], track[:,1], '-')
    plt.plot(x, y, '.')
    plt.figure()
    plt.plot(ddist, data[:,0], '.')
    plt.plot(distance, track[:,0], '.')
    plt.plot(ddist, x, '.')
    #plt.plot(x, y, '-')
    plt.show()

block_type_ids = {
    'intro': 1,
    'unblind': 2,
    'blind': 3
    }
def proto_interp(events):
    def get(nt):
        prev = events['ts'].searchsorted(nt, side='left') - 1
        es = events[prev]
        is_in_block = es['event_type'] == 'block_start'

        block_type = es['block_type'].copy()
        block_type[~is_in_block] = ''
        return block_type, es['block_number']

    return get

class Recinterp:
    def __init__(self, data, t=None):
        self.data = data
        if t is None:
            t = self.data['ts']
        self.t = t
        
    def __getitem__(self, name):
        return scipy.interpolate.interp1d(self.t, self.data[name], bounds_error=False)

def load_data():
    sessions = sys.argv[1:]
    
    #bg = plt.imread('avoindata-Ortoilmakuva_2016.png')
    #extent = json.load(open('avoindata-Ortoilmakuva_2016.json'))
    bg = plt.imread('aerial_2017.png')
    extent = json.load(open('aerial_2017.json'))
    
    extent = extent['min'][0], extent['max'][0], extent['min'][1], extent['max'][1]
    print(extent)
    plt.imshow(bg, extent=extent)
    
    
    track_spans = [
            (100, 350),
            (600, 850),
            ]


    track = json.load(open('track.json'))
    track = np.array(track + [track[0]])
    trackproj, dist_to_coords = track_projector(track)

    plt.plot(track[:,0], track[:,1], 'r', lw=1)
    for span in track_spans:
        d = dist_to_coords(np.linspace(span[0], span[1], 100))
        plt.plot(d[:,0], d[:,1], 'r', lw=4)
    plt.axis('off')
    plt.savefig('map_annot.png', dpi=600)
    plt.show()

    x = []; y = []
    sds = []
    for session in sessions:
        get = lambda x: np.load(os.path.join(session, x + '.npz'))
        gps = Recinterp(get('gps')['gps'])
        
        t_start = gps.data['ts'][0]
        t_end = gps.data['ts'][-1]
        ts = np.arange(t_start, t_end, 0.01)

        sd = pd.DataFrame({'ts': ts})
        for f in 'latitude longitude x y'.split():
            sd[f] = gps[f](ts)

        sd['wheelspeed'] = Recinterp(get('telemetry')['wheelspeed'])['wheelspeed'](ts)
        
        sd['gap'] = Recinterp(get('lidar')['leader'])['distance'](ts)
        
        proto = get('protocol')['events']
        sd['block_type'], sd['block_number'] = proto_interp(proto)(ts)
        sd['block_type'] = sd['block_type'].map(block_type_ids)
        sd['distance'] = d = trackproj(sd[['x', 'y']].values)
        straight = ((d > track_spans[0][0]) & (d < track_spans[0][1])) | ((d > track_spans[1][0]) & (d < track_spans[1][1]))
        sd['on_straight'] = straight
        ws = sd['wheelspeed'].values.copy()
        #plt.plot(sd['ts'], sd['gap'], '.')
        #plt.show()
        sd['thw'] = sd['gap']/sd['wheelspeed']
        plt.plot(sd['distance'][sd.on_straight], sd['thw'][sd.on_straight], 'r.', alpha=0.01)
        valid = sd['block_type'] == 3
        
        vinterp = scipy.interpolate.interp1d(sd['ts'], valid, kind='nearest', fill_value='extrapolate')
        blockinterp = scipy.interpolate.interp1d(sd['ts'], sd['block_number'], kind='nearest', fill_value='extrapolate')
        blinder = get('blinder')['commands']
        lift_times = blinder['ts'][(blinder['command'] == 'l') & (vinterp(blinder['ts']).astype(bool))]
        blind_dur = np.diff(lift_times) - 0.3
        lift_times = lift_times[:-1]
        invalid = np.flatnonzero(blockinterp(lift_times[1:]) != blockinterp(lift_times[:-1]))
        blind_dur[invalid] = np.nan
        #sd['blind_dur'] = np.nan
        blind_durs = np.empty(len(sd))
        blind_durs[:] = np.nan
        blind_durs[sd.ts.values.searchsorted(lift_times)] = blind_dur
        sd['blind_dur'] = blind_durs

        relspeed = np.gradient(sd['gap'])/0.01 + sd['wheelspeed']
        #plt.plot(sd['ts'], relspeed)
        #plt.show()
        
        print(int(os.path.basename(session)[2:]))
        sd['session_id'] = int(os.path.basename(session)[2:])
        sds.append(sd)

        #plt.plot(sd['x'], sd['y'], '.', alpha=0.1, color='red')
        plt.show()
    
    plt.show()
    d = pd.concat(sds, ignore_index=True)#.to_records(index=False)
    d = d.to_records(index=False)
    #d.to_hdf('summary.h5', 'data')
    #np.savez_compressed('summary.npz', data=d)
    #np.save('summary.npy', d, allow_pickle=False)
    #xy = np.array((x, y)).T
    plt.plot(d.x, d.y, 'g.', alpha=0.1)
    #retrack(xy, track)
    #plt.axis('equal')
    #track = clicktrack()
    plt.show()
    #print(json.dumps(track))

if __name__ == '__main__': load_data()
