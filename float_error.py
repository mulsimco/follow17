import time
import numpy as np
import matplotlib.pyplot as plt

multiplier = 1000000
start = int(time.time())*multiplier
int_ts = np.arange(start, start+10*multiplier, 10)
float_ts = int_ts/multiplier
diff = int_ts - (float_ts*multiplier).astype(int)
print(float_ts.dtype)
plt.plot(int_ts, diff)
plt.show()
