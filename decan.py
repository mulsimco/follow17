from collections import namedtuple

import numpy as np
import ujson as json
from scipy.interpolate import interp1d

class rawarray:
    def __init__(self, data, rowlen, base_offset=0):
        self.data = data
        self.rowlen = rowlen
        self.base_offset = base_offset

    def column(self, offset, dtype):
        dt = dict(
            names=('val',),
            formats=(dtype,),
            offsets=(self.base_offset + offset,),
            itemsize=self.rowlen
        )
        return np.frombuffer(self.data, dtype=dt)['val']

    @property
    def colbytes(self):
        return self.rowlen - self.base_offset

    def after(self, offset):
        return rawarray(self.data, self.rowlen, self.base_offset + offset)

candata = namedtuple('candata', 'sender,ts,data')
def decode_can(lines):
    data = bytearray()
    ts = []
    msglen = 10
    template = b'\0'*msglen
    total_size = 0
    for line in lines:
        hdr, obj = json.loads(line)
        msg = obj['can']
        try:
            msg = bytes.fromhex("0" + msg)
        except ValueError:
            continue
        if len(msg) > 10:
            continue
        buf = bytearray(template)
        buf[:len(msg)] = msg
        data += buf
        ts.append(hdr['ts'])
    ts = np.array(ts)
    dt = np.ediff1d(ts, to_begin=0)
    changes = dt > np.mean(dt)
    changes[0] = True; changes[-1] = True
    rng = np.arange(len(ts))
    ts = interp1d(rng[changes], ts[changes])(rng)
    #plt.hist(dt, bins=100)
    #plt.show()
    print(len(data), total_size)
    data = np.frombuffer(data, dtype=np.uint8)
    data = rawarray(data, msglen)
    #data = data.reshape(-1, msglen)
    senders = data.column(0, '>i2')
    return candata(senders, ts, data.after(2))

