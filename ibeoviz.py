import subprocess
import sys
import json
from pprint import pprint
import matplotlib.pyplot as plt

def jsons(lines):
    for line in lines:
        if(hasattr(line, 'decode')):
            line = line.decode('utf-8')
        yield json.loads(line)


def ibeo_objects(idc):
    mangler = subprocess.Popen(['./objects_to_json'], stdout=subprocess.PIPE, stdin=idc)
    return jsons(mangler.stdout)

def run():
    objects = ibeo_objects(sys.stdin)
    objects = iter(objects)
    ts0 = None
    for hdr, objs in objects:
        ts = hdr['ts']
        if ts0 is None:
            ts0 = ts
        rts = ts - ts0
        if rts < 60*15: continue
        print(rts)
        for obj in objs['objects']:
            xys = [(c['x'], c['y']) for c in obj['contour']]
            x, y = zip(*xys)
            plt.plot(y, x)
        plt.xlim(-100, 100)
        plt.ylim(-100, 100)
        plt.pause(0.1)
        plt.cla()

if __name__ == '__main__': run()
