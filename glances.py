import pandas as pd
import json
import os
import numpy as np

def jsons(lines):
    for line in lines:
        if(hasattr(line, 'decode')):
            line = line.decode('utf-8')
        yield json.loads(line)


def get_glance_times(objs):
    lift_times = []
    for hdr, o in objs:

        if 'controller' not in o: continue
        if o['controller'].strip() == 'Lifting':
            lift_times.append(hdr['ts'])
    return np.array(lift_times)

def merge(*sdirs):
    data = []
    sids = []
    for sdir in sdirs:
        glance_times = get_glance_times(jsons(open(os.path.join(sdir, 'blinder.jsons'))))
        d = pd.DataFrame({'glance_times': glance_times})
        d['session_id'] = os.path.basename(sdir)
        if len(d) == 0:
            continue
        data.append(d)
        sids.append(os.path.basename(sdir))
    data = pd.concat(data, axis=0)
    data.to_pickle('glance_times.pickle')

import argh
argh.dispatch_command(merge)

