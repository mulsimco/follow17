import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
import scipy.stats
import json

def plot_background():
    bg = plt.imread('avoindata-Ortoilmakuva_2016.png')
    extent = json.load(open('avoindata-Ortoilmakuva_2016.json'))
    extent = extent['min'][0], extent['max'][0], extent['min'][1], extent['max'][1]
    plt.imshow(bg, extent=extent)

def get_track_data():
    data = np.load('summary.npy')
    data = pd.DataFrame.from_records(data)
    data['thw'] = data['gap']/data['wheelspeed']
    
    def getaccel(x):
        #dt = np.gradient(x.ts)
        #mdt = np.median(dt)
        accel = np.gradient(x)/DT # UGLY HARDCODING!!!
        accel = gaussian_filter1d(accel, 0.1/DT)
        
        return accel
    
    data['accel'] = data.groupby('session_id')['wheelspeed'].transform(getaccel)
    data['jerk'] = data.groupby('session_id')['accel'].transform(lambda x: np.gradient(x)/DT)
    data['absaccel'] = data.accel.abs()
    data['absjerk'] = data.jerk.abs()
    data.query("(block_type == 2 or block_type == 3)", inplace=True)
    return data
DT = 0.01
def get_track_aggregates():
    data = get_track_data()
    #wtf = data[data.session_id == 13]
    #plt.plot(wtf.ts, np.gradient(wtf.wheelspeed)/DT)
    #plt.show()
    
    data.query('wheelspeed > 1.0 and on_straight == True')

    #m = data.groupby(('session_id', 'block_type')).agg(lambda x: np.exp(np.nanmean(np.log(x)))).reset_index()
    m = data.groupby(('session_id', 'block_type')).agg(lambda x: np.exp(np.nanmean(np.log(x)))).reset_index()
    #print(m)
    for g, d in data.groupby(('session_id')):
        #if g[1] != 2: continue
        #if g == 1: continue
        #plt.hist(d.thw, bins=100)
        #d['jerk'][~d.on_straight] = np.nan
        #plt.plot(d['ts'], d['accel'])
        #plt.hist(d.absaccel, bins=100)
        
        plt.show()
        #plt.plot(d['distance'], np.gradient(d.wheelspeed)/DT, '.', alpha=0.1)
        #plt.plot(d['ts'], d['accel'])
        #plt.show()

        #plot_background()
        #d = d[(d.distance > 289) & (d.distance < 290)]
        #plt.plot(d['x'], d['y'], '.', alpha=0.1, color='red')
        #plt.show()
    plt.show()
    return m
    

    unblind = m[m.block_type == 2]
    blind = m[m.block_type == 3]
    m['block_type'] = m.block_type.map({0: '', 1: 'training', 2: 'normal', 3: 'blind'})
    np.save('aggregates.npy', m.to_records(index=False))
    print(np.exp(np.mean(np.log(unblind.thw))))
    #plt.hist(unblind.thw)
    #dthw = (blind.thw.values - unblind.thw.values)
    dthw = blind.thw.values# - unblind.thw.values
    od = (blind.blind_dur.values)

    #plt.plot(unblind.absaccel, unblind.absjerk, '.')
    plt.plot(unblind.absjerk, unblind.thw, '.')
    plt.figure()
    plt.plot(unblind.absaccel, unblind.thw, '.')
    plt.show()
    
    """
    x = (unblind.thw.values)
    y = (unblind.absaccel.values)
    print(x)
    print(y)
    plt.plot(x, y, '.')
    print(scipy.stats.pearsonr(x, y))
    plt.xlabel('Net time-headway (geom. mean, seconds)')
    plt.ylabel('Mean abs acceleration (geom. mean, m/s^2)')
    plt.show()
    """

    #include = (od > 1.0) & (od < 4.0)
    #dthw = dthw[include]
    #od = od[include]
    plt.plot(od, dthw, '.', label='Participant')
    fit = np.poly1d(np.polyfit(od, dthw, 1))
    rng = np.linspace(np.min(od), np.max(od), 100)
    plt.plot(rng, fit(rng), label=fit)
    plt.plot(rng, rng, 'k--', label="y=x")
    plt.ylabel('THW increase (seconds)')
    plt.xlabel('Occlusion duration (seconds)')
    print(fit)
    print(scipy.stats.pearsonr(od, dthw))
    plt.legend()
    plt.show()

SIM_EXP_IDS = dict(
    closeTheGap=1,
    calibration=2,
    verification=3,
    switchLanes=4,
    speedControl=5,
    miniCalibration=6,
    miniVerification=7,
    followInTraffic=8,
    blindFollowInTraffic=9,
    laneDriving=10,
    )
SIM_ID_EXPS = {v: k for k, v in SIM_EXP_IDS.items()}

VEHICLE_LENGTH = 4.56
def get_sim_aggregates():
    data = np.load('simagg.npy')
    data['ts'] /= 2.0
    data = data[np.in1d(data['experiment'], [SIM_EXP_IDS['followInTraffic'], SIM_EXP_IDS['blindFollowInTraffic']])]
    data = pd.DataFrame(data)
    #data = data.iloc[::10] # Avoid destroying memory
    blocks = data.groupby(['session_id', 'block'])
    data['dt'] = blocks['ts'].transform(np.gradient)
    data['speed'] = blocks['player'].transform(np.gradient)/data.dt
    data['leader_speed'] = blocks['leader'].transform(np.gradient)/data.dt
    data['accel'] = blocks['speed'].transform(np.gradient)/data.dt
    #data['accel'] = blocks['accel'].transform(lambda x: gaussian_filter1d(x, 0.01*90))
    data['leader_accel'] = blocks['leader_speed'].transform(np.gradient)/data.dt
    data['posaccel'] = data['accel']
    data.posaccel[data.posaccel < 0] = np.nan
    data['absaccel'] = data.accel.abs()
    data['gap'] = data.leader - data.player - VEHICLE_LENGTH
    data['thw'] = data.gap/data.speed
    
    def blind_durs(x):
        durs = np.empty(len(x))
        durs[:] = np.nan
        lift_idx = np.flatnonzero(np.diff(x.isBlind.values.astype(int)) > 0)
        lift_times = x.ts.values[lift_idx]
        blind_durs = np.diff(lift_times) - 0.3
        durs[lift_idx[:-1]] = blind_durs
        return pd.Series(durs)

    data['blind_dur'] = data.groupby(('session_id', 'block')).apply(blind_durs).values
    
    data = data.query('player > 100 and speed > 1.0')
    def nopractice(x):
        blocks = x.block.unique()[1:]
        return x[x.block.isin(blocks)]
    data = data.groupby(('session_id', 'experiment')).apply(nopractice)
    
    """
    for g, d in data.groupby(('session_id', 'block')):
        if d.experiment.iloc[0] != SIM_EXP_IDS['blindFollowInTraffic']: continue
        gd = d[d.blind_dur.notnull()]
        plt.plot(gd['ts'], gd['blind_dur'], 'o-')
        plt.plot(gd['ts'], gd['thw'])
        plt.semilogy()
        plt.show()
        #last = d['block'].unique()[-1]
        #d = d[d.block == last]
        #ax = plt.subplot(2,1,1)
        #plt.plot(d.abstime, d.accel)
        #plt.plot(d.abstime, d.leader_accel)
        #plt.show()
        #plt.hist(np.abs(d.accel), bins=100)
        #plt.axvline(np.nanmean(d.absaccel), color='red')
        #plt.semilogx()
        #plt.plot(d.ts, d.accel)
        #plt.plot(d.ts, d.leader_accel)
        #plt.subplot(2,1,2, sharex=ax)
    """
    
    def blockstats(x):
        return pd.DataFrame([dict(duration=(x.ts.iloc[-1] - x.ts.iloc[0]))])
    # TODO: No crashes!
    blockstats = data.groupby(('session_id', 'block')).apply(blockstats)
    blockdurs = blocks['ts'].agg(lambda x: x.iloc[-1] - x.iloc[0])#.reset_index()
    blockdurs = blockdurs[blockdurs > 150].reset_index()
    blocks_per_session = blockdurs.groupby('session_id').count()
    valid_sessions = blocks_per_session.query('block >= 6').reset_index()['session_id']
    
    m = data.groupby(('session_id', 'experiment')).agg(lambda x: np.exp(np.nanmean(np.log(x)))).reset_index()
    m = m[m.session_id.isin(valid_sessions)]
    return m


def thw_vs_od():
    bg = background()
    sim = get_sim_aggregates()
    track = get_track_aggregates()
    bg['report_gap'] = pd.to_numeric(bg['dsi13_riittävänetäisyydenpitäminenedelläajavaan'], errors='coerce')
    sim = sim.merge(bg, on='session_id', suffixes=('', '_bg'))
    blind = sim[sim.experiment == SIM_EXP_IDS['blindFollowInTraffic']]
    unblind = sim[sim.experiment == SIM_EXP_IDS['followInTraffic']]
    track = track.merge(bg, on='session_id', suffixes=('', '_bg'))
    blind = track[track.block_type == 3]
    unblind = track[track.block_type == 2]
    data = unblind.merge(blind, on='session_id', suffixes=('', '_blind'))
    ##print(blind)
    data['button'] = 3
    data.button[data.session_id <= 8] = 2
    data.button[data.session_id <= 6] = 1
    #for sid, d in blind.groupby('session_id'):
    #    plt.text(d.blind_dur, d.thw, sid)

    plt.scatter(blind.thw, blind.blind_dur, 'o')
    plt.plot([0,0], [10,10])
    plt.show()
    import statsmodels.formula.api as smf
    model = smf.ols("blind_dur_blind ~ thw_blind + absaccel_blind", data=data).fit()
    #x = data.thw_blind - data.blind_dur_blind; y = data.absaccel_blind
    y = model.fittedvalues
    x = data.blind_dur_blind
    plt.scatter(x, y)
    print(scipy.stats.pearsonr(x, y))
    #plt.colorbar()
    fit = np.poly1d(np.polyfit(blind.blind_dur, blind.thw, 1))
    rng = np.linspace(blind.blind_dur.min(), blind.blind_dur.max(), 100)
    #plt.plot(rng, fit(rng), '-')
    #plt.plot(rng, rng, '--')
    plt.ylabel('Thw')
    plt.xlabel('Occlusion duration')
    plt.show()
    """
    plt.figure()
    #x = unblind.dsisafety
    #y = unblind.thw
    x = blind.report_gap
    y = blind.thw - blind.blind_dur
    valid = x.notnull() & y.notnull()
    x, y = x[valid], y[valid]
    plt.scatter(x, y, c=blind[valid].gender)
    print(scipy.stats.spearmanr(x, y))
    plt.show()
    """

def track_vs_sim():
    bg = background()
    track = get_track_aggregates()
    sim = get_sim_aggregates()
    sim = sim[sim.experiment == SIM_EXP_IDS['followInTraffic']]
    track = track[track.block_type == 2]
    m = track.merge(sim, on='session_id', suffixes=('', '_sim'))
    m = m.merge(bg, on='session_id', suffixes=('', '_bg'))
    m['gender'] = m['Sukupuoli'].map(dict(mies=0, nainen=1))
    m['report_gap'] = pd.to_numeric(m['dsi13_riittävänetäisyydenpitäminenedelläajavaan'], errors='coerce')
    m = m[m.report_gap.notnull()]
    print(scipy.stats.spearmanr(m.report_gap, m.thw))
    plt.plot(m.report_gap, m.thw, '.')
    plt.show()
    print(m['report_gap'])
    plt.figure()
    plt.ylabel('Thw sim (seconds, brutto)')
    plt.xlabel('Thw track (seconds, netto)')
    m['trackdiff'] = np.abs(m.thw - m.thw_sim)
    plt.scatter(m.thw, m.thw_sim)
    #plt.scatter(np.log(m.thw/m.absaccel), np.log(m.thw_sim/m.absaccel_sim), c=m.report_gap)
    #plt.colorbar()
    rng = np.linspace(m.thw.min(), m.thw.max(), 10)
    plt.plot(rng, rng, '--')
    print(scipy.stats.spearmanr(m.thw, m.thw_sim))
    plt.figure()
    plt.ylabel('Absaccel sim (m/s²)')
    plt.xlabel('Absaccel track (m/s²)')
    plt.scatter(m.absaccel, m.posaccel, c=m.gender)
    plt.colorbar()
    print(scipy.stats.spearmanr(m.absaccel, m.posaccel))
    plt.show()

def background():
    data = pd.read_csv('background.csv')
    data = data[data.Kokeennimi == "Headway 17"]
    data['session_id'] = data["Koehenkilötunnus"].astype(int)
    #print(data['session_id'])
    data['dsimotor'] = pd.to_numeric(data['dsimotorFAC1_6'], errors='coerce')
    data['dsisafety'] = pd.to_numeric(data['dsisafetyFAC2_6'], errors='coerce')
    data['gender'] = data['Sukupuoli'].map(dict(mies=0, nainen=1))
    data['bis'] = pd.to_numeric(data['bisFAC1_5'], errors='coerce')
    data['bas'] = pd.to_numeric(data['basFAC2_5'], errors='coerce')
    
    """
    from sklearn.decomposition import PCA
    pca = PCA()
    pca.fit(data[['bis', 'bas', 'dsimotor', 'dsisafety']].dropna())
    plt.plot(data.bis - data.bas, data.dsimotor - data.dsisafety, 'o')
    x = data.bis - data.bas
    y = data.dsimotor - data.dsisafety
    valid = x.notnull() & y.notnull()
    """


    #print(pd.to_numeric(data['dsi13_riittävänetäisyydenpitäminenedelläajavaan'], errors='coerce'))
    #print(data['dsi13_riittävänetäisyydenpitäminenedelläajavaan'].map({' ': ''}))
    return data

def thw_vs_od_timeseries():
    data = get_track_data()
    print(data.columns.values)
    for g, d in data.groupby(('session_id')):
        #if d['block_type'].iloc[0] != 3: continue
        #plt.plot(d.ts, d['on_straight'])
        gd = d[d.blind_dur.notnull()]
        plt.subplot(1,2,1)
        gd.blind_dur[~gd.on_straight] = np.nan
        gd.thw[~gd.on_straight] = np.nan
        plt.plot(gd.ts, np.log(gd.blind_dur), 'o-', label='blinddur')
        plt.plot(gd.ts, np.log(gd.thw), 'o-')
        plt.subplot(1,2,2)
        plt.plot(gd.thw, gd.blind_dur, 'o')
        plt.legend()
        plt.show()

def check_track_aggregates():
    data = get_track_data()
    
    for g, d in data.groupby('session_id'):
        d = d.query('wheelspeed > 1.0 and block_type == 3 and on_straight and thw > 0')
        plt.title(str(g))
        #plt.hist(d.thw, bins=100)
        thw = d.thw.values
        gm = np.exp(np.mean(np.log(d.thw)))
        #plt.axvline(gm, color='red')
        plt.plot(d.wheelspeed*3.6, d.gap, '.', alpha=0.01, color='red')
        plt.show()
        #thw = d.thw.values.copy()
        #valid = (d.speed > 1.0) &

    """
    for g, d in data.groupby(('session_id', 'block_number')):
        if d.block_type.iloc[0] != 3: continue
        thw = d.thw.values.copy()
        thw[~(d.on_straight.values)] = np.nan
        plt.title(str(g))
        plt.plot(d['ts'], d.thw, alpha=0.5)
        plt.plot(d['ts'], thw)
        plt.show()
    print(list(data.columns))
    """
    #for g, d in data.groupby(('session_id', 

def get_track_aggregates2():
    data = np.load('summary.npy')
    data = pd.DataFrame.from_records(data)
    data['thw'] = data['gap']/data['wheelspeed']
    
    def getaccel(x):
        #dt = np.gradient(x.ts)
        #mdt = np.median(dt)
        accel = np.gradient(x)/DT # UGLY HARDCODING!!!
        accel = gaussian_filter1d(accel, 0.1/DT)
        
        return accel
    
    data['accel'] = data.groupby('session_id')['wheelspeed'].transform(getaccel)
    data['jerk'] = data.groupby('session_id')['accel'].transform(lambda x: np.gradient(x)/DT)
    data.query('on_straight', inplace=True)
    
    def getagg(x):
        ps = [10, 25, 50, 75, 90]
        thws = np.percentile(x['thw'], ps)
        accels = np.percentile(x['accel'], ps)
        jerks = np.percentile(x['jerk'], ps)
        out = {}
        for p, thw, accel, jerk in zip(ps, thws, accels, jerks):
            out[f"thw_{p}"] = thw
            out[f"accel_{p}"] = accel
            out[f"jerk_{p}"] = jerk
        return pd.DataFrame.from_records([out])

    agg = data.groupby(('session_id','block_type')).apply(getagg).reset_index()
    agg.to_csv('track_percentiles.csv', index=False)

if __name__ == '__main__':
    #thw_vs_od_timeseries()
    #thw_vs_od()
    #background()
    #get_sim_aggregates()
    #get_track_aggregates()
    get_track_aggregates2()
    #check_track_aggregates()
    #track_vs_sim()
